---
title: Mon passage sous Android 9.0
url: 203.html
id: 203
categories:
  - Non classé
tags:
---

Il y a quelques semaines, mon téléphone à décidé de rendre l'âme. D'ailleurs, juste avant un entretiens téléphonique pour une embauche. Plein d'allégresse après un tel événement qui n'a pas de caractère stressant (ah ah ah), je me suis mis en quête d'un nouveau téléphone.

Contrairement à la dernière fois, j'ai eu le contrôle de mon choix, et je l'ai fait suivant des critères personnels. Mon objectif était d'éviter les téléphones pleins de bloatware, et d'avoir du coup un minimum de contrôle dessus. J'ai choisi pour cela un téléphone sous Android One (un Nokia 7.1), pour les mises à jour et pour l'absence de trucs en plus.

J'ai du coup refait la même chose que la dernière fois, c'est à dire dégoogliser au possible mon téléphone. J'ai décidé de le faire sans changement de bootloader où de root, pour me limiter à ce qu'il est possible de faire pour quelqu'un qui comme moi n'est pas un grand technicien.

Bye-bye les applis Google
-------------------------

Comme toujours, la première étape a été de supprimer / désactiver les applications Google. Comme d'habitude, tout a été désactivable, et j'ai donc pu avoir un téléphone aussi clean que possible. Au vue de la tendance de Google à désormais tout connecter, j'ai également décidé de faire du ménage dans les applications de bases.

### Remplacements aux applications de base

Donc tout d'abord, j'ai