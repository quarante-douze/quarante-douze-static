---
title: 'La droite et la gauche, et les soucis que ces notions provoquent.'
url: 206.html
id: 206
categories:
  - Non classé
tags:
---

Depuis quelques temps, des journalistes du Monde sont cibles de [harcèlement](https://twitter.com/samuellaurent/status/1136915763502231552) de la part de militant du groupe La République en Marche. La source de cela est la découverte qu'il y avait par LREM la pratique de l'[astroturfing](https://fr.wikipedia.org/wiki/Astroturfing) de la part du mouvement, par la création de faux comptes twitters.

Mon but n'est pas de parler des mérites et défauts du journaliste Samuel Laurent (et j'aurais tendance à dire que si vous vous croyez munie d'un devoir de rendre justice divine qui vous donne le droit de pratiquer le harcèlement ciblé : vous ne valez en faisant cela pas mieux qu'un agresseur ou qu'un bully de collège qui se mettent comme des lâche en groupe pour attaquer quelqu'un. Oh, cela ne vous empêche pas d'être sans doute un chouette type par ailleurs quand vous n'êtes pas un harceleur. Mais ce comportement est merdique, fin.) ni de dire si LREM est ou pas les seuls à utiliser faux compte et manipulation du genre (protip : non, les fameux trolls d'extrèmes droites et faux compte, on en vu passer pas mal dans les milieux militants).

Non, ce qui me questionne c'est bien plus, c'est que à côté de cela une réthorique existe chez ces militants, qui est très présente : nous sommes les modérés, en lutte contre les extrèmes. Et je pense que cela est typique d'un soucis dans la politique, le fait de penser le positionnement droite gauche comme une sorte d'absolu, et de certains biais qui en existent (l'idée que le centre serait une sorte de "juste mesure" entre les deux mouvements). Sauf que le soucis est faux, pour plusieurs raisons.

Qu'est-ce que la droite, qu'est-ce que la gauche ?
--------------------------------------------------

C'est là la première question que l'on doit répondre. Qu'est-ce qui défini en France la droite et la gauche ?

Généralement, on pose à gauche les partis qui s'intéressent aux choses suivantes : le sorts des employés, une répartitions plus "égalitaire" des richesses, la défense d'une certaine justice sociale notamment envers les minorités, l'idée d'un plus fort contrôle de l'économie soit par l'état, soit par le peuple.

À droite, on posera plus les idées de l'importance des élites, du mérite, une vision plus libérale économiquement (en tout cas du point de vue de tout ce qui est aide sociale) mais souvent plus authoritarienne et conservatrice d'un point de vue sociétal, voir parfois allant jusqu'à l'idée qu'il faudrait bien plus de contrôle, d'autorité.

En France, le centre sera souvent plus libéral (un peu comme la droite), tentera de prendre des positions plus ou moins progressistes, et tentera d'osciller un peu entre quelques aspects des autres. Sauf sur l'aspect libéral, où c'est souvent vraiment plus un libéralisme parfois encore plus "dur" que celui de la droite qui est défendu. Souvent, en fait c'est une sorte de libéralisme politique et économique à la fois que déclare défendre le centre.

À cela se rajoute les "extrèmes" : À gauche, le communisme et l'anarchisme et toutes mouvances proches, contre ou très critique du système économique libéral dans son ensemble et voudraient l'abolir/le réformer en profondeur avec pour l'un un plus grand contrôle de l'état (avec l'idée qu'on ne peut pas faire confiance à un système dont le but premier est de faire de l'argent, puisqu'il encourage le fait de faire passer les gains avant les besoins réels), et pour l'autre une multitude de système et de théories qui vaudrait une armada d'articles tellements y'aurait à dire. À droite, on est plus dans les mouvances qui prône un "retour à l'ordre" fort, avec une grande figure d'autorité et une obéissance bien plus présente à des normes fondées sur les groupes majoritaires, ainsi que l'expulsion des étrangers, vu comme source des maux du pays.

Cependant, on peut tomber dans au moins quatre soucis au niveau de ces "extrèmes" :

*   Tout d'abord, qu'est-ce que l'extrème gauche pense de la question des minorités, de la justice sociale sur ce point ? Et bien la réponse est en fait assez divisée. Il y a en effet une partie de l'extrème-gauche qui vont être dans l'idée qu'il faut tout faire pour lutter contre les oppressions sociales (et que celles-ci peuvent être théorisé dans le cadre du [matérialisme historique](https://fr.wikipedia.org/wiki/Mat%C3%A9rialisme_historique), notamment les luttes féministes avec l'idée que les genres seraient aussi une oppression économique), d'autres qui vont être plus en accord avec l’extrême-droite, et une autre partie qui estime que ce n'est pas l'objectif même du mouvement qui ne vise que la libération des oppressions économiques. Je ne saurais pas jauger les différentes parties, mais déjà on voit que des questions ne sont pas placé sur une partie de la "frise" alors qu'elle le sont de l'autre. Ces questions sont d'ailleurs très importante dans l'extrème gauche actuelle, puisqu'elles posent la question des mouvements féministes, LGBT+, dans ces mouvements.
*   Les anarchiste sont souvent placé à l'extrème-gauche, avec les communistes… Or, une partie du communisme actuel met en avant un rôle que l'état doit avoir justement pour permettre cela, quand les anarchistes sont souvent bien plus critique voir abolitionniste sur le plan même de l'état. Cependant, cela n'empêches pas parfois d'être d'accord sur d'autres points. Alors, ensemble, pas ensemble ? Je sais pas, et ça dépendra de à qui on demande. Compliqué ? Oui.
*   Si y'a un "extrême" dans la question du libéralisme d'un côté... Où se trouve l’extrême de l'autre ? Où met-on ceux qui veulent aller jusqu'au bout du libéralisme, de tout privatiser, de l'idée que l'état ne pourrait _intrinsèquement_ et _en essence_ pas jouer de rôle dans l'économie, et que la main invisible devrait agir ? Et l'idée que les gens sont à 100% les uniques acteurs de leur réussite ou non ? La négation de ce que montre des décennies de science sociale ? Ce sont des positions extrêmes sur la question de l'économie, mais elle ne se trouve pas dans nos extrêmes alors que l'autre bout y est.
*   Et pour prendre un exemple extrème : où placer le libertarianisme, à droite économiquement mais qui estime que l'état doit être aboli parce qu'il serait aliénant pour la liberté ? Peut-il être véritablement à 100% du côté de l'extrème-droite s'il est anti-étatique alors qu'une grosse partie du reste est pro-étatique ?

De plus, à côté de cela, nous pouvons remarquer que notre gouvernement actuel qui revandiquerait le fait d'être centriste, n'hésite pas à défendre l'importance de l'autorité, et à employer des méthodes autoritaires et dure pour lutter contre les manifestations. Est-on alors encore dans le mélange "libéralisme politique plus liberalisme économique" ?

Tout ces questions me font dire qu'un axe droite-gauche pose de grosse limite. Mais alors quelle est la solution ? Faut-il plus d'axe ?

Le quadrant politique, un premier outil… trop imparfait.
--------------------------------------------------------

The [quadrant politique](https://www.politicalcompass.org/), est un concept qui propose de se positionner non pas sur un seul axe, mais sur deux axes. D'un côté, il y a un positionnement économique (l'axe droite gauche pour eux) et un positionnement sociétal entre autoritarisme et positionnement libertaire.

De ce fait, on pourrait faire la différence entre un parti à positionnement autoritaire et contre l'interventionnisme économique, un parti à positionnement de gauche économiquement et autoritaire, un parti à positionnement de droite économiquement et libertaire, et un parti libertaire et de gauche économiquement.

Cependant, je pense que ce type de positionnement entraine toute une nouvelle gamme de soucis : le positionnement "libertaire" (l'idée du contrôle de l'état) est fusionné sur ce site avec le positionnement pour les libertés individuel. Et je pense qu'on aurait tord de penser que les deux sont liés. En effet, si un communiste estimant le besoin d'un état fort pour dominer l'économie (gauche et "autorité") peut aussi estimer que le rôle de l'état est de protéger les minorités ! Dans-ce cas, est-il vraiment autoritariste dans le sens de cet boussole politique ? Je vais prendre l'exemple inverse, le libertarianisme. Je pense qu'on aurait tors de penser qu'une grande partie des libertariens, invoquant la "liberté économique" et la "liberté face à l'état" sont forcément ouvert sur le plan sociétal. En effet, il est tout a fait possible pour un libertarien de défendre l'idée que l'homosexualité serait contre-nature… et ça arrive régulièrement. Le libéralisme économique, le progressisme et l'étatisme sont donc déjà trois questions différentes.

De plus, état fort, est-ce que cela veut dire un état démocratique où non ? Parce que l'on peut imaginer un état peu présent dans la vie des citoyens, mais qui serait controlé juste par une petite caste. De l'autre, on peut imaginer un état qui contrôle bien trop la vie des citoyens, mais qui serait contrôlé entièrement démocratiquement.

Dans ce cas-là, quelle est la solution ? Faire encore plus d'axe ? Parce que je n'ai pas encore parler de tas d'autres positions importantes :

*   L'écologie.
*   La question des outils informatiques et de leur contrôle et de l'ouverture des sources (Logiciel Libre, Open Source)
*   Des questions autour du droit d'auteur et des biens communs. (Domaine Public, etc)
*   Les questions autour des conditions de vie animales et/ou de la consommation de viandes.
*   Des questions de santés en règle générale.
*   De l'interventionnisme, de l’isolationnisme.
*   Du protectionnisme économique : doit-on aider le commerce intérieur, doit-on faire en sorte d'avoir des "acteurs français" contre les grandes multi-nationales ?
*   De la question de la "liberté d'expression". Peut-on tout dire, et sinon qu'est-ce qui doit être limité

Ce sont pleins de questions politiques, mais qui n'ont pas toujours de positionnement clair par rapport aux autres axes. J'ai vu des vegan de tout bord politique, des pro-libre aussi bien capitalistes que communistes, des interventionniste aussi bien conservateur que progressistes…

Conclusion
----------

Nous avons vu que placer dans des axes les mouvements politiques pose pleins de question, et peut provoquer des soucis. Cependant, il faut reconnaître que souvent, certaines viennent par bloc. Et c'est peut-être la plus grande critique que l'ont peut faire à ma question. Il y a bien peut-être une plus grande corrélation entre l'idée d'être conservateur et libéral économiquement qu'entre conservateur et pro-contrôle de l'économie. Sans doute parce que le libéralisme économique est très compatible avec l'idée du "mérite". Mais cela montre bien pour moi la limite de ce que je dis : Même s'il y a de gros soucis dans la notion droite gauche, ces notions ont quand même une utilité pour définir des choses.

Quand des gens de gauche disent que "Macron est de droite", ils font un message important de leur point de vue : que la politique de Macron est similaire à celle des présidents de droite qui étaient précédemment au pouvoir, critiquant alors l'idée de Macron d'être un "renouveau". De même pour l'idée de "gauche" qui est chère à une partie de ceux se revandiquant de gauche, et surtout d'une certaine idée de la gauche. La gauche n'est alors pas qu'un positionnement économique : il s'agit alors d'un message mélangeant plusieurs de ces idées, pour en faire une sorte de mouvement unis autour d'une idée dont les autres découleraient.

Alors, peut-être la solution est de reconnaître qu'à côté de cela, il existe des gauches, et pas une gauche. Des droites et pas une droite, et d'y accoler des labels. De même, certains ont trouvé la solution face à l’extrémisme et à la radicalisation qui peut exister parfois au centre : parler d’extrême-centre, oxymore des plus intéressant puisqu'il renverse directement l'idée que le centre ne pourrait pas être extrême, au lieu de tenter de contourner le problème et de créer un tout autre système de pensée.

Je n'ai cependant pas de solution parfaite, et moi-même je continuerais de parler de gauche et de droite. En écrivant cet article, je me suis constamment dit "mais si, ça c'est de gauche" ou "ça c'est de droite". Souvent en voulant placer les idées avec lesquels je suis d'accord avec mon côté politique. Cependant… Est-ce que je peux nier que parfois le monde du libre contient des gens réac' ? Non, j'en ai vu trop d'exemple. Certaines idées vont se retrouver à différents endroit de l'échiquier politique.

Ce qu'il faut faire, c'est comprendre cette complexité, et se rappeler que les labels de bases ne sont pas tout. Mais je pense que les notions de "droite" et de "gauche" gardent un caractère qui peut être utile, et qu'elles ne sont pas nécessairement à jeter à 100%.