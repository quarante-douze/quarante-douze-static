---
title: A propos du départ de RMS
url: 211.html
id: 211
categories:
  - Non classé
tags:
---

Richard Matthew Stallman est le fondateur du mouvement du logiciel libre et du projet GNU, et a été jusqu'à récemment le directeur de la Free Software Foundation, l'une des fondations ayant un rôle important dans le monde du logiciel libre.

Ces derniers jours, de nombreux membres de la communauté ce sont insurgés des dernières déclaration de Stallman, où il prenait la défense de Marvin Minsky, un collègue décédé du MIT qui d'après les info qu'on a a également eu des relations sexuelles avec les victimes d'Eipstein, dans le cadre du coup de l'affaire de trafic sexuel d'Eipstein.

Le, ou un des premiers articles parlant du sujet était un article demandant justement son départ de la FSF, [Remove Stallman](https://medium.com/@selamie/remove-richard-stallman-fec6ec210794), qui expliquait particulièrement où se trouvait le soucis : En effet, outre l'aspect problématique de sa défense, Stallman a particulièrement mis en avant le fait que la victime (mineure, je rappelle) se serait "présenté comme consentante" à Minsky. Il a également continué en défendant que "définir le viol selon l'age" serait "moralement absurde". Stallman a déjà par le passé posé de nombreux problèmes sur ce sujet et sur d'autres relié.

Visiblement, cela a été la fois de trop. Les projet [GNOME](https://blog.halon.org.uk/2019/09/gnome-foundation-relationship-gnu-fsf/) et la [Software Freedom Concervancy](https://sfconservancy.org/news/2019/sep/16/rms-does-not-speak-for-us/) ont annoncé qu'ils demandaient son retrait de la FSF. Cela s'est donc produit ensuite, et on peut supposer qu'on entre dans une nouvelle ère de la FSF, qui sera peut-être moins radicale et avec moins de problèmes sur des sujets aussi important.

Donc, pourquoi y revenir ? Parce que cette histoire m'a permis de constater une technique pour dénigrer des propos très utilisé :

L'accusation de n'avoir lu que le titre
---------------------------------------

Le principe de ce type d'accusation est simple. Prenez dans un sujet, un article qui a écrit quelque chose de faux dans le titre ou dans le contenu. Ici, c'est un article de [VICE](https://www.vice.com/en_us/article/9ke3ke/famed-computer-scientist-richard-stallman-described-epstein-victims-as-entirely-willing), qui a dit que Richard Stallman aurait décrit directement les victimes de Eipstein comme "consentante". Cet article commet effectivement des erreurs - qui ne changent pas la gravité des propos - tout en ayant juste sur d'autres parties (le fait que Stallman a sorti des arguments très problématique sur la pédophilie).

Le soucis est que des gens s'en sont servi pour dénigrer toute les critiques de ses propos, disant que les gens sont contre Stallman parce qu'il n'auraient que lu "le titre" (ce qui est amusant quand l'article de GNOME cite directement le mail en question)