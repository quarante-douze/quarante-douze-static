---
title: 'XFCE 4.16 utilisera des CSD, plus d''explication'
url: 214.html
id: 214
categories:
  - Non classé
tags:
---

L'environnement de bureau pour Linux/Unixes XFCE, apprécié pour sa légereté et sa rapidité, a annoncé depuis quelques temps un changement de design. Il a en effet [indiqué passer à ce qu'on appelle des décorations côtés clients](https://wiki.xfce.org/releng/4.16/roadmap/general_ui/csd), c'est à dire que les applications s'occuperont de dessiner elle-même les barres de titre, au lieu de laisser le gestionnaire de fenêtre le faire.

Ce changement aura tout particulièrement deux effets :

Tout d'abord, Les applications où c'est assez simple seront en partie redesignée pour utiliser des Headerbar, une barre de titre combinant décoration de fenêtre et la possibilité d'y ajouter quelques outils en plus. Parmi les applications touchée, il y a toute les fenêtre de paramètres (ou la Headerbar remplacera la grande zone réservée à l'icone et description de la fenêtre) et des applications telles que le gestionnaire de tâche

![](https://wiki.xfce.org/_media/releng/4.16/roadmap/general_ui/color.png)

Avant

![](https://wiki.xfce.org/_media/releng/4.16/roadmap/general_ui/color-csd.png)

Après. Notez la barre de titre qui contient désormais la description de l'application

Les autres applications GTK seront simplement paramétrée pour utiliser par défaut une barre de titre normale, mais dessinée par l'application plutôt que par le gestionnaire de fenêtre. Ils ont indiqué ne pas avoir pour l'instant pour objectif de faire de gros redesigns aux applications.

Pourquoi un tel changement ?
----------------------------

Le projet XFCE indique vouloir faire ce changement pour un certains nombre de raisons :

*   La première est que XFWM, leur gestionnaire de fenêtre, ne fourni comme espace pour redimensionner la fenêtre que a bordure de la fenêtre. Cela signifie que tout theme de fenêtre voulant des bordures fines sera plus difficile à redimensionner. Utiliser les décoration côté client leur offre le support de cela "gratuitement".
*   Il y a aussi un avantage de cohérence : le theme étant le même pour l'application et pour la barre de titre, cela assure qu'ils soient cohérent entre eux.
*   Un dernier avantage est que les décorations côté client étant dessiné par la boite à outil de l'application, elles peuvent supporter certaines fonctionnalités de l'application : par exemple s'afficher avec un thème sombre pour les applications en demandant un.

![](https://wiki.xfce.org/_media/releng/4.16/roadmap/general_ui/panel-dark.png)

Sans CSD, XFWM ne peut pas adapter la barre de titre au theme sombre

![](https://wiki.xfce.org/_media/releng/4.16/roadmap/general_ui/panel-dark-csd.png)

Avec, il est plus facile de respecter le theme sombre

Quels effets, en vrai ?
-----------------------

Beaucoup de spéculations on eu lieu sur ce que cela aura comme conséquences. En regardant un peu, j'ai décidé de séléctionner quelques uns des craintes et de tenter d'y répondre à partir des informations que l'on a.