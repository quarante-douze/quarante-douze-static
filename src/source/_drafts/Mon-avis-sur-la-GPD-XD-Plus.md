---
title: Mon avis sur la GPD XD Plus
url: 205.html
id: 205
categories:
  - Non classé
tags:
---

La GPD XD Plus est une console portable Android au look rappellant énormément la Nintendo 3DS, produite par l'entreprise chinoise Gamepad Digital. Cependant, à la place d'un double écran, elle fonctionne plutôt comme la Gameboy Advance SP, ayant sous le clapet les différents boutons. Plutôt chère, elle coute dans les 200€, et est souvent avant-tout utilisée comme console à émulation, même si elles proposes quelques fonctionnalités utiles pour être utilisée en tant que console portable avec des jeux Android (déjà, tout ceux compatible manette.

Étant un grand amateur de console open-source, et ayant beaucoup de jeux Android bien plus agréable à la manette (tel que ittle dew, ou), et ayant envie depuis un moment de jouer aux hack-roms que j'apprécie plutôt sur une console que sur PC, j'ai décidé de me la prendre quand j'ai vu que je pouvais avoir une réduc' dessus.

Impression globale
------------------

En l'ouvrant, deux chose surtout m'ont un peu fait tiqué : la position pas forcément ergonomique des boutons principaux, et le clapet un peu dur à ouvrir. Cependant, une fois ces inconvéniant passé, la console reste agréable en main et la forme "à clapet" permet d'éviter que l'écran s'abime trop dans les transport. Gros point positif avec le fait qu'elle ait la taille d'une 3DS : je peux du coup commander des pochette 3DS pour la transporter, ce qui laisse plus de choix - et des choix cools pour un gros nerd comme moi, surtout.