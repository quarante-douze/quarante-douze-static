---
title: Un premier mockup d'un redesign de Firefox pour la version 57
tags:
  - Firefox
url: 23.html
id: 23
categories:
  - Libreries
date: 2017-03-26 21:16:00
---

Il semblerait qu'avec Firefox 57, Mozilla veuille essayer de faire une grande étape dans l'évolution de son navigateur web. On avait déjà entendu parler du projet Quantum, qui vise à intégrer des éléments venant de Servo dans le moteur Gecko, afin de l'améliorer, et de profiter notamment des possibilités de parralélisation que le langage de programmation Rust, dans lequel est écrit Servo, offre. Il semblerait que Mozilla désire commencer à intégrer les premiers éléments de Quantum dans la version 57 de son navigateur.

À côté de cela, un projet de raffraichissement du design du navigateur web semble également lancé, et nous avons un premier mock-up ici. Attention, le design présenté ici est un mockup, et sera peut-être - et surement - sujet à des changements ! :) Et s'il y a d'autres mockup proposé dans les temps qui viennent, j'essairais de les montrer afin qu'on voit l'évolution du projet.

![Un mockup proposé pour le redesign de Firefox](http://cdn.ghacks.net/wp-content/uploads/2017/03/firefox-photon-design.png)

Parmi les choses que l'on peut remarqué, il y a l'utilisation d'onglets rectangulaires en remplacement de ceux arrondis d'Australis. Au niveau de la barre principale, on peut remarquer la présence d'une icone pour [l'outil de screenshot de Firefox](https://testpilot.firefox.com/experiments/page-shot) présent en Test Pilot, et de deux autres icones inconnues. Le premier ressemble à une bibliothèque, ce qui pourrait laisser supposer quelque chose de proche de la barre de favori, peut-être mélangeant [le projet ActivityStream](https://testpilot.firefox.com/experiments/activity-stream), et le second est un livre, ce qui laisse penser au mode lecture de Firefox, qui consiste en l'utilisation d'un style épuré ne gardant que le texte d'un article. De l'autre côté, le bouton "home" semble de retour, ainsi qu'un bouton de rechargement qui n'est plus intégrer à la barre d'adresse.

J'avoue que pour ma part, j'espère que sera activée en même temps l'utilisation sous GTK3 des décoration côté client sous GTK3.20+, ce qui permettrait d'avoir une headerbar sous Gnome :) Cela permettrait au navigateur d'économiser de l'espace vertical, et d'avoir les boutons de fenêtre intégrés à la barre d'onglet sous tout les systèmes d'exploitation !

Source : [Ghacks](http://www.ghacks.net/2017/03/24/here-is-the-first-mockup-screenshot-of-firefox-57s-new-design/)