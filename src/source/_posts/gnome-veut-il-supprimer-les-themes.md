---
title: GNOME veut-il supprimer les themes ?
tags:
  - GNOME
url: 10.html
id: 10
categories:
  - Libreries
date: 2018-08-24 14:00:00
---

Il y a quelques jour, Sam Hewitt, designer et créateur de themes (ex-createur pour les themes) et d'icones pour GNU/Linux (il est notamment à l'origine du theme d'icone Paper, Moka, Suru, Solus, et à fait des icones pour Elementary et GNOME) et récemment devenu membre de la fondation GNOME à posté un article qui a provoqué des débats : dans cet article, il questionne le problème des themes et des applications dans GNOME. Et il a dit quelque chose qui a provoque la colère des linuxiens, une ire aux conséquences appocalyptique… qu'il ne serait pas contre la suppression des themes.

Quel est le soucis dont il parle ?
----------------------------------

GTK offre la possibilité de donner un theme aux widget en utilisant la technologie CSS, une technologie également utilisée pour gérer le style graphiques des sites web sur internet. Les feuilles de style CSS définissent l'apparence de divers éléments d'une page web, et ici d'une application. Cette possibilité cependant est donnée à la fois aux applications, et à la fois à des themes globaux, pouvant remplacer le theme de base de GTK. Cela peut provoquer des confrontations entre ces deux themes. En effet, si une application modifie son CSS, produit des widgets propre à l'application, certains styles seront appliqué sur des parties de l'applications. Sauf que ce theme peut entrer en contradiction avec un theme global qui n'a pas été testé par le créateur. Le CSS est très puissant et peut permettre de faire des modifications assez important aux applications, notamment des cas qui n'ont pas été pris en compte par le développer de cette application. Cela peut produire des applications qui n'auront pas une apparence parfaite avec certains themes, voir qui seront carrément moins fonctionnelles à cause de cela. C'est en fait un peu une course, où les développeurs de themes doivent supporter toujours plus d'applications (chose que Sam Hewitt à fait, ayant développé des themes par le passé). Le theme de GNOME n'est pas simplement un theme changeable, mais fait partie de la plateforme sur laquelle certaines applications sont développées. Ce problème existe tout particulièrement avec les applications elementary OS, qui ont été développé pour une feuille de style (celle d'elementary), et qui marcherait sans doute mieux avec leur style par défaut, plutôt que d'utiliser le theme "global". L'idée est qui si pour l'instant un theme peux tout gérer avec les quelques douzaines d'applications basée sur la plateforme GNOME, cela serait beaucoup moins gérable avec des centaines. Il estime donc qu'il serait mieux si les applications pouvaient choisir avec quel themes elles sont affichées. Mais même s'il dit qu'il ne serait pas faché sur les themes étaient abandonnés, il faut savoir qu'ayant travaillé avec le projet Elementary (qui utilise son propre theme, comme faisant partie de sa plateforme), il ne parle sans doute pas de les abandonner dans GTK même, mais de laisser plus de pouvoir aux applications pour définir quels themes elles utiliseraient. De plus, il est important de savoir que cet article parlait surtout des themes par défaut de distributions utilisant la plateforme GNOME, et non pas ceux que l'on changerait nous-même. L'avis du projet sur ce sujet est un "si ça casse, vous pouvez gardez les bouts". En effet, si la possibilité est laissée (via GNOME Tweak) de customiser son theme, ce n'est pas officiellement supporté par la plateforme, et ils n'ont alors pas dans leur responsabilité le fonctionnement de tous les themes.

Cet article ne représente pas l'avis de "GNOME" et n'est pas une annonce
------------------------------------------------------------------------

GNOME n'est pas un esprit de ruche à la manière de je-ne-sais-quelle espèce d'extraterrestre de SF. C'est un projet avec des avis différents qui se confrontent et collaborent. L'avis d'un designer ne représente pas l'avis générale de GNOME. Cet article est un article posté sur un blog personnel, et n'engage pas l'avis du reste de la communauté. Le mainteneur du code utilisé par les themes dans GTK à déjà indiqué que ce code ne serait pas abandonné. Divers possibilités ont été réfléchies/évoquées pour répondre à ce questionnement :

*   Tout d'abord, développer une "API Adwaita" qui permettrait de customiser un peu Adwaita pour les distributions. C'est l'une des possibilités proposées lors du hackfest lié aux themes.
*   Faire contribuer les développeurs de themes de distributions à Adwaita, afin qu'Adwaita soit plus attractif pour eux (je pense que ce serait pas mal de rendre Adwaita plus "flat design", un peu comme le travail fait actuellement sur les icones).
*   Indiquer dans GNOME Tweak quand on utiliserait un theme qui n'est pas celui de la plateforme, pour indiquer que les développeurs d'applications ne seront pas forcément responsables de glitch visuels.
*   Laisser aux applications la possibilité de "forcer" un theme avec lequel elles marchent… possibilité qui existe déjà, puisque c'est ce que GIMP fait avec GTK2.
*   Et sans doute d'autres que j'oublie.

Conclusion
----------

"GNOME" ne veut pas supprimer les thèmes et non, reddit, Sam Hewitt ne "hait" pas la customization 😛. Il y a une réflexion sur les problèmes engendré par les collisions entre le theme global et celui des applications, qui n'a pas encore eut de conclusion à proprement parler. C'est un problème assez global sur internet de bondir sur le moindre poste et de le voir comme une annonce, et la preuve qu'un projet a des "intentions néfastes". Parfois, les news c'est qu'il n'y a pas de news, que des gens qui réfléchissent. Et ici voici la news : des gens réfléchissent sur un sujet.