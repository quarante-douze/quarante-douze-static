---
title: 'Unfamous Resistenza : Un pavé dans l''Asphalte 2'
tags:
  - Creative Commons
  - Musique
url: 64.html
id: 64
categories:
  - Contenus culturels
  - Libreries
date: 2014-01-06 18:00:00
---

Le collectif musical Unfamous Resistenza nous a gratifié de l'annonce de leur prochaine compilation numérique en téléchargement gratuit : Un pavé dans l'Asphalte 2, qui au lieu de contenir 100 musique comme le premier en contiendra 150. Décrites comme une compilation « éclectique, engagée, déviante, consciente, subversive » sur la page Facebook du collectif, et sortira en début 2015, la fin des selection étant en septembre 2014.

[Source](https://www.facebook.com/photo.php?fbid=569948369760734&set=a.329970223758551.79400.329573583798215&type=1&relevant_count=1)