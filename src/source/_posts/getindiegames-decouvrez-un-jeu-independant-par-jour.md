---
title: 'GetIndieGames, découvrez un jeu indépendant par jour'
tags:
  - Jeux indé
  - Jeux vidéo
url: 73.html
id: 73
categories:
  - Contenus culturels
date: 2013-12-22 16:46:00
---

Le jeu indépendant est un domaine ou des tas de nouveaux jeux sortent en permanence, et ou on trouve souvent des petites oeuvres courtes, mais passionnantes à jouer. Cependant, il n'est pas toujours facile de découvrir l'entiereté de ce qui se fait de nouveau dans ce monde immense.

[GetIndieGames](http://getindiegames.tumblr.com/) est donc un site utilisant Tumblr qui vous permet de découvrir des jeux indépendant, presque toujours gratuit. Sa particularité est qu'il en poste un par jour, ce qui permet d'avoir son petit jeu indé du jour, et de faire chaque jour une petite découverte.

Bref, un site comme on l'aime, qui a pour but de partager des réalisations venant des internets !