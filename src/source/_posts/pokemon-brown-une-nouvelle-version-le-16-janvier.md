---
title: 'Pokemon Brown, une nouvelle version le 16 Janvier'
tags:
  - Hackrom
  - Pokémon
url: 58.html
id: 58
categories:
  - Fandoms
date: 2014-01-13 16:42:00
---

Pokemon Brown est un hack-rom des jeux gameboy pokémon, qui installe une nouvelle région, une nouvelle aventure, et pas mal de nouveauté graphiques. Ce hack va sortir une nouvelle version le 16 janvier, et un petit changelog ainsi que des screenshot ont été publiés sur le sujet :

*   Ajout d'un nouveau donjon, qui permettra notamment de capturer un certain nombre de pokémons que vous aviez avant à échanger depuis les jeux de la première et deuxième génération. Si vous atteignez la fin, vous aurez un prix spécial, sachant que cependant vous devrez ce donjon vous même, puisqu'il ne vous sera pas dit ou il est.
    
*   Recodage de la structure des images de Pokémon, qui maintenant s'affichent sur des émulateurs Gameboy/GBA de toutes qualités, et sur des GameBoys réelles. Plus de morceaux de l'image qui manque, et plus de bugs graphiques quand vous voyez les stats d'un pokémon lors d'un combat.
    
*   Un peu de recalibrage a été fait, en particulier pour la première partie du jeu. Les sets d'attaques des premières pokémons ont été modifié, et quelque chose a été ajouté dans la _Mergson Cave_ pour rendre le début plus supportable.
    
*   Le support du dash a été ajouté, appuyez sur B pour courir.
    
*   Les palettes de couleurs Super Gameboy ont été mises à jour, et des instructions seront inclue pour savoir comment activer le mode Super Gameboy pour pas mal d'émulateurs
    
*   De nombreuses zones ont été retravaillée, et une bonne partie des bugs de map ont été corrigés.
    
*   Une grande partie des images de la génération 4 ont été réimporté, parce que la plupars rendait mal.
    
*   Plusieurs bugs divers ont été corrigé, ainsi que des erreurs de typo, et des restes de Rouge que le créateur avait oublié de changé.
    
*   Rajout de quelques musiques, par exemple pour les magasins, les musiques de Jotho ont aussi été un peu améliorée.
    
*   Rajout d'un tableau pour expliquer les nouveaux types.
    
*   Ajout d'une nouvelle splash screen au début du jeu.
    
*   Des instructions pour support des terminaux mobiles (téléphones portables, tablettes...)
    
*   Et sûrement d'autres nouveautés !
    

[Source](https://www.facebook.com/permalink.php?story_fbid=454070048028208&id=242973372471211&stream_ref=1)