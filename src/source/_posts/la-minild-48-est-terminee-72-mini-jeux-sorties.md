---
title: 'La MiniLD #48 est terminée, 72 mini-jeux sorties'
tags:
  - Jeux indé
  - Jeux vidéo
  - Lundum Dare
url: 56.html
id: 56
categories:
  - Contenus culturels
date: 2014-01-14 11:00:00
---

Depuis la dernière fois ou on vous à parlé de la Mini Ludum Dare, nous n'avions pas encore les thèmes de cette mini-compétitions de jeux amateurs, mais maintenant elle est terminée (pour rappel, elle se passait du 10 au 12 janvier). Les thèmes sont « façade, schèmes, conspiration, tromperie » et ont engendrés la créations de 72 entrées (même selon l'article, on voit écrit « All Entries (73) ». Quel est ce piège, serait-ce cela la conspiration dont ils parlent ?).

Il n'y aura pas de votes, mais il est recommandé d'aller mettre des commentaires pour aider les créateurs à s'améliorer :) Les jeux sont donc diverses, et ont été faits en 48 heures, comme le veulent les règles des LD. [Vous pouvez donc aller à la pèche aux jeux ici](http://www.ludumdare.com/compo/minild-48/?action=preview), et n'hésitez surtout pas à partager dans les commentaires vos mini jeux préférés !