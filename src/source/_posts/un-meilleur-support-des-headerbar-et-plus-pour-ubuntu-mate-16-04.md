---
title: Un meilleur support des headerbar et plus pour Ubuntu-Mate 16.04
tags:
  - GTK
  - Headerbar
  - Mate-Desktop
  - Ubuntu-Mate
url: 36.html
id: 36
categories:
  - Libreries
date: 2016-03-15 16:17:00
---

Ubuntu Mate a été une des bonnes surprises dans l'écosystème Ubuntu ces dernières années. Le but de cette variante devenue officielle d'Ubuntu est d'offrir aux utilisateurs la possibilité d'utiliser Mate-Desktop, la continuation de Gnome 2 pour ceux qui ont été déçu par les changements apporté dans la troisième version majeure de l'environnement de bureau GNOME. Rapidement, elle s'est mise à offrir des capacités de customisations assez élevé, tout en offrant des moyens simples de le faire.

L'une des dernières possibilités en date était [d'avoir un look à la Unity](http://www.omgubuntu.co.uk/2016/02/ubuntu-mate-16-04-unity-style-desktop), notamment grâce à l'introduction d'un applet pour mate-panel permettant d'avoir un dock intégré au panneau et un offrant la possibilité d'avoir un menu global à la Unity et Mac OS X.

Mais la dernière nouveauté présente est toute simple : [Le support des CSD](http://www.omgubuntu.co.uk/2016/03/ubuntu-mate-16-04-client-side-decoration) (qui permette d'avoir les headerbars, ces hybrides entre une barre de titre et des barres d'outil, un choix de Gnome pour économiser de l'espace vertical, et mettre le contenu au coeur de l'application) va être total : Les ombres sont parfaitement rendue . Les popover sont aussi maintenant rendu correctement. Le support des CSD marche aussi sans compositeur, et naturellement il n'y aura d'ombres dans ce cas. Il est à noter que l'intégration des boutons et des barres de défilement dans les thèmes GTK2 et GTK3 vont être mieux alligné, et les applications Qt 4 et 5 vont avoir leur propriété d'apparence aligné sur les widget GTK.

Tout cela va permettre d'avoir un aspect graphique et un feeling cohérant, ce malgré les différences de boites à outils (GTK ou Qt) ou de design (Gnome HIG, applications traditionnelles...). On peut dire que l'idée est de tenter de concillier le besoin d'une cohérence pour l'environnement de bureau et les choix des applications.

![](https://lh3.googleusercontent.com/fd5Ko9qI8T6tc8Dp-B4T4vTyAagTLSQeb0yjnMnlhcX0ndUs0AzCE303VBALVys2mdMy=w1366-h768-no)

Faisant partie de ceux qui apprécient énormément les applications GNOME et leurs styles graphiques (que ce soit l'usage des CSD ou le minimalisme des applications), je ne peux qu'approuver une telle chose : Cela permet aux applications utilisant des CSD d'être encore mieux utilisable et intégrée dans Mate Desktop.

Mais ce n'est pas tous, et deux autres nouveautés ont été dites par Martin Winpress dans son [post Google+](https://plus.google.com/u/0/+MartinWimpress/posts/aibudHBGxqT) : L'ajout d'une nouvelle version (la version [0.11.0](http://www.webupd8.org/2016/03/lightweight-dock-plank-0110-released.html)) de plank supportant les docklets, des petites applications intégrés au dock, et le support d'un indicateur pour [synapse](https://doc.ubuntu-fr.org/synapse), un lanceur d'application avec des capacités sémantiques.

Avec tout ça, je sens que je vais peut-être essayer Ubuntu Mate 16.04, moi :)