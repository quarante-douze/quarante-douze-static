---
title: 'Locomalito, « pour l''amour, la culture et la goire des jeux vidéo »'
tags:
  - Jeux indé
  - Jeux vidéo
url: 46.html
id: 46
categories:
  - Contenus culturels
date: 2014-05-08 17:45:00
---

[Locomalito](http://www.locomalito.com/index.php) est un créateur de jeux freewares/donationware indépendant, assez intéressant dans sa philosophie liée à la création de ses jeux : Il vise à créer des jeux vidéo « à l'ancienne », comme pas mal de créateur, mais ce qui est surtout intéressant, c'est qu'il a déterminé un certain nombre de points qui sont important dans la création de ses jeux (visible dans [sa page décrivant sa philosophie](http://www.locomalito.com/philosophy.php)) :

→ Ils doivent être possible à terminer en une seule session de jeu  
→ Il n'y a qu'un seul niveau de difficulté, celui du jeu  
→ Il doit y avoir des secrets ingame  
→ Il y a un système de scoring  
→ C'est nous qui devons monter de niveau, pas le jeu.

Evidemment, il dit de manière explicite dans le sous titre de la page que c'est sa manière de voir le jeu vidéo, et donc si vous aimez ce genre de jeux vidéo, vous devriez apprécier les jeux qu'il fait, et au final sans doute même si vous n'êtes pas totalement fan de sa vision, parce qu'on sent qu'il créer des jeux par passions. Quant à ses jeux, ils sont d'inspiration très rétro, mais d'inspirations rétro assez diverses, puisqu'en plus de l'inspiration esthétique des jeux qui est celles de jeux vidéo antédéluviens, il y a aussi l'inspiration des thèmes de ses jeux, et pour voir cette diversité, rien de tel qu'une petite présentations de deux petits projets :

### L'Abbaye des Morts

_[L'Abbaye des Morts](http://www.locomalito.com/index.php)_ est inspiré des vieux jeux de plateforme utilisant des tableaux en écran fixe, inspiration aussi présente chez VVVVVV et surtout [You Have to Win the Game](http://www.piratehearts.com/blog/games/you-have-to-win-the-game/), avec un détail que j'adore dans ces jeux : Le petit titre présent pour chaque tableau. C'est tout bête, mais je trouve que ça a son charme. Mais ce qui est aussi intéressant dans l'Abbaye des Morts est que ce jeu fait une petite référence à une période historique précise, c'est à dire la répression du catharisme par l'église catholique au 13e siècle. Pour tout vous avouer, je ne savais même pas ce qu'était le Catharisme quand j'ai joué au jeu… Mais bref, ce jeu est surtout un jeu de plateforme assez intéressant, assez proche des deux jeux que j'ai cité, j'espère donc que vous allez bien vous amuser avec !

### They Came from Verminest.

Celui là, c'est mon coup de cœur… Pas seulement parce que _[They Came from Verminest](http://www.locomalito.com/verminest.php)_ est un jeux inspiré des vieux soft de shoot them up avec une ambiance parfaitement arcade, mais surtout pour son esthétique inspiré des vieux films de SF/horreur des années 50… D'ailleurs, rien que l'esthétique du trailer est totalement dans cette esthétique film des années 50 !

Il reste cependant d'autres jeux à découvrir sur le site web de ce créateur indépendant, aux gameplays et aux sujets divers et variés