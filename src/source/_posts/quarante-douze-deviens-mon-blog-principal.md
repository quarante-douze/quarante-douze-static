---
title: Quarante-Douze deviens mon blog principal
url: 43.html
id: 43
categories:
  - La vie du site
date: 2014-07-03 16:47:00
tags:
---

J'ai toujours aimé les blogs, l'expression qu'ils permettaient sur internet, et mon envie d'en tenir pleins à la fois a toujours été un de mes gros soucis... En effet, j'ai eut la tendance à m'éparpiller dans tout les sens, a faire pleins de projets, et à ne jamais les terminer ou rester longtemps dessus. Quel est donc le but que je compte faire avec Quarante-Douze ?

Créer un blog plus "libre", dans le sens ou je pourrais y poster sans avoir vraiment de ligne à tenir, ou je pourrais y aller plus selon mes envis, faire des coups de gueules sans me soucier de si c'est le sujet ou pas... Tout en étant sur autre chose qu'un blog "personnel" comme l'était mon blog "Kazhnuz' World", qui faisait un peu mégalo, en y repensant. Le but de Quarante-Douze est donc d'être un espace de reflexion, de considération, assez libre niveaux des sujets explorés, même si vous trouverez surtout sous ma plume des sujets un poil "geek", "militant", et traitant souvent de logiciel libre... On ne se refait pas :p Mais ce n'est pas un blog personnel, et il est ouvert à tous ceux qui seraient tenté par l'idée de construire un truc un peu alternatif et tout. Toujours est-il que pour l'instant, je vais me concentrer sur un but : Écrire des articles. Un nouveau thème graphique sera aussi dans les bacs, même si pour l'instant celui-ci est je pense assez efficace, et pleins de petits missingno.

Quarante-Douze va aussi héberger quelques services, comme un shaarli que j'utiliserais pour partager les articles qui me plaise, et peut-être au fur et à mesure ouvrir quelques services publiques, si ça peut servir ^^