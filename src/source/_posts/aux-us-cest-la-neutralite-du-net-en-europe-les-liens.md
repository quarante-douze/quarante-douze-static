---
title: 'Aux US c''est la neutralité du net, en Europe les liens ?'
tags:
  - Neutralité du Net
url: 17.html
id: 17
categories:
  - Libreries
  - Politique
date: 2018-01-08 16:30:00
---

Je crois qu'il n'y a pas de complot dans le monde, mais des épidémies de foutage de gueule massives (on commence dans la subtilité et la bonne humeur). Pendant qu'aux US c'est la neutralité du net qui a été attaquée, en Europe des lobby veulent "tout simplement" s'attaquer aux liens hypertexte, base du web. À la base de cette nouvelle ? [Cette lettre ouverte](http://www.lemonde.fr/idees/article/2017/12/13/en-matiere-d-information-la-gratuite-est-un-mythe_5229088_3232.html), même si elle n'est pas trop ouverte non plus puisqu'il faut être abonné du monde.

Grossomodo, qu'est-ce que veulent les lobby journalistiques ? C'est assez simple : ils estiment qu'en tant que producteur d'un contenu qui est "utilisé" par des plateformes qui font des liens vers eux, ils leur "fournissent du contenu" et qu'ils devraient donc recevoir du fric pour chaque lien entrant vers eux. Son idée est qu'en matière d'information, la gratuité est un mythe (c'est pas faux), et que leur créateurs paie pour un contenue que des plateforme en ligne utilise ensuite. Ils visent notamment FB et Google, mais dans cette attaque qu'on pourrait croire juste dirigée envers les GAFAMs, c'est à la libre création de liens sur internet qui s'attaque, et c'est un peu génant parce que c'est une des bases du web.

L'article de [Julia Reda sur le sujet](https://quarante-douze.net/) explique très bien à quel point c'est dangereux de s'attaquer aux liens parce que c'est une des bases du web, et je ne pourrais trop vous recommander de le lire.

Le web, c'est une interconnection de différentes pages, site et services entre eux, à travers cet outil fondamental qui est le liens hypertexte. Le Web, ce n'est en fin de compte qu'une série de page avec plus ou moins d'interactivité, avec des liens fait entre ces pages. Tuer ce lien, c'est donc s'attaquer à tout un modèle d'accès à l'info, où plus précisément de partage de source d'accès à l'info.

En effet, l'accès gratuit à l'information, il est pas fait par les plateforme qui font des liens. Ce n'est pas google qui a rendu gratuit les journaux en ligne, ce sont les journaux en ligne eux-même. Je veux dire, on se calme les gens, Google à fait assez de merde pour pas qu'on en rajoute d'autres :'D. Ce que font ces plateforme (et aussi bien des GAFAMs que des services libres et open-source tels que [Searx](https://searx.me/), [Diaspora*](https://diasporafoundation.org/), [Mastodon](https://joinmastodon.org/)…), en mettant un lien n'est pas "mettre à disposition du contenu", c'est indiqué où ce contenu peut-être trouvé sur internet.

D'ailleurs, on peut faire remarquer que le Monde, en publisant ce texte sous forme de texte qui ne peut être lu qu'en payant trouve cela : cela montre que si on ne met pas le texte à disposition, ce ne sont pas les moteurs de recherche et les réseaux sociaux qui le feront.

Julia Reda cite dans son article Prof. Höppner, avocat des éditeurs allemands lors d’une affaire contre Facebook et Google, qui indique bien quel est le but d'un tel projet :

> Ceci est un droit à la prohibition. C’est un droit qui permet de s’assurer qu’il n’y a pas de plateformes qui apparaissent n’importe où, n’importe quand, exploitent des contenus publiés ailleurs et basent là-dessus leur modèle commercial. Le but premier et principal est d’empêcher ces exploitations commerciales – purement et simplement, d’empêcher leur existence.

En visant le lien, dans ce but assumer, c'est donc une attaque contre les principes même du web qui sont fait, au nom d'une volonté de contrôler même la mise à disposition du chemin pour aller vers le contenu.

Maintenant, on peut se poser la question pragmatique : quel serait les effets d'une telle loi ?

Tout d'abord, cette attaque contre les liens pourrait provoquer une isolations des différents services entre eux, et le fait de rendre plus difficile la capacité à commenter, discuter d'un article, d'autant plus quand le but avoué est de pouvoir tuer les modèles économiques basés sur les liens (ou en tout cas de les faire raquer).

Ensuite, ce serait un acte mauvais pour les médias eux-même. Parce que bon, si les plateforme de diffusion décident de pas payer, et bloquent les médias concernés, je pense que ce serait eux qui vont souffrir le plus, pas lesdites plateformes. Je veux dire, si demain, plus aucun lien extérieur ne redirige vers Le Monde, ou Le Figaro, ou n'importe quel autre média, les média concernés vont perdre des tas de lecteurs, et surtout d'autres médias vont en profiter pour prendre la place. Cette volonté isolationniste est une mauvaise idée pour les média aussi, voir pour eux en premier.

J'ai déjà dit dans mon [coup de gueule](https://quarante-douze.net/index.php?article179/neutralite-du-net-petite-mise-au-point) sur les événements qui s'étaient produit autour de la Neutralité du Net que j'avais peur de voir dans le monde d'internet des situations proches de celles du monde du multimédia, avec des trusts qui veulent controler tout les étages de la production. Et j'admet que je me demande si cela ne pourrait pas arriver que dans un tel cas, les Géants du Web ne deviendraient pas intéressé pour financer leurs _propres_ groupes de presses… Et franchement je pense que ce ne serait pas bon, et que cela provoquerait des risques assez énorme niveau hyper-centralisation. (Et je parie que certains signataire de la lettre ouverte seraient les premiers à critiquer cette concurrence déloyal… :') Non pas qu'ils en auraient tors, mais il y aurait un aspect "arroseur arrosé" presque digne d'un vieux cartoon. )

Je pense que cela risquerait encore une fois d'amplifier le phénomène de centralisation des médias, qui existe déjà. Ce serait également un gros soucis pour pleins de petits sites qui n'ont pas la puissance financière d'un FB et tout. Et un dernier soucis que je vois, c'est pour pas mal de "petits journaux" qui risqueraient de raquer si les GAFAMs décident de bloquer tous les média plus "traditionnels" sans discernement, parce qu'ils pourront bien plus difficilement être découvert et diffuser à leur communauté.

Bref, je pense que c'est une bien belle connerie, et qui pourrait donner des résultats assez catastrophiques.