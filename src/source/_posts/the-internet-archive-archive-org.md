---
title: The Internet Archive - Archive.org
tags:
  - Domaine Public
  - Internet Archive
url: 75.html
id: 75
categories:
  - Contenus culturels
  - Libreries
date: 2013-12-18 22:21:00
---

Si certains connaissent plus ce site parce qu'il est ce qui permet d'avoir la Wayback Machine, une véritable machine à remonter le temps des internets, le but de l'[Internet Archive](http://archive.org) est d'être une gigantesque bibliothèque numérique à but non lucratif, constituées de clichés instantanés (copie de pages prises à différents moments) d’Internet, de logiciels, de films, de livres et d’enregistrements audio. C'est une des plus énormes collection que l'on peut trouver sur internet.

Malgré son design austère, on ne peine pas à trouver ce qu'on cherche, même si l'immensité de l'archive fait que l'on peut parfois être déroutée. Diffusant souvent sous plusieurs format différent son contenu quand s'est possible, l'Internet Archive est un must pour son contenu, même si celui-ci est parfois redondant. Pour vous donner une idée de son contenu, au moment de l'écriture de cette description le site indique qu'il y a eut 372 milliard de sauvegarde de pages web (donc parfois la même page sauvegardé à plusieurs moments différents), 1 492 052 vidéos, 5 638 380 textes, 1 807 191 fichier audio et 49 283 logiciels.

Il est a noter que ce site utilise la durée des droits d'auteurs en Amérique du Nord, c'est à dire cinquante ans, ce qui fait que certains fichiers sont peut-être encore protégés en Europe.