---
title: Comment customiser son Firefox pour protéger sa vie privée.
tags:
  - Firefox
  - Vie privée
url: 49.html
id: 49
categories:
  - Internet et numérique
  - Libreries
date: 2014-05-01 18:44:00
---

EDIT : Adblock Plus remplacé par uBlock Origins, pour les raisons suivantes : uBlock Origins est largement plus léger, et AdBlock Plus à tout un modèle économique fondée sur des "publicités acceptable" qui pose un certain nombre de problèmes.

De nombreux articles nous avertissent des dangers liés à l'exploitations de nos données personelles, et notamment de celles que notre navigateur envoie aux différents sites que l'on visite, notamment les possibilités de manipulation du comportement. [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/) est un navigateur web libre et ouvert, et qui est une excellente alternative aux propriétaires Internet Explorer, Opéra et Google Chrome. Les positions de Mozilla en faveur d'un internet libre et ouvert sont d'ailleurs une des grandes raisons pour lesquelle ce navigateur est apprécié de ceux qui veulent un internet plus ouvert à tous. Cependant, si utiliser Firefox est déjà un pas en avant dans le fait de se protéger sur internet, quelques plug-ins sont conseillés pour avoir une vie sur internet la plus libre possible. Comme d'habitude, nous ne sommes surement pas exaustif, et n'hésitez pas à nous corriger ou à partagez des solutions que vous connaissez pour naviguer de manière plus protégée

En premier lieu, un indispensable est [uBlock Origins](https://addons.mozilla.org/fr/firefox/addon/ublock-origin/), qui vous protège des pubs intempestive, qui sont parfois envahissante, omnipresente, qui parfois sont juste des arnaques (pensez notamment aux célèbre pubs « tout les esthéticiens la hait », ou des trucs du genre), voir sont carrément des nids à programmes malveillants. De plus, si vous culpabilisez de ne plus rapporter à des sites l'argent des pubs, vous pouvez désactivé l'add-on site par site.

Une autre chose conseillée est d'utiliser l'add-on [https everywhere](https://www.eff.org/https-everywhere), un addon qui vous fait en sorte que votre navigateur se dirige vers une solution en https, plutot qu'en http dès que possible. L'HTTPS (le s en plus signifiant « secure ») est un protocole de chiffrement de la connection HTTP, pour faire simple, et est censé vous protéger de ce qu'on appelle « les attaques de l'homme du millieu », c'est à dire des interceptions de votre connection.

Ensuite, pour vous protéger du tracking, c'est à dire des scripts javascript ajouté dans des pages qui peuvent tracker votre navigation de page en page, nous proposons deux solutions. Celle que nous préférons est celle proposé par le site Prism Break, [Disconnect](https://disconnect.me/), qui possède l'aventage des pleinement libre, par rapport à Gosthery. Disconnect bloque donc les tracker, ce qui permet de vous protéger de cette méthode là d'obtenir des données sur votre navigation.

L'étape suivante que nous conseillons est [Smart Referer](https://addons.mozilla.org/fr/firefox/addon/smart-referer/), qui configure votre référant ( Définition de Wikipedia : « Un référant, plus connu sous l'anglicisme referer1 ou referrer2, est, dans le domaine des réseaux informatiques, une information transmise à un serveur HTTP lorsqu'un visiteur suit un lien pour accéder à l'une de ses ressources, lui indiquant l'URL de la page où se situe ce lien qu'il a suivi. » ) pour qu'il n'indique d’où vous venez que quand vous restez sur le même nom de domaine (et le bloque donc dès que vous passez d'un site à un autre, de sorte à ce que les sites web ne savent pas d’où vous venez).

Une autre méthode pour tracker l'utilisateur est les cookies, ces petits bouts de données que laisse sur votre ordinateur les sites web. La solution pour résoudre ce problème est simple, et c'est le principe de l'addon suivant, [Self Destructing Cookie](https://addons.mozilla.org/en-US/firefox/addon/self-destructing-cookies/) : Ne laisser exister que les cookies de sites envers lesquels on a confiance (ou dont on a besoin des cookies pour garder notre compte connecté). Self Destructing Cookie ordonnera donc par défaut aux cookies de s'autodétruire à la fermeture de l'onglet ou du navigateur, et vous pourrez choisir des sites pour lesquels ils resteront.

Vous pouvez également vous protéger en rendant aléatoire votre User Agent, c'est à dire les données qui identifie votre navigateur, votre systeme d'exploitation, votre résolution d'écran... L'add-on pour cela est [Random Agent Spoofer](https://addons.mozilla.org/fr/firefox/addon/random-agent-spoofer/), qui fera donc croire aux différents sites que vous visitez que vous utilisez un navigateur différent du votre.

Pour finir, une des solutions, mais pas des moindres : utiliser [NoScript](https://addons.mozilla.org/fr/firefox/addon/noscript/?src=ss) pour choisir sur quel site vous executez ou non le javascript. Attention cependant, de nombreux sites utilisent abondamment le javascript pour fonctionner.

D'autres solutions peuvent exister, tels que l'usage de VPN ou autre, mais ce sera le sujet d'un autre tutoriel surement dans l'avenir. Une dernière chose que nous pouvons conseiller est d'utiliser [un moteur de recherche alternatif](http://beerware-magazine.net/content/logicielsquelques-moteurs-de-recherches-respecteux-de-votre-vie-privee-223) ;-)