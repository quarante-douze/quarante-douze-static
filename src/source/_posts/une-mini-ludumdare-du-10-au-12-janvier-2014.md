---
title: Une Mini LudumDare du 10 au 12 Janvier 2014
tags:
  - Jeux indé
  - Jeux vidéo
url: 63.html
id: 63
categories:
  - Contenus culturels
date: 2014-01-07 12:45:00
---

Amateur de petit jeux indépendant et/ou amateur, bonsoir !

La Ludum Dare, compétition de jeux amateurs fait en temps limité va faire une MiniLD du 10 au 12 janviers 2014, ce qui permettra aux créateurs d'essayer de réaliser un jeu vidéo en 48 heures.

Toutes les informations sont présentes au lien suivant : http://www.ludumdare.com/compo/2013/12/29/minild-48/