---
title: Playlist 14 de Unfamous Rezistenza
tags:
  - Creative Commons
  - Musique
url: 66.html
id: 66
categories:
  - Contenus culturels
  - Libreries
date: 2014-01-06 14:00:00
---

Le collectif Unfamous Rezistenza à sortit une playlist récemment, permettant aux internautes d'avoir accès en téléchargement à gratuit à tout un tas de musiques. Cette playlist nous gate avec 40 musiques, dans des styles divers et variés, plus précisément de 36 morceaux et 4 mix.

Selon la page Diaspora* du collectif, cette playlist comprend les artistes suivants : Gada Tronsi, Agglomeira, Broken Toy, Carlos Lemosh, In Délirium, Dawamesk, Etiket Zero, Germinal, Home Taping, ICD, K(no o), Le Neurone Actif, Loan, DoubleYou See, Mirdym, Monsieur Connard, 1probablemc, Fame, System NO3, Thödol Continuum, Unstatic, M'siou Rigolitch, Dernier Rempart, 6.R.M.E, Abraxxxas, Acid Freaks, Mentalo, Lipid Tron, HNO3, Zergutten Project, Karash Nikol, Otisto23 & Laurent De Wilde, N.rgle, Dead City Riot, Bring To Ruin, Frontierer, Bobé Van Jézu & Sub-altern's, Morgomix, Messkla, DJ Galex

[A l'adresse suivante](http://blog.unfamousresistenza.fr/ecouter/playlist-cadeau/les-cadeaux-dunfamous-resistenza-n14/), vous pourrez trouver la playlist accessible avec un lecteur, un téléchargement de toutes les musiques, et des liens vers les soundcloud de tout les artistes, ce pour le plus grand plaisir de vos oreilles.