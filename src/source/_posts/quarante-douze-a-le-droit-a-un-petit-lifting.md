---
title: Quarante-Douze à le droit à un petit lifting !
tags:
  - Redesign
url: 30.html
id: 30
categories:
  - La vie du site
date: 2016-11-14 21:00:00
---

Le blog Dimension Quarante-Douze à eut le droit à de petites modifications ces derniers temps !

La première de ces modifications est la récupérations de nombreux vieux articles qui viennent de mes différents blogs, puisque j'avais fait un backup de mon blog en Février 2015. Du coup, cela m'a permi de récupérer une partie de ce que j'avais perdu, même si tous les articles publiés entre Février 2015 et le passage sous PluXML restent perdu à jamais.

Mais j'en ai profiter pour passer sous la version 5.5 de PluXML, et de rajouter quelques éléments. Pour cela, je marque un petit changelog

*   Nouveau plugins (PlxMySearch, Gravatar).
    
*   Ajout d'un Wiki et d'un lien vers mon Shaarli.
    
*   Ajout de liens vers des sites que j'aime bien.
    
*   Rétablissement des commentaires.
    
*   Passage de bootstrap-material à paper pour le theme.
    
*   Rajout de la recherche et des catégories dans la sidebar.
    
*   Correction d'un bug, JQuery et le javascript de bootstrap étaient mal chargés.
    
*   Ajout de la date en Calendrier Républicain (système Romme) sur le côté.
    

J'ai également remplacé mon compte github par un lien vers mon compte framagit :D Ou y'a plus de petits codes pertinents.

N'hésitez pas à prévenir de s'il reste des bugs que j'aurais pas remarqué :)