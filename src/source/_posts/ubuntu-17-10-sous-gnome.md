---
title: Ubuntu 17.10 sous GNOME
tags:
  - GNOME
  - Ubuntu
url: 19.html
id: 19
categories:
  - Libreries
date: 2017-09-07 21:59:00
---

Comme vous le savez, Ubuntu a décidé de repasser sous GNOME-Shell pour son interface principale. Cependant, les détails n'étaient lors de l'annonce pas connu, et assez contradictoire entre eux. D'un côté certains pensaient qu'on aurait un GNOME Vanilla, et d'autres espéraient qu'on aurait un clone 1:1 d'Unity utilisant le moteur de Gnome-Shell. Aujourd'hui, le gel de l'ajout de nouvelle fonctionnalité est passé, et de ce fait, on a une vision plus claire de ce à quoi pourra ressembler le futur GNOMEsque de Ubuntu.

Avant de véritablement donner la liste des différences et des nouveautés, je vais commencé par parler de la manière dont la transition a été faite. Le travail sur la session Ubuntu a été réalisé en concertation avec la communauté GNOME, en particulier par des discussions sur quelle était la meilleur marche à suivre pour créer une expérience Ubuntu (notamment à la GUADEC). Des développeurs Ubuntu sont également devenus membres de la fondations GNOME (et ont notamment leurs blogs désormais actifs sur Planet GNOME).

Au niveau technique, le choix s'est porté sur une manière de faire similaire à la session GNOME Classic. En plus des deux sessions connus de GNOME Shell (GNOME et Classic), une troisième session a été ajoutée dans Ubuntu : la session Ubuntu (jusque là, tout semble logique). Cela permettra d'avoir toutes les modifications spécifiques à Ubuntu dans une session à part, sans avoir à modifier la session GNOME. Cela veut dire que toutes les modifications faites par Canonical et l'équipe bureau d'Ubuntu peuvent être contournée par la simple installation de la session GNOME ( `sudo apt install gnome-session` ). Ensuite, les modifications ont été implémenté par :

*   Des patchs au niveau de la distribution (le theme GDM différent)
    
*   Des paramètres différents (les boutons de fenêtres, par exemple)
    
*   Des patchs actifs que dans la session Ubuntu (le theme Ubuntu, par exemple)
    
*   Des extensions activés par défaut dans la session Ubuntu.
    

Au niveau de la version de GNOME, Ubuntu 17.10 utilisera la version la plus récente de GNOME : GNOME 3.26. Cela permettra aux utilisateurs d'accéder aux toutes dernières nouveautés de cette version. Les contributions venant d'Ubuntu dans GNOME ont également augmentées, avec par exemple le travail de Marco Travisan sur le support des écrans à haute densité de pixel dans GNOME et le fait que ce soit désormais Jeremy Bicha qui maintient GNOME Tweaks. Il y a d'autres exemples de patchs fait par des développeurs employés par Canonical et/ou membre de la communauté Ubuntu, mais mon but n'est pas de faire une liste.

L'interface globale : ça ressemble à Unity, mais en différent.
--------------------------------------------------------------

Lorsque l'on voit l'interface globale de Ubuntu 17.10, on remarque une première chose : la présence de l'interface avec une barre horizontale en haut de l'écran, et une dock vertical à droite. Les deux principales différences à première vue avec GNOME sont donc ce dock et l'intégration des icones de status (via le protocole appindicator) sur la barre horizontale.

![Ubuntu 17.10](https://quarante-douze.net/data/medias/new-theme-main-view.png)

_Note : image piquée à Didier Roche, je suis sur mon netbook et ne peux donc pas prendre de screenshot d'Ubuntu 17.10_

Cependant, dès qu'on y regarde de plus près, les différences avec Unity commencent à apparaître, et elle sont nombreuses. Le theme utilisé est toujours Ambiance, et des modifications ont été faites au theme GNOME Shell et au theme GDM afin de créer des couleurs plus proche de celle de la marque Ubuntu.

L'apparence basique est donc retenue, mais le bureau Ubuntu reste fidèle au mode de fonctionnement de GNOME.

Sur la barre verticale, le calendrier et les notifications sont désormais centrées, et le menu global est remplacé par deux boutons : le bouton "Activité" de GNOME (on en reparlera après) et un menu d'application réduit, destinée à affiché les fonctionnalités globale de l'application. Le HUD (qui permettait de chercher rapidement une fonction dans une application en appuyant sur `alt`) n'est également plus présent.

Sur le dock, les barre de progressions et les badges qui indiquaient les nouveaux messages ne sont plus présent, [mais il est prévu de les faire revenir](https://github.com/micheleg/dash-to-dock/pull/590). La poubelle ne se trouve plus sur le dock, et tout en bas du dock se trouve l'accès aux applications supplémentaires.

Le Dash (menu principal) est donc remplacé par deux menus différents : celui des applications, et celui. Dans les deux, on peut trouver l'accès à la recherche des applications, et également à des moteurs de recherches internes fournis par les applications, qui permettent de chercher les fichiers, etc. Contrairement à GNOME cependant, le coin actif qui active la vue d'ensemble des fenêtres est désactivé, pour éviter que les utilisateurs l'active par accident. Le bouton `super` fonctionnera toujours pour l'activer.

Au niveau des fenêtres, elles utilisent désormais des barre d'entête (headerbar), les barre d'outil fusionnée à une barre de titre de GNOME. Les boutons de fenêtre se trouve sur la droite (pour minimiser les différences avec GNOME) et contiennent les boutons minimiser et maximiser (qui sont plus logique puisqu'il y a un dock).

Il y a également d'autres différences, mais j'ai marqué ici ce qui semble être pour moi les plus marquante.

Le choix qui fache : pourquoi forker DtD ?
------------------------------------------

(et l'application pour les appindicator)

J'ai souvent vu des personnes sur les flux de commentaires qui s'interrogent sur les choix de Canonical au niveau des extensions. En effet, au lieu d'installer directement dash-to-dock et l'extension appindicator, ils ont procédés à des forks léger de ces extensions. Qu'est-ce que cela signifie ? Cela signifie que leur fork sont situés dans des branches spécifiques des extensions cités plus haut, avec notamment des identifiants différents (ubuntu-dock et ubuntu-appindicator).

De nombreuses personnes se demandent pourquoi. Est-ce encore un syndrome de _Not Invented Here_ ? Ne serait-ce pas _plus simple_ d'utiliser l'extension Dash-to-Dock ? Pourquoi moi, utilisateur avancé, voudrais-je utilisé Ubuntu-Dock qui à un accès moins facile à la customisation ?

Je vais commencer par la dernière, en reprenant un commentaire que j'avais fait à l'époque sur OMGUbuntu (parce que je suis flemmard). En effet, la première chose à comprendre sur Ubuntu-Dock est ce qu'il est, où plutôt ce qu'il n'est pas : Ubuntu-Dock n'est pas un « meilleurs dock » que Dash-to-Dock. Ce fork n'a pas été fait parce que d-t-d n'aurait pas été assez bien pour Ubuntu. Au contraire, ils l'ont choisi et l'on forké parce qu'il était parfait. Ce dock n'a pas pour but de vous donner envie de remplacer Dash-to-Dock, qui sera toujours le dock « supérieur » avec plus de fonctionnalité. Il y aura très certainement des tas de tutoriel vous disant comment remplacer Ubuntu-Dock par Dash-to-Dock… et ce sera même super simple puisqu'il est prévu que Ubuntu-Dock s'auto-desactive quand Dash-to-Dock est actif ! Et à la moindre nouvelle fonctionnalité désirée par Ubuntu, il vont ajouté la fonctionnalité à d-t-d. La preuve dans le lien que j'ai cité plus haut.

Le but d'Ubuntu-Dock est simple : c'est d'être le dock par défaut de l'expérience par défaut d'Ubuntu. Voilà.

Mais vous me direz, alors pourquoi forker ? C'est expliqué dans l'[article de Didier Roche qui parle du Dock](https://didrocks.fr/2017/08/18/ubuntu-gnome-shell-in-artful-day-5/). Le dock par défaut d'Ubuntu fait partie des paquets par défaut du system d'exploitation. C'est un composant du système, qui doit donc passé par des tests d'assurence de qualité pour être sur que tout marche bien. Hors, il est possible de mettre les extensions GNOME à jours depuis [le dépots d'extension](https://extensions.gnome.org). Ces nouvelles versions s'installent donc dans votre dossier utilisateur, et **ont la prioriété sur celles installées par défaut**. S'ils n'avaient pas fait ce light-fork (pareil pour celui pour les indicateurs), un _composant du système d'Ubuntu_ pourrait avoir des mises à jours qui ne passe pas par les protocoles pour s'assurer de la qualité des paquets.

Ubuntu étant un projet commercial avec des millions d'utilisateurs dans le monde et un bug étant si vite arrivé dans une mise à jour, c'est donc un choix tout a fait logique et pertinent.

Ma conclusion
-------------

Quand Unity a été abandonné, j'admet que j'ai été triste. Non pas tellement pour Unity 7 en lui même (il était condamné à terme), mais pour Unity 8 et les téléphones Ubuntu. Cependant, je dois dire une chose : je suis pour l'instant très content avec le travail fait par les équipes de Canonical sur le sujet. Ils ont collaboré avec les projets upstreams (que ce soit GNOME où même les extensions). Là, récemment, j'ai découvert qu'ils avaient travaillé avec les équipes d'engagement de GNOME sur le sujet de la documentation pour le passage de Unity à GNOME.

Certes, je ne dis pas que c'est sans défaut : Le fait de ne pas avoir le coin actif activé par défaut fait que cela rend impossible de déplacer un élément en drag-and-drop vers une application sur un autre espace de travail. Je suis également en désaccord avec certains choix de Canonical, tel que de ne pas activer Tracker par défaut (j'estime que c'est un outil vraiment utile qui accélère les recherches). Et j'aimerais bien qu'Ubuntu 18.04 utilise un theme plus moderne inspirée de Unity8, comme le theme GTK [United](https://github.com/godlyranchdressing/United-GNOME/) et le theme d'icone [Suru](https://snwh.org/suru).

Cependant, pour une transition dont la décision n'a été prise qu'en Avril dernier, je trouve que le chemin parcouru est vraiment pas mal, et que la collaboration avec les projets du libre (un point que l'on a toujours reproché à Canonical) est vraiment bonne.