---
title: 'Terminix devient Tilix, pour des questions de droit d''auteur'
tags:
  - Application GTK
url: 24.html
id: 24
categories:
  - Libreries
date: 2017-03-20 08:46:00
---

Tilix (anciennement Terminix) est un émulateur de terminal en tuile pour GNOME, qui respecte ses Human Interfaces Guideline, et y ajoute des fonctionnalités très utiles, tels que le fait de pouvoir diviser l'interface en section, dans un système de tuile à la manière de certains gestionnaires de fenêtre comme i3 ou awesome-wm.

![une screenshot de terminix](https://camo.githubusercontent.com/3178e6b3193f24471bd60f48b6faba15e0536b71/68747470733a2f2f676e756e6e312e6769746875622e696f2f74696c69782d7765622f6173736574732f696d616765732f67616c6c6572792f74696c69782d73637265656e73686f742d312e706e67)

J'aime beaucoup cet émulateur de terminal, que je trouve mieux intégré à mon système (un gnome-shell sous Fedora) que le terminal par défaut de Gnome... C'est pour dire ! C'est avec [FeedReader](https://jangernert.github.io/FeedReader/) (un lecteur de flux RSS pour GNOME) et [Lollypop](https://gnumdk.github.io/lollypop-web/) (un lecteur de musique) un de mes logiciel GNOME third-party préféré (il me manque juste maintenant un logiciel gérant XMPP qui respecte les HIG Gnome, ahah).

Cependant, le projet Terminix à reçu une notice d'infraction de copyright par la société Terminix International, qui commercialise des insecticide, notamment contre les termites. Il a donc du changé son nom en Tilix, et la sortie de Tilix 1.5.4 sert en grande partie à acter ce changement (avec quelques corrections de bugs).

Les paquets et les dépots vont surement petit à petit changer de nom, donc pour en savoir plus, je vous conseille de rester connecter [au site officiel de Tilix](https://gnunn1.github.io/tilix-web/).