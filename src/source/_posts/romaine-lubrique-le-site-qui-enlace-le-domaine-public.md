---
title: 'Romaine Lubrique, le site qui enlace le Domaine Public'
tags:
  - Domaine Public
url: 71.html
id: 71
categories:
  - Contenus culturels
  - Libreries
date: 2013-12-26 14:51:00
---

Le Domaine Public est souvent considéré par les défenseurs du libre comme un de nos droits fondamentaux.

En effet, soixante-dix ans (cinquante dans d'autres pays) après la mort d'un auteur en France, son oeuvre deviens - sauf exception - la propriété de tout un chacun, elle devient propriété de l'humanité.

[Romaine Lubrique](http://romainelubrique.org/) est un site qui a pour ambition de vous donner un accès à se Domaine Public, à le promouvoir, à le faire partager. Vous y trouverez de la littérature, de la musique, des films, de la danse, des photographies...

N'hésitez pas à y faire un tour, pour découvrir ce que nos ancêtres nous ont legué, cette fois au sens littéral du terme !