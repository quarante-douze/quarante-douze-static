---
title: Le Revenu de Base Inconditionné
tags:
  - Revenu de Base
  - Social
url: 76.html
id: 76
categories:
  - Politique
date: 2013-12-03 12:04:00
---

Après que JCfrog ait un peu expliqué pourquoi l'effet positif que pouvait avoir le fait de faire des trucs user-friendly et sympathique pour une cause, la graphiste Jennifer Aouizerat à fait une petite présentation sympa du sujet. Je vous met donc la première image du post, qui sert de liens vers la BD. C'est simple, court, efficace, et bien plus agréable que le site officiel sur le revenu de base, ah ah :) Il y a même un joli bouton qui vous amène à la pétition sur le sujet, qui est par contre un poil plus "officielle" que celles dont vous devez avoir l'habitude si vous appréciez change.org ou avaaz ^^

![La BD de Jennifer Aouizerat](http://66.media.tumblr.com/e5f27f86e816d54ff2fe0d4e30649726/tumblr_inline_mwrqzyDKX61r5th97.jpg)

_Cette image est extrète d'une présentation du revenu de base sur le tumblr de Jennifer Aouizerat, sous licence [Creative Common BY-NC-ND](http://creativecommons.org/licenses/by-nc-nd/3.0/fr/). Pour accéder à l'article complet qu'elle a fait, [cliquez simplement ici](http://pixnart.tumblr.com/post/67945662258/le-revenu-de-base-cest-pas-pour-les-poissons-mais) :)_

* * *

J'aimerais rajouter quelques détails à ce que dis la blogueuse, cependant ^^ La question est de rappeller pourquoi en quoi le Revenu de Base à un interet pour la société, dans son ensemble. Il faut se rappeller une chose simple, notre époque est placé sous le signe d'une baisse du travail et des intermédiaires. Pourquoi ? Parce que certains travaux sont des travaux faits pour des machines, c'est à dire qu'ils n'exploitent que le corps de l'homme, et aucune des caractéristiques de son esprit, contrairement aux métiers liés aux services, aux emplois de conceptions, à l'artisanat (qui demande une certaines créativité) et aux métiers comme plombier, cuisinier, qui demandent un savoir, une certaine capacité d'improvisation...

Au contraire de ces métiers, certains métiers nécessitait d'utiliser l'homme comme une machine, n'étaient qu'une réification de l'homme. Du coup, s'il n'y avait pas le problème de l'emploi, la disparition des emplois pénibles ne serait pas une mauvaise chose. Si cela ne les plongeait pas des gens dans la misère, la baisse du nombre d'emploi faisant de l'homme une machine serait une bonne chose. Sauf qu'entre se retrouver SDF et être une machine une partie de sa journée, le choix est vite fait. Combiné au fait que selon les droits de l'hommes, tout homme à le droit à une vie décente, et bien on peut se dire que le revenu de bases inconditionné est une solution, surtout qu'en plus, il a l'aventage de permettre de rebondir vers autre chose sans trop de problème, sans le stress de la paperasse à quelqu'un qui perds son emploi.

Je rajouterais à cela que la confusion entre "emploi" et "travail est assez grave, allez vous dire que tout les bénévoles qui ont contribués à des centaines de logiciels libre (dont surement au moins un que vous utilisez) n'ont pas travaillé ? Comme le dit JCFrog, [on serait parfois plus utile au chomâge qu'avec un emploi...](http://jcfrog.com/blog/je-serais-tellement-plus-utile-au-chomage-emploi-hasbeen/).