---
title: Sonic the Comic Online
tags:
  - Sonic the Hedgehog
  - Webcomic
url: 62.html
id: 62
categories:
  - Contenus culturels
  - Fandoms
date: 2014-01-08 16:53:00
---

La saga Sonic the Comics, débuté par la société britannique Fleetway en 1993 fut l'une des deux grandes saga de comics basé sur la célèbre mascotte de SEGA, basé sur la variante "occidentale" de l'univers du hérisson bleu, et dépourvus d'éléments provenant de la série animé surnommée Sonic Satam. Elle s'est cependant arrêté officiellement en 2002 avec le numéro 223, après divers problèmes (il n'y avait notamment plus de nouvelles histoires depuis le numéro 184).

En 2003, [une suite à été débuté par des fans](http://www.stconline.co.uk/), même si d'anciens membres du staff de l'ancienne série ont participé à la créations des nouveaux comics, si bien que désormais, la série fête ses 20 ans depuis la fondation du premier projet par Fleetway et en est rendue au numéro 262 (soit 39 numéro créé par les fans). Ces fancomics ont comme particularité de développer un univers assez unique, mettant en scène quelques personnages originaux en plus du cast Soniciens.

Pour ma part, je ne suis pas un grand fan de Sonic the Comic, son univers et son ambiance me plait bien moins que celle des comics Sonic the Hedgehog publié encore aujourd'hui par la société Archie Comics. Cependant, j'aime bien certaines idées de Sonic the Comic Online, comme par exemple leur traitement du concept de l'Iblis Trigger de Sonic 2006.

En plus de cela, un résumé des épisodes précédant est présent sur le site, pour que vous ne soyez pas perdu ;-)