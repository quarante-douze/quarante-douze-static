---
title: Quelques moteurs de recherches respecteux de votre vie privée
tags:
  - Moteurs de recherches
  - Outils numériques alternatifs
url: 59.html
id: 59
categories:
  - Internet et numérique
  - Libreries
date: 2014-01-13 01:38:00
---

Si nous sommes sur un site parlant principalement de culture accessible gratuitement, nous songeons également au moyen d'utiliser cette culture. En effet, nous adorons Internet non pour ses lolcats et ses meme ( bon, okay, on avoue, on aime bien ça aussi :p ), mais pour toute cette culture, toutes ces informations qui sont accessibles sur le web et encore plus sur internet. Mais évidemment, pour avoir ces informations, il faut pouvoir y accéder et les utiliser, et c'est pour cela que nous parlons aussi de temps en temps de logiciels. Et une des étapes pour trouver quelque chose, c'est la recherche. Nous pouvons certes trouver par hasard parfois des choses qui nous plaisent, mais souvent nous avons à le chercher. Et il s'avère qu'avec Prism, il y a eut une certaine pertes de confiance envers le mastodonte Google.

Voici donc six moteurs de recherches alternatifs plus respectueux de votre vie privée... Et on vous rassure, on n'a mis ni Bing, ni Yahoo :)

#### 1 - Mysearch

MySearch est ce qu'on nomme un logiciel qui récupère les résultats à partir de différents moteurs de recherches, faisant de lui un méta-moteur de recherche. Utilisant Google, Wikipedia, Google Video, Google Images et OpenStreetMap pour obtenir ses résultats, il a comme première particularité de tourner en local assez facilement, grâce à python : il vous suffit de le télécharger, de le lancer et d'accéder à l'adresse http://localhost:60061/ pour l'utiliser !

[Cliquez ici pour accéder pour en savoir plus](http://codingteam.net/project/mysearch)

#### 2 - Searx

Comme notre logiciel précédant, Searx peut être utiliser en local, est open-source, et est un méta-moteur de recherche. Il reste sur d'autres point plus puissant, notamment parce qu'il utilise plus de sources, à savoir : Google, Bing, Yahoo, Duckduckgo, DDG Definition, Currency, Startpage, Urbandictionary, Twitter, Piratebay, Youtube, Dailymotion, Vimeo, GitHub, StackOverFlow, Soundcloud, Deviantart, Flickr... Yep, cela permet un bon recoupage de résultat. Encore en développement, des fonctionnalités se rajouteront au fur et à mesure.

Vous pouvez essayer ses résultats [ici](http://searx.0x2a.tk/), le télécharger [là](https://github.com/asciimoo/searx), et si vous le téléchargez, n'hésitez pas à consulter [le wiki, pour connaitre comment installer ce logiciel ;-)](https://github.com/asciimoo/searx/wiki/Installation)

#### 3 - Yacy

Yacy n'est pas un méta-moteur de recherche, mais un moteur de recherche open-source fonctionnant sur le mode du Peer to Peer, c'est à dire qu'au lieu d'avoir des résultats stockés sur un serveur central, ce moteur de recherche partage les sources de résultats depuis les nombreux utilisateurs. Ce mode de fonctionnement fait qu'aucune censure n'est théoriquement possible, ce qui en fait un moteur avec un très fort potentiel, ce qui est malheureusement terni par la lenteur du moteur de recherche (devant se connecter sur les nombreux peers) et le manque de pertinence des résultats.

Vous pouvez [cliquer ici pour en savoir plus sur ce logiciel](http://yacy.net/fr/)

#### 4 - StartPage et Ixquick

Ces deux moteurs de recherches accessibles en ligne sont édités par la même société, le premier étant un crawler spécialisé pour rendre les résultats de Google, le second étant un méta-moteur de recherche. Les deux services possèdent comme particularité de passer les résultats par un proxy pour aider votre anonymisation, ce qui fait que Startpage est souvent le moteur de rechercher par défaut des navigateur utilisant TOR, pour faire double-sécurité.

Cliquez ici pour accéder à [Startpage](https://startpage.com/) et à [Ixquick](https://www.ixquick.com/)

#### 5 - Duckduckgo

Duckduckgo est un moteur de recherche confidentiel, sans bulle filtrante (c'est à dire que les résultats sont les mêmes pour tous), il est également doté d'une interface dépouillée, son but étant de remplir son rôle, et de bien le remplir. Ce site est très apprécié pour sa politique de confidentialité, ainsi que ses quelques outils qu'ils proposent, mais possède comme défaut de donner des résultats moins pertinent que les sites utilisant les résultats de Google, étant donné qu'il produit ses propres résultats. Cependant, il est à remarqué que Duckduckgo étant présent aux États-Unis, et ne donnant au final qu'une promesse quand à nos données personnelles, on peut se demander si utiliser un véritable moteur de recherche libre n'est pas une meilleurs solution...

Cliquez ici pour accéder à Duckduckgo.