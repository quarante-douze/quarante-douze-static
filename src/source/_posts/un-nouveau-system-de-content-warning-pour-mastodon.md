---
title: Un nouveau system de "content warning" pour Mastodon
tags:
  - Mastodon
url: 29.html
id: 29
categories:
  - Internet et numérique
  - Libreries
date: 2017-01-26 10:50:00
---

[Mastodon](https://github.com/tootsuite/mastodon), l'implémentation alternative de GNU Social continue d'étendre le protocole afin de permettre des nouvelles possibilités. Il y a quelques temps, un véritable système de blocage avait été implémenté (cependant GNU Social n'est pas (encore ?) compatible avec cet ajout, ce qui en fait une grosse limite), ce qui permettait de bloquer les comptes indésirables (une fonctionnalité qui serait bien sur Diaspora*).

Récemment, la nouvelle fonction implémenté par Blackle est un système de content warning : Vous pouvez masquer le contenu du post et indiquer pourquoi ce contenu est masqué. Cela rend ce système très flexible, parce que comme vous indiquez _pourquoi_ vous le masquer, il peut servir aussi bien pour des spoilers d'oeuvres, pour des triggers warnings (le fait de prévenir quand le contenu d'un post est sur un sujet qui peut être dur pour des personnes, par exemple celleux qui ont subit un traumatisme), ou des contenus _Not Safe For Work_. Une [vidéo du système](https://www.youtube.com/watch?v=jQOapNNn8F0&) (lorsqu'il était encore en développement) en action.

C'est donc un petit ajout très intéressant, et c'est le genre d'initiative qui aide des réseaux sociaux à devenir des espaces plus sains :)