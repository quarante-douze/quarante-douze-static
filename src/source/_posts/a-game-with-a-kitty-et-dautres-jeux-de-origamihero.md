---
title: A Game with A Kitty (et d'autres jeux de OrigamiHero)
tags:
  - Jeux indé
  - Jeux vidéo
url: 61.html
id: 61
categories:
  - Contenus culturels
date: 2014-01-08 18:03:00
---

Vous voulez un jeu avait un personnage qui roxxe, qui est puissant, qui est fort, qui est beau, qui sens bon le sable chaud ? Bon, ici vous n'aurez pas ça, en effet, [A Game with a Kitty](http://www.origamihero.com/ori6/) vous propose de jouer un chat bleu un peu rondouillard.

Reprenant le principe des vieux Mario, doté de principes très rétro, cette série de jeux constitué de 6 jeux principaux (avec sur la même page quelques jeux bonus du même créateurs, plus ou moins proches) sont des jeux de plateformes assez divers et variés, présentant des scénarios pas vraiment liés entre eux, avec pour presque tous un côté rétro, proche de l'époque 16 bits et de jeux comme Super Mario World. Cependant, A Game with A Kitty 6 est un jeu 3D, inspiré de plateformer tridimentionels comme Super Mario 64.

Note : A Game with a Kitty 3 utilise Adobe AIR, contrairement aux autres.