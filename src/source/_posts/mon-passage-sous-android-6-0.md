---
title: Mon passage sous Android 6.0
tags:
  - Android
  - Outils numériques alternatifs
url: 32.html
id: 32
categories:
  - Libreries
date: 2016-11-05 21:38:00
---

Suite à la tragique et mystérieuse disparition de mon ancien et fidèle smartphone, me voilà avec un Cubot Rainbow. Sous Android 6.0, je n'ai pu que remarquer la présence de plus en plus forte de Google dans ces smartphones. De ce fait, j'ai fait un petit peu de ménage afin de pouvoir avoir un téléphone comme il me convient. Ce guide n'est pas un guide de dégooglisation complète (je n'ai pas rooté mon téléphone), ni de libération complète (j'utilise le Humble Store qui est non-libre). Voyez ça uniquement comme une présentation de personnalisation que _moi_ j'ai fait, lié à _mes_ idées :)

De ce fait, cela veut aussi dire que pas mal des personnalisations que je montre sont personnelles, et son dûes à mes gouts (j'ai un widget Calendrier Révolutionnaire, tout comment j'en ai un sous Gnome-Shell) :p Cependant, je pense que certains éléments peuvent être utiles si quelque chose vous déplait dans votre téléphone.

Les applications Google
-----------------------

La première chose que j'ai fait à été d'ouvrir Chrome pour aller télécharger le magasin d'applications libre [F-Droid](https://f-droid.org/). J'ai ensuite désactivé toutes les applications Google (sauf les services Google Play et une application nommé "Appli Google" qui ne sont pas désactivable), et les ai remplacé par des équivalent libres.

Je donne donc la liste des applications désinstallées, et leur remplacement quand j'en ai utilisé un.

*   Google Play -> Fdroid (plus d'autre applications qu'on verra plus tard).
    
*   Google Chrome -> [Firefox](https://f-droid.org/repository/browse/?fdfilter=Firefox&fdid=org.mozilla.firefox). J'utilise pour l'instant une [petite appli](https://f-droid.org/repository/browse/?fdfilter=Firefox&fdid=de.marmaro.krt.ffupdater) pour le mettre à jour, j'hésite à passer au canal [Aurora](https://www.mozilla.org/en-US/firefox/channel/#developer), qui supporte les mises à jour auto.
    
*   Google Drive -> [Owncloud](https://f-droid.org/repository/browse/?fdfilter=OwnCloud&fdid=com.owncloud.android) \* Google Maps -> [OSMAnd~](https://f-droid.org/repository/browse/?fdfilter=OSM&fdid=net.osmand.plus))
    
*   Google Hangouts -> [Conversations](https://f-droid.org/repository/browse/?fdfilter=conversations&fdid=eu.siacs.conversations) (XMPP)
    
*   Youtube -> [SkyTube](https://f-droid.org/repository/browse/?fdfilter=SkyTube&fdid=free.rm.skytube.oss)
    
*   Gmail -> [K-9 Mail](https://f-droid.org/repository/browse/?fdfilter=K-9&fdid=com.fsck.k9)
    
*   Google Photos
    

À mon grand étonnement, il n'y avait pas d'application Google+, je n'ai donc pas eut à la désactiver. J'ai mis cependant une application [Diaspora*](https://quarante-douze.net/), même si ce n'est du coup pas vraiment pour la "remplacer" :p

Après cela, j'avais donc en grande partie un téléphone qui me convenait, et qui était adapté à mon côté libriste extrémiste que c'est de sa faute si y'a des terroristes #JPNeySealofQuality. Il me restait cependant un terrible ennemi, quelque chose qui était là, devant moi, et qui jamais ne semblait vouloir partir : **La barre de recherche Google** _(jouer ici un bruit d'orage)_

Le Launcher Google Now
----------------------

On arrive à ce qui a été l'élément qui a mis un peu de temps (parce que je connaissais pas du tout les nouveauté d'Android 6.0) avant d'être résolu, et sans doute de la raison numéro 1 pourquoi j'écris cet article : Cette barre de recherche Google, présente par défaut. Sur mon ancien téléphone, j'avais un Android datant d'avant l'arrivée de Google Now. De ce fait, j'ai été un peu surpris en apprenant que la barre Google n'était pas un widget, et n'était pas possible à enlevé. Après avoir fait quelque recherche, j'ai appris que c'était parce que le Launcher de base était carrément une application Google, la fameuse « Appli Google » !

Bien évidemment, moi une barre de recherche pour un service que je n'utilise pas, pour un moteur de recherche que je n'utilise pas non plus, j'en voulais pas. Du coup, pour ne pas l'avoir, il faut installer un Launcher alternatif. Parmi les possibilités, il y a tous ceux disponible sur F-Droid, à une exception : Launcher3. En effet, ce Launcher à un défaut, étant celui par défaut des version d'Android avant l'arrivée du Launcher Google Now, il possède déjà du code pour activer la barre de recherche Google Now si l'appli Google est activée – et l'option dans les menu pour ne pas l'activer n'est plus présente.

Je vous laisse découvrir les autres launcher de F-Droid, mais perso ils me convenaient pas (même s'ils sont loin d'être mauvais). J'avais envie d'un launcher proche de celui de base, moderne, FOSS, mais qui n'avait pas la barre Google Now. . C'est suite à une recherche web que j'ai trouvé mon bonheur, sur le forum XDA-Developer : Un port pour Android 6.0 de [Trebuchet](http://forum.xda-developers.com/android/apps-games/app-cm13-trebuchet-laucher-rom-6-0-1-t3350696), le Launcher par défaut de CyanogenMod.

Je l'ai donc téléchargé, installé et mis par défaut, et pour être sûr que la barre Google ne s'affiche pas, je suis allé dans les paramètres, et j'ai mis à off la fonction "Barre de Recherche". Afin de mieux récupérer la place laissée par la barre Google j'ai changé la taille de la grille de 4x4 (Confortable) à du 5x5 (Cosy). Ensuite, j'ai remplacé la barre de recherche Google Now par la [barre de recherche DuckDuckGo](https://f-droid.org/repository/browse/?fdfilter=DuckDuckGo&fdid=com.duckduckgo.mobile.android), et j'ai rajouté un widget [calendrier révolutionnaire](https://f-droid.org/repository/browse/?fdfilter=French+Calendar&fdid=ca.rmen.android.frenchcalendar) (paramétré pour qu'il utilise les équinoxe pour le calcul des jours, comme l'extension Gnome).

Evidemment, ce n'était pas totalement parfait : Si je garde appuyé trop longtemps le bouton homme, ce #@£$% de Google Now vient me casser les pieds, et j'ai tendance à confondre l'action pour afficher les fenêtres et celle-ci. Mais avec le temps je vais m'y faire, et c'est mieux que d'avoir la barre Google omniprésente.

EDIT : En fait, j'ai trouvé comment régler cela, il suffit d'aller dans : Paramètres > Applications > Bouton Engrenage > Applications par défaut > Assistance et saisie vocale > Application d'assistance > Aucune. On peut également choisir une autre application qui remplacera celle Google si on en a d'installé :)

We need to go deeper
--------------------

J'ai également rajouté quelques applications et app-store en plus, afin de renforcer mon expérience utilisateur. J'ai installé [Amaze](https://f-droid.org/repository/browse/?fdfilter=Amaze&fdid=com.amaze.filemanager), un explorateur de fichier plus intéressant que celui que j'ai par défaut, c'est [Silence](https://f-droid.org/repository/browse/?fdfilter=Silence&fdid=org.smssecure.smssecure), le successeur de SMSSecure qui gère mes SMS. J'installerais sans doute par la suite d'autres applications en ligne de services web que j'utilise, ou suivant mes besoins :).

Et même si F-Droid est super, je trouve qu'il est parfois un peu limité, même niveau des applications libres. De plus, je joue à quelque jeux propriétaires. J'ai donc installé deux stores supplémentaires : L'application [Humble Bundle](https://www.humblebundle.com/app), qui me donne accès à mes jeux mobiles acheté sur le site. Truc tout bête, mais j'aime bien :) J'ai aussi télécharger l'application [XDA-Lab](http://forum.xda-developers.com/android/apps-games/labs-t3241866), qui me donne accès à pleins de trucs, j'ai même pas encore fini de faire le tour tellement c'est énorme. Cet application m'a notamment permis d'installer VLC dans une version plus récente que celle disponible sur Fdroid. Nice :) Il est cependant à noter que ces deux applications nécessite des comptes.