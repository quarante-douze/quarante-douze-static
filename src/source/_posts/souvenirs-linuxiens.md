---
title: Souvenirs linuxiens
tags:
  - GNOME
  - GNOME2
  - GTK
  - Linux
  - MATE
  - Souvenirs
url: 20.html
id: 20
categories:
  - Libreries
date: 2017-08-25 22:30:00
---

Aujourd'hui, Linux à 26 ans. Je fais donc un petit article où je parle de l'évolution de mon utilisation de Linux :D

En fait, parler de ma "découverte de Linux" est compliqué : j'ai toujours connu l'existence de Linux : Mon père était utilisateur de logiciel libre, et membre du groupe d'utilisateur de Linux de ma ville - je me souviens qu'il m'y emmenait quand j'étais petit. Il utilisait SuSE globalement, même si je me souviens d'avoir trouvé des CD Red Hat et Ubuntu en rangeant des cartons xD Je me souviens l'avoir vu utiliser plusieurs bureau, dont je pense [Window Maker](https://en.wikipedia.org/wiki/Window_Maker). Je crois ausis l'aovir vu utiliser du [GNOME 1](https://en.wikipedia.org/wiki/File:GNOME-escritorio-1.x.png), et je suis certain pour du [GNOME 2](https://upload.wikimedia.org/wikipedia/commons/d/d9/Gnome-screenshot-full.jpg).

![Window Maker](https://upload.wikimedia.org/wikipedia/commons/f/fa/Wmaker-0.80.2.png)

> Window Maker. Je me souviens particulièrement bien de l'icone avec le truc noir et blanc, je crois que c'était l'icone qui m'intriguait le plus :')

Pour ma part, j'ai commencé à l'utiliser en 2008, j'avais 14~15. Il s'agissait très certainement d'un Ubuntu 7.10 ou 8.04. Je saurais pas dire par contre exactement lequel des deux c'était, vu que ça dépend de quand je l'avais installé. J'avais pas fait de partionnage de mes disques et tout, j'avais utilisé un utilitaire pour cela : [Wubi](https://en.wikipedia.org/wiki/Wubi_(software)). Cela permettait d'installer Ubuntu à côté de Windows, sans avoir à partitionner son disque. J'ai eut au départ pas mal de soucis de carte WiFi, qui m'ont fait arrêter de l'utiliser la première fois.

J'ai retenté tout au long de mon année de troisième et durant le lycée, cette fois en utilisant [le tutoriel](https://openclassrooms.com/courses/reprenez-le-controle-a-l-aide-de-linux) du Site du Zero (aujourd'hui OpenClassroom), notamment pour les partitions. Parmi les éléments que j'ai rapidement aimé dans Ubuntu, ce sont les dépots de logiciel. Je pouvais installer pleins de logiciels depuis ma distribution, youhou ! Cependant, à chaque fois, je le lançais surtout une ou deux fois, sans vraiment l'utiliser. Je faisait un peu de Super Tux dessus, mais je revenais souvent à Windows.

Un autre élément que j'ai adoré rapidement (et que j'apprécie encore), c'était le fait de pouvoir télécharger et utiliser des thèmes, sans avoir à patcher des fichiers comme sous Windows. J'adorais déjà sous Windows XP modifier le thème de mon ordinateur (pareil pour les themes Winamp et autres trucs du genre), mais sous Linux c'était tout un nouvel univers de thèmes et d'icones customs qui s'offrait à moi !

![Ubuntu à l'époque](https://upload.wikimedia.org/wikipedia/commons/b/bc/Ubuntu-7.10-default-screenshot-800x600.png)

À un moment pendant mon lycée (je pense alors que j'étais en première, peut-être avant, durant les vacances), mon père m'a installé sur un disque-dur externe OpenSuse, me permettant de me lancer plus profondément dans Linux. Je me souviens notamment que c'est sur cette machine que j'ai écrit les premiers chapitres de ma première tentative de saga, 3E8. À l'époque, le premier chapitre devait utiliser comme protagoniste Anaku, qui travaillait comme serviteur dans une therme (une idée que je vais reprendre dans Galdr). J'avais commencé à écrire ce chapitre en revenant d'un voyage à Rome, durant l'année scolaire 2009~2010. D'ailleurs, j'avais perdu ses chapitre et ne les ai retrouvé qu'en même temps que mon disque-dur externe récemment, ce qui avait lancé la réécriture du chapitre 1 d'une manière différente.

Ce que j'aimais bien avec cet OpenSUSE, c'était que j'avais plus de connaissance et moins de reticence à tenter de le découvrir en profondeur. J'ai commencé à l'époque à faire un peu de ligne de commande. Et évidemment, j'avais adoré choisir mon theme :D Pour vous donnez une idée de à quoi ressemblait mon bureau à l'époque, voilà (c'est pas une screen d'époque, c'est une reconstruction dans une machine virtuelle en réutilisant les mêmes themes et fond d'écran).

![Ce à quoi ressemblait mon opensuse](https://i11.servimg.com/u/f11/09/00/10/23/vieuxt11.jpg)

> Thème (gtk2 et metacity) : Crux. Theme d'icone : GNOME. Le fond d'écran est un vieux fond d'écran triskel que j'avais trouvé et recoloré sous GIMP.

Ensuite, j'ai perdu lors de mon déménagement en 2011 le disque dur avec mon opensuse dessus, et j'ai pas réutilisé Linux avant la fin de ma licence 2 en 2013, ou j'ai installé sur mon PC portable un [Linux Mint](https://linuxmint.com/) sous Cinnamon. À noter qu'avant, j'utilisais plus windows mais MacOS pendant un temps, sur un PC parce que niklapoliss. Je suis passé ensuite de version de Linux en version de Linux, ayant essayé des dérivés d'Ubuntu et différents bureaux (et pas mal d'entre eux, j'ai essayé Cinnamon, [Mate](http://mate-desktop.org/), [XFCE](https://xfce.org/), [KDE](https://kde.org), et bien d'autres…).

En plus de la possibilité de pouvoir changer de theme, c'était le fait de pouvoir changer de bureau entier qui s'ouvrait à moi xD Après, cela avait aussi un défaut : je passais mon temps à faire ce qu'on appelle du distro-hopping, c'est à dire que je changeais de distribution Linux tout les 2 à 3 mois.

À l'époque, je n'étais pas trop fan de GNOME et Unity, n'aimant pas leur expérience utilisateur qui sortait de celle "windows-like" qu'offraient les autres environnements de bureau, je faisais notamment partie de ceux qui estimaient que GNOME était "trop différent". J'estimais que les interfaces traditionnelles étaient plus adaptés aux besoins des utilisateurs, à la fois des utilisateurs avancés (parce que plus simple à customiser) et à celles des utilisateurs novices (parce que plus reconnaissable). J'ai maintenant un avis un peu différent sur le sujet.

C'est également vers cette époque que j'ai commencé à arrêter d'utiliser Windows, pour utiliser uniquement Linux que ce soit pour le travail où pour jouer. J'ai également petit à petit commencé à utiliser Linux pour d'autre utilisations : Sur mon serveur et sur des Raspberry Pi pour me construire des petite consoles à émulation avec [Lakka.tv](http://lakka.tv).

J'ai ici une petite screenshot de mon bureau en 2014, sous XFCE :

![Mon bureau en 2014, sous XFCE](https://i58.servimg.com/u/f58/09/00/10/23/screen10.jpg)

> Xubuntu 14.04 avec le thème [Numix](https://numixproject.org) et les icones Numix-Circle, les barres du bureau modifié pour rappeller à la fois Gnome 2 (avec une barre en haut et une en bas) et Unity (avec les launcheur dans la barre sur le côté, cette barre se cachant quand la souris n'est pas dessus). Le thème conky est le theme seasheel ou un truc du genre, présent par défaut dans le logiciel Conky Manager, et le theme Audacity est le thème par défaut.

Cependant, j'ai décidé un jour de tester si GNOME était aussi horrible que cela à l'utilisation, et j'ai installé Ubuntu GNOME sur mon PC, il y a environ deux ans. Depuis, j'ai quasiment uniquement utilisé GNOME, que ce soit sous Ubuntu GNOME ou sous Fedora. J'ai rapidement aimé ses concepts (minimalisme des interfaces mais avec un système d'extension puissant).

![Mon bureau aujourd'hui](https://orig00.deviantart.net/84ac/f/2017/237/b/1/capture_d_cran_de_2017_08_25_23_07_47_by_kazhnuz-dbladfb.png)

> Ubuntu GNOME 17.04 sous GNOME 3.24. Les theme d'icone, du shell et des application sont Pop par System76, et le fond d'écran vient de Freedom Planet

Aujourd'hui, j'utilise plus qu'Ubuntu GNOME, et bientôt je passerais sous Ubuntu puisqu'Ubuntu va désormais utiliser GNOME Shell comme bureau principal. Ce mouvement vers Linux s'est accompagné d'un mouvement plus global de passage au logiciel libre chez moi, en particulier à partir des leaks du projet PRISM et du début de mon questionnement personnel sur le modèle économique basé sur la publicité : J'ai désactivé [les appli Android de mon smartphone](https://quarante-douze.net/index.php?article7/mon-passage-sous-android-6-0), et j'ai commencé à utiliser des logiciels différents pour les services en ligne, etc.

Cependant, même si ça s'accompagne dans un mouvement de choix de vie, je n'ai pas envie de faire de "l'évangélisme" où d'autres trucs du genre. Déjà, jouer aux témoins de Jehova avec "avez vous entendu parler de Linux" ça m'intéresse pas, ensuite j'estime que les gens sont assez grand⋅e⋅s pour faire ce qu'iel veulent. (et puis ayant beaucoup d'amis travaillant dans l'art ou produisant de l'art à titre de loisir, je sais qu'il y a certaine utilisation où ces pas possible).

Y'a aussi un peu de flemme, pour être tout à fait honnête.

Pour moi, Linux est passé de ce système d'exploitation différent que mon père utilisait sur son PC, et qui m'intrigait énormément quand j'étais petit, à un élément de ma vie quotidienne. Pareil pour le logiciel libre, Richard Stallman, etc. Avant, pour moi tout cela faisait partie "des trucs compliqué d'adulte" dont mon père parlait. Avant, c'était quelque chose de mystérieux, que j'avais envie de découvrir. D'une certaine manière, c'est presque dommage que ce soit devenu aussi quotidien : une certaine forme de magie a disparu.

Peut-être qu'un jour, quand je parlerais de tout ça, cela semblerait être des trucs compliqué d'adulte à un enfant, qui sera curieux et voudra en découvrir plus ?