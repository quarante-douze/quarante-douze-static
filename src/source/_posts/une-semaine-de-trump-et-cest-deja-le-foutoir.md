---
title: 'Une semaine de Trump, et c''est déjà le foutoir.'
tags:
  - Trump
url: 27.html
id: 27
categories:
  - Politique
date: 2017-01-27 11:40:00
---

Ce qu'on entendait souvent avant l'éléction de Trump, c'était le fameux "nan mais il ne va pas pouvoir faire des mal, les institutions démocratiques l'empêcherons de faire ses conneries"... Et bien, semblerait que non. Dès la première semaine, il a mis le paquet. Plutot que de faire un gros article et tout, je vais juste faire une liste non-exhaustive de trucs qu'il a fait, pour bien montrer _pourquoi_ on avait peur, et pourquoi non, il ne va pas "améliorer" les choses.

Une semaine de trump, c'est :

*   Il prépare la suppression de la "Obamacare", la loi qui permettait à des tas d'américains d'avoir accès à une assurance maladie. Aucune surprise là dessus, c'est surtout que ça va être très dur pour les bénéficiaires...
    
*   Idem pour l'immigration et le mur à la frontière : Il prépare bien tout ça. Il était cependant toujours persuadé que le Mexique va se plier, [étant même allé jusqu'à dire que s'il ne voulait pas payer, le président Mexicain pouvait "annuler sa visite"](http://www.liberation.fr/planete/2017/01/26/trump-au-president-mexicain-payez-le-mur-ou-annulez-votre-visite_1544216). D'ailleurs, il semblerait que le président Mexicain ait annulé sa visite et que le président américain ai comme projet de financer le mur [avec une taxe sur les importation mexicaine](http://www.liberation.fr/planete/2017/01/26/trump-veut-financer-le-mur-par-une-taxe-de-20-sur-les-produits-venus-du-mexique_1544352).
    
*   La [suppression des pages](http://tempsreel.nouvelobs.com/rue89/sur-le-radar/20170124.OBS4242/avant-apres-le-site-de-la-maison-blanche-version-donald-trump.html) relatives au changement climatique, aux droits civiques, aux LGBT, des handicapés et aux Natives Nations.
    
*   Un décret [supprimant le financement des ONG qui soutiennent l'IVG](http://www.lessentiel.lu/fr/news/monde/story/30843194) (interruption volontaire de grossesse). Très étrangement, ce décret a été signé qu'en présence d'hommes... Les Pays-Bas ont réagit [en lançant un fond international pour ces associations](http://www.aufeminin.com/news-societe/trump-la-meilleure-reponse-des-pays-bas-au-decret-anti-ivg-s2127126.html).
    
*   Les oléoducs ont été approuvés, le forage des gaz de schistes aussi. Parce que l'environnement aussi, il en a rien a faire.
    
*   La [censure des grandes agences scientifiques](http://www.numerama.com/politique/227209-trump-musele-les-scientifiques-des-administrations-environnementale-et-agricole.html), qui ont dû créer [des comptes twitters alternatifs](https://quarante-douze.net/) pour organiser une "résistence scientifique". Numérama maintient [une liste pour s'y abonner plus facilement](https://twitter.com/Numerama/lists/r-sistance-scientifique/). Peut-être que tout ceux qui parlent de "censure" quand il y a un consensus scientifique sur un sujet apprendront peut-être avec ça ce qu'est une vrai censure... ?
    
*   Il veut [établir une liste des crimes](http://www.directmatin.fr/monde/2017-01-26/trump-va-faire-publier-la-liste-des-crimes-commis-par-les-immigres-747565) (et délit, puisqu'en anglais on utilise le mot crime aussi pour les délits) commis par toutes les personnes d'origines étrangères dans le pays. Une bonne méthode de manipulation et de chercher des boucs émissaires, en equivant rapidement le fait que les crimes ont des tas de sources multiples. Un bon exemple classique à cela est celleux qui sortent la réthorique des « étrangers qui violent les femmes blanches », en ommettant de dire que la plupart des viols sont commis par quelqu'un de proche que connait la victime. C'est pourquoi on peut utiliser une information pour désinformer : quand elle est partielle, il est possible de déformer la réalité en en montrant qu'une partie.
    
*   Dans [ses ordres sur la "sureté publique"](https://twitter.com/cobun/status/824398742275104768), il demande _explicitement_ à toutes les agences fédérales que leurs politiques de respect de la vie privée exclue les non-américains et les résidents permanents des protections offertes par le Privacy Act. Si par défaut ces protections ne concernaient que les citoyens américains, des agences les avaient étendues à tout le monde.
    

Cela nous prouve une chose : On avait de bonne raisons de redouter cette éléction. Nous avons ici des exemples d'atteintes à la liberté d'expression et d'informations, aux droits des femmes et des minorités, à la vie privée et à l'environnement. Je suis assez sidéré que des gens qui se disent "anti-impérialistes" sont près à le soutenir alors qu'il montre à quel point il est près à s'attaquer aux libertés civiles, et à quel point il l'avait déjà dit. D'ailleurs, récemment il a dit que Chelsea Manning n'aurait jamais dû être libéré de prison (rappellons aussi qu'il avait dit que lui président, Snowden aurait été ramené aux US en quatrième vitesse).

Cependant, il ne faut pas se contenter de faire ce constat, mais plutot réfléchir à ce que cela veut dire. Ici, je vais parler plutot de comment nous, qui ne sommes pas citoyens des Etats-Unis pouvons réagir. Certaines de ces idées peuvent être faites par des citoyens US, mais ne parlera pas à leurs soucis les plus urgents.

On peut voir déjà des réactions aux actions de Trump, ce depuis son arrivée : [L'Internet Archive](http://archive.org) avait réagit en déclarant [dupliquer ses données au Canada](http://tempsreel.nouvelobs.com/rue89/rue89-nos-vies-connectees/20161130.RUE5916/pour-se-proteger-de-trump-la-memoire-d-internet-se-duplique-au-canada.html) (même si j'espère qu'il vont rendre redondant leurs informations à travers plusieurs pays). Les comptes twitters non-officiel fonctionnent en cachant le nom de ceux qui le font pour les protéger de l'administration Trump. La réaction des Pays-Bas face à sa suppression des aides pour les associations soutenant l'IVG est très saine. En créant un fond international, on s'affranchit des Etats-Unis dirigés par Trump. Il faut espérer que d'autres initiatives du genre se forment.

On remarque un point commun dans ces trois décision : Il s'agit de faire en sorte de ne pas trop déprendre de l'administration de Trump voir des États-Unis, afin d'essayer de baisser l'influence que la politique de Trump pourrait avoir. Evidemment, dans le monde mondialisé qu'on a aujourd'hui, on ne peut pas éviter à 100% la politique de Trump. Cependant, il est possible d'éviter de trop trop dépendre de ces humeurs. D'une certaine manière, on revient à la question du status d'hyperpuissance des US.

Sur les questions de censures et de vie privée, je pense que c'est là que des projets tels que [Dégooglisons Internet](https://degooglisons-internet.org/) et que la communauté scientifique internationale peuvent permettre de réagir : Il est possible de décentraliser les données, d'éviter de nous rendre trop dépendant de silos, et des risques de censures. Comme on a proposé l'utilisation des réseaux sociaux décentralisés et du logiciel libre comme moyen de combattre la censure et la surveillance en Turquie ou en Russie, cela peut aussi permettre de la combattre aux US.

Cependant, il faut se rendre compte que ce n'est qu'une partie du soucis. Mais on peut déjà faire quelque chose sur ce point-là.

**EDIT au 30/01/2017** : Et maintenant on a le MuslimBan. Les résidents de sept pays musulmans ont été interdits d'accès à l'entrée aux Etats Unis. Une décision est purement pour aller dans le sens du poil des bigots, puisque certains pays dans lesquels les US ont des intérêts économiques. Trump sacrifie les réfugiés (et toute la communauté musulmane au racisme qu'il amplifie de par sa recherche de boucs émissaires à tous les problèmes du pays) sur l'autel de la bigoterie.