---
title: Linux Mint présente les X-Apps
tags:
  - Linux
  - Linux Mint
url: 37.html
id: 37
categories:
  - Libreries
date: 2016-02-02 14:32:00
---

Un des soucis avec les environnements de bureaux basé sur Gnome, mais voulant avoir un style plus "traditionnel" à souvent résidé dans la question des "headerbar" et des nouvelles Human Interface Guidelines du projet.

En effet, cette bar fusionnant la barre d'outil et la barre de titre possède un style bien particulier, qui tranche radicalement avec le look traditionnel des applications, ayant une barre de titre, une de menu et une barre d'outil. Ces applications sont parfois aussi vues comme n'affichant pas assez les différentes options que permet le logiciel, voir en retirant de temps en temps. De même, avec les temps, les applications ont eut leur style de plus en plus intégré aux idées de Gnome-Shell, pouvant sembler "alien" dans d'autres environnements

La solution trouvé par Ubuntu a été de patcher ces applications, dans le but de maintenir une barre de titre. Cependant, ces patch provoquent parfois des éléments bizarre, comme la présence d'une barre de titre et d'une headerbar. De plus, les autres éléments des Human Interface Guidelines de Gnome, comme l'aspect plutot minimaliste, restent souvent là. Jusqu'à sa version 17, Linux Mint utilisaient ces applications, ou parfois de vieilles versions des applications Gnome (par exemple, gedit était toujours downgradé à la version 2.30). Maintenir des patchs temporaires était donc couteux en temps et en moyens, et les applications devaient soit être à nouveau, soit être gardé à des versions antérieur sans vraiment pouvoir évoluer.

Pour finir, ces applications provoquaient donc des dépendances à des éléments de Gnome, et utiliser celles du projet Mate, provoqueraient des dépendances à des éléments de Mate-Desktop.

La solution réside alors donc dans la création des X-Apps, des forks des applications Gnome ou Mate, dont le but est de créé des applications génériques en GTK3, n'étant lié à aucun environnement de bureau. Ils trouvaient que créer des "Applications Cinnamon" n'auraient aucun sens, d'autant plus que du coup la question se pose pour différents environnement de bureaux (Cinnamon, Mate et XFCE principalement). Elle peuvent donc du coup être utilisé dans tous ces environnements de bureau, notamment pour Linux Mint qui peut décider de les déployer à travers ses différentes saveurs, ce qui permet d'avoir les mêmes applications, quel que soit son environnement de bureau. Elles auront également des noms génériques, comme "éditeur de texte".

Deux X-Apps ont été révélée : Xedit, basé sur Pluma, la version MATE de Gedit et Xplayer, basé sur Totem 3.10.

![](http://www.linuxmint.com/tmp/blog/2985/thumb_xedit.png) On peut notamment deviner l'intéret que cela pourrait avoir pour des distributions comme Xubuntu, et pour tout ceux qui veulent utiliser des applications GTK avec un style "traditionnel".

Un autre grand aventage sera pour ceux appréciant le look natif des applications Gnome tout en voulant un bureau linux aussi customisable que Cinnamon, qui pourront alors utiliser les applications Gnome non-patché, même si cela se verra sans doute bien plus sous LMDE, qui ne contiendra donc sans doute pas les applications Gnome patchée par Canonical. C'est donc un choix que je vais suivre avec intéret, et je me demande ce que Mate va faire à propos de cela.

Cependant, je me demande si le défaut de la présence de dépendance à MATE est aussi fort qu'il est dit. Je pense que ce serait dans l'intéret de tout le monde que XFCE, Cinnamon et Mate travaillent en commun sur un projet d'applications en GTK3. Une telle coopération ferait du bien aux trois environnements, à mon humble avis. Source : [Le blog de Linux Mint](http://blog.linuxmint.com/?p=2985)