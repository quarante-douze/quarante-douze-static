---
title: 'Flight of Pigasus, un petit shmup Master System sympa'
url: 183.html
id: 183
categories:
  - Non classé
date: 2018-12-06 08:41:16
tags:
---

Le monde des homebrew est un monde que j’apprécie beaucoup, notamment avec sa pelletée de petit jeu sympa, offrant une expérience cool en plus du défi technique de développer pour une console ancienne.

Et Flight of Pigasus est un de ces petits jeux sympathiques, qui méritent qu’on s’attardent dessus pour le fun qu’ils vont nous procurer. Développé pour la SEGA Master System, une console pour laquelle j’ai tout particulièrement de l’affection, ce jeu à un concept très simple, tout comme sa description sur le site [smspower](http://www.smspower.org/) : « Vous êtes Pigasus, le cochon volant. Choisissez soit le mode 2 ou 5 minutes et tentez de réaliser le meilleur score possible. »

![Un écran titre, montrant "Flight of Pigarus", et présentant les deux options principales : "2 minutes game" et "5 minutes games".](https://www.quarante-douze.net/wp-content/uploads/2018/12/image1.png)

L’écran titre du jeu

  

Ce jeu est donc un shoot-them-up coloré, où votre cochon volant tirant des lasers affrontera tout un bestiaire animalier, se battant vaillamment contre vaches et poulets, dans des graphismes démontrant parfaitement les capacités de la Master System, profitant à la fois des couleurs vives et des 2 palettes de 16 couleurs de la console. J’ai une affection toute particulière pour le robot-vache, et les boss ressemblant au Dark Matter de Kirby.

Au niveau gameplay, c’est du shmup. Vous devez tout détruire, obtenir les bonus. Choix sympa, le jeu ne possède pas un système de vie limité, mais d’upgrade-downgrade. Chaque coup vous fera perdre un niveau d’arme, chaque bonus vous en fera regagner un. Si vous êtes touché à votre dernier niveau d’arme, la partie se termine. Ce choix permet de rendre le jeu plus accessible en rendant moins redoutable le fait d’être touché : si comme dans de nombreux shmup être touché vous fait perdre vie et puissance, c’est de manière moins importante parce que vos chances sont moins limité (vous pouvez remonter votre nombre de vie) et que vous ne perdez qu’un niveau d’armement.

![Un petit cochon qui tire des lasers.](https://www.quarante-douze.net/wp-content/uploads/2018/12/image3.png)

Boom

  

Bref, Flight of Pigasus est un petit jeu sympa, créé spécialement pour une console que j’aime beaucoup. Pour moi, du tout bon ! Petit bonus, il y a même un manuel fourni avec, et une fausse boite, tout aussi… _Master System_ que l’étaient les vrais, ahah !

Source des images et lien vers le jeu : [SMSPower](http://www.smspower.org/Homebrew/FlightOfPigarus-SMS)