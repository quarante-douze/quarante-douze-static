---
title: 'La TIC-80, une console fictive libre'
tags:
  - Console fictive
  - TIC-80
url: 15.html
id: 15
categories:
  - Contenus culturels
  - Libreries
date: 2018-07-11 17:31:00
---

La [Pico-8](https://www.lexaloffle.com/pico-8.php) est sûrement la plus connue des "consoles fictives", ces programmes informatique ayant pour but d'être des plateformes de développement de petits jeux, avec des limitations techniques souvent inspirés des consoles rétro. L'intérêt principale de ce genre de console, et notamment de la pico-8 est d'offrir une spécification rétro et des outils dédié de développement (utilisant notamment le langage de programmation Lua, assez populaire pour sa simplicité), permettant à des amateurs de créer des petits jeux sympathique assez facilement. Si la Pico-8 est très [apprécié et possède une communauté active](https://github.com/felipebueno/awesome-PICO-8), elle n'est cependant pas open-source, et le kit de développement (et logiciel pour y jouer autrement que sur le web) est payant. Même si une tentative est faite d'offrir un lanceur compatible open-source avec le projet [PicoLove](https://github.com/gamax92/picolove) (mais qui ne fournira pas les outils de développement : pour créer des jeux pico-8 il vous faudra toujours acheter le kit de développement), je vais ici vous parler d'une tentative d'alternative : la [TIC-80](http://tic.computer). La TIC-80 est donc un clone de la PICO-8, libre et open-source, disponible sur les plateformes Windows, OS X, Linux et HTML5. Comme son modèle, elle propose un certain nombre de limitation pour les jeux, limitations que voici :

*   **Affichage** : 240×136 pixels, palette de 16 couleurs
*   **Controlleurs** : 2 manettes de 8 boutons, souris
*   **Sprites** : 256 sprites d'avant-plan 8×8 pixels, et 256 tuiles d'arrière-plan de 8×8 pixel
*   **Carte** : 240×136 cases, ce qui fait une taille de 1920×1088 pixel.
*   **Son** : 4 canaux.
*   **Code** : 64 KB de Lua, Moonscript ou Javascript.

Par rapport à la Pico-8, on peut remarquer que plus de langages de programmations sont supportés. Au niveaux des [jeux fait avec](https://tic.computer/play), on peut remarquer la présence de bien moins de jeux, et que la console HTML5 semble plus lagguer que la Pico-8. Cependant, la TIC-80 reste un projet intéressant, et un moyen sympathique d'apprendre à programmer un jeu. Ce qui serait particulièrement intéressant pour moi serait que des runners de consoles fictives soient ajouté à des projets de méta-émulateurs tel libretro, cela permettrait à ces consoles d'être mieux connues et à plus profiter des projet qu'on peut y trouver.