---
title: 'Le Roman de Goupil le Renard, une continuation libre du Roman de Renart'
tags:
  - Domaine Public
url: 65.html
id: 65
categories:
  - Contenus culturels
  - Libreries
date: 2014-01-06 17:30:00
---

_Note : le media dont je parle a été retrouvé grâce à la Wayback Machine de l'Internet Archive :) Le projet n'est donc plus en cours_

Dans la famille des petits concepts amusants que j'ai trouvé sur internet, je demande le [Roman de Goupil le Renart](https://web.archive.org/web/20100112135136/http://wikimaginaire.free-h.org/index.php/Cat%C3%A9gorie:Le_Roman_de_Goupil_le_renard).

Le Roman de Goupil est une série de récits pour enfant en continuation (plutôt libre) du Roman de Renart, jouant sur le fait que Renard soit devenu le nom commun pour parler de cet animal, tandis que goupil n'est désormais quasiment plus utilisé. Restant à la fois plus ou moins dans l'esprit de l'époque, mais avec une modernisation certaine et un aspect enfantin étant donné le public visée, les récits prenant plus l'aspect de contes pour enfant.

Il y a 8 récits dans le roman de Goupil, et je trouve que c'est le genre de petite initiative amusante qui permet de faire vivre le domaine public :)