---
title: Parlons un peu de portails et de fancréation
tags:
  - Jeux vidéo
  - Portail
  - Vidéos
url: 48.html
id: 48
categories:
  - Fandoms
date: 2014-05-03 10:28:00
---

**Note : Cet article est un ancien article écrit pour le site Beerware-Magazine. Suite à un certain nombre de soucis de BDD, ce site avec l'ancien quarante-douze ont fermé. Heureusement, j'avais fait une exportation qui est donc reprise ici.**

Bonjour, et bienvenue dans cette seconde chronique de Beerware-Magazine (EDIT : et désormais de quarante-douze :p) : Les Multivers des Fandoms. Le principe de cette chronique est de vous présenter ce que peut permettre la création amateur, et ce premier article sera un peu spécial, puisqu'il s'agit un peu de présenter de manière un peu généralement la création amateur. Mais pour cela, nous allons demandez l'aide d'une série de deux jeux magnifique : Portal et Portal 2. Donc vous allez devinez que nous allons faire d'une pierre deux coup : Faire ce premier article général, et parler de la fancreation sur Portal.

Donc, prenez vos companion cubes, vos portal guns, et allons-y ! Une fancreation est une création basé sur une œuvre préexistante, généralement faite par un fan de cette oeuvre, dans le cadre de ce qu'on appelle un fandom, c'est à dire une communauté ou un ensemble de communauté réunissant les fan de cette œuvre. La fancreation peut prendre des formes très divers, et si les deux plus connus sont les fanfictions et les fanarts (allez, premier cadeau portalesque : de magnifiques fanart steampunk de Portal : [Steampunk Portal](http://risachantag.deviantart.com/art/Steampunk-Portal-207145877), [The Gentlemanly Escort Cube](http://risachantag.deviantart.com/art/The-Gentlemanly-Escort-Cube-207538713), [The Gentry Turret](http://risachantag.deviantart.com/art/Steampunk-Portal-The-Gentry-Turret-366770320), [Wheatley Dumbwaiter Service](http://risachantag.deviantart.com/art/Wheatley-Dumbwaiter-Service-209207059) !), vous devinez que tout les types de médias peuvent servir de média à une fancreation. Nous allons donc vous présenter quelques types de fancreations :

Les deux autres exemples que je vais vous présenter de fancreation sont les fangames, les fanfilms et les fanmusics.

Les fanfilms peuvent être de formes très diverses, mais il s'agit généralement de format court, parce que faire des longs métrages demande énormément de travail, et même un court métrage, cela peut en demander pas mal, d'ailleurs. Les fanfilms peuvent avoir pour but d'adapter l'oeuvre dans le format cinématographique. Un fanfilm peut être filmé, comme pour le court métrage [Portal : No Escape](https://www.youtube.com/watch?v=4drucg1A6Xk), ou alors fait en animation, comme ce petit gag nommé « [The End of the World](https://www.youtube.com/watch?v=nrl-i14kUcw) », [le clip de cette fanmusique de Metal Gear Solid refait par les Tourelles de Portal](https://www.youtube.com/watch?v=GUXjtTJORPk) ou [Meets the Core](https://www.youtube.com/watch?v=YlFXCC4Hfa0). A noter que parfois, l'animation peut utiliser le moteur du média d'origine quand il s'agit d'un jeu.

En effet, nous allons maintenant parler des fanmusiques, qui peuvent être des covers (comme ces cover de la chanson final de Portal (Still Alive) [celui par AMATORY4](https://www.youtube.com/watch?v=BjIsV8oe37U), [celui utilisé dans le jeu Portal Prelude](https://www.youtube.com/watch?v=hYjTaSXyQ9s) [ou celui des missFlag](https://www.youtube.com/watch?v=jQTQRBu0pLE)), des parodies de chansons populaires (comme celles de, dont tout les clips sont des animations assez sympa) ou des chansons originales. Parmi les parodies de chansons transposée dans l'univers de Portal, vous pouvez trouver des vidéos de [la chaine youtube de Harry101UK](https://www.youtube.com/channel/UCUJXm3LMFLSEe_A2IBf8GwQ), dont [This is Aperture](https://www.youtube.com/watch?v=JZIVmKOdrBk), [If I Were a Core](https://www.youtube.com/watch?v=4U_RvUYINpo) ou [The Wheatley Song](https://www.youtube.com/watch?v=Di2wDDwxqHg&list=TLvNJduzqlF3B4es99Vg5-qfyLXHschYVu).

Les fancreations peuvent être également des jeux vidéo, et ces fangames peuvent être des remakes (lorsque le média de base est lui-même un jeu), mais sont souvent lorsque le jeu est un tantinet élevé niveau des technologies des démakes, c'est à dire qu'ils refont le jeu de manière moins « moderne », c'est à dire en le passant de la 3D à la 2D (comme le fait pour notre exemple le jeu [Portal : The Flash Game](http://portal.wecreatestuff.com/portal.php)), voir même le faire dans des styles plus rétro comme le 8bit voir en ASCII Art, c'est à dire sans graphismes avec que des caractère de texte pour faire l'image (comme le fait le jeu [ASCII Portal](https://cymonsgames.com/asciiportal/))

![](https://cymonsgames.com/games/asciiportal/asciiportal3.gif)  
_Ça, c'est du rétrogaming._

Mais il est possible de viser la création d'un jeu dans la continuité (qui peut être un préquel ou une suite), comme le fait le jeu [Portal Prélude](http://www.portalprelude.com/) (qui est terminé mais n'est plus maintenu officiellement, ce qui peut provoquer quelques soucis à l'installation…) qui se situe avant le premier Portal, plus précisément au moment de l'activation de GladOS dans le passé. Ce jeu est également un mod, c'est à dire une modification du jeu original. On a tendance à parler de mod pour quand le mod modifie un jeu natif et nécessite donc l'achat de ce jeu natif, et de hackrom quand il s'agit d'une modification d'un fichier rom à lire en émulation.

En bref, après tout ça, nous n'avons qu'effleuré l'univers de la fancreation, et sans doute même de la fancreation sur Portal. Si vous avez des fancreations sur Portal que vous connaissez, n'hésitez pas à les partager dans les commentaires, cela permettra aux lecteurs de découvrir encore plus de fancreation sur l'univers de Portal !