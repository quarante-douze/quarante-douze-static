---
title: Servo (le nouveau moteur de rendu de Mozilla) sera testable en juin
tags:
  - Firefox
  - Servo
url: 35.html
id: 35
categories:
  - Libreries
date: 2016-03-16 09:53:00
---

[Servo](https://github.com/servo/servo) est le prochain moteur de rendu de Mozilla, développé en Rust (un nouveau langage de programmation travaillé pour ce projet, destiné notamment à améliorer la vitesse d'execution et la parallélisation des tâches, ainsi que la sécurité). Si le projet existe depuis un moment déjà, il n'est pas encore très facile à tester sans compiler les sources, il n'y a pas encore eut de build sortit au public et téléchargeable.

Mais cela va changer : Servo sera testable en juin, avec [browser.html](https://github.com/browserhtml/browserhtml), un navigateur web utilisant Servo, écrit entièrement en HTML + CSS. Est-ce que cela veut dire que nous aurons le "successeur de Firefox" en juin ? Non, et pour plusieurs raisons. Tout d'abord, il faut bien comprendre que ce ne sera qu'un premier test d'un moteur de rendu encore loin d'être terminé. Il s'agit d'un travail très long et complexe, qui en plus ne peut pas recevoir toute l'attention, puisque Firefox à aussi besoin d'avancer à côté. De même, rien ne prouve que Browser.html sera le navigateur de mozilla en Servo : il ne s'agit que d'un navigateur experimental.

![](https://raw.githubusercontent.com/browserhtml/browserhtml/master/browser.gif) D'autant plus que l'interface de browser.html est encore loin de supporter les toolkits et les rendus natifs sur les différents systems ou il pourra tourner. Toujours est-il qu'avoir un premier aperçu de ce navigateur next-gen sera sans doute assez intéressant :)

Sources : [mozilla.dev.servo sur Google Groups](https://groups.google.com/forum/#!topic/mozilla.dev.servo/dcrNW6389g4)