---
title: Quelques petites listes de jeux par Wikipedia...
tags:
  - Jeux vidéo
  - Wikipedia
url: 70.html
id: 70
categories:
  - Contenus culturels
  - Libreries
date: 2013-12-28 23:46:00
---

Les jeux vidéos gratuits, il y en beaucoup en cavale sur le net, et les recenser est une chose ardue, qui nécessite de l'entraide et du partage. C'est pour cela que l'encyclopédie libre Wikipédia s'avérait être le médium idéale pour tenter une liste de jeux gratuits, avec quelques projets filles que nous allons vous présenter. **La version en liens n'en contient qu'une partie, c'est à dire ceux non open-source et qui ont été diffusé gratuitement dès le début**. Les autres versions sont cependant tout de suite disponible depuis la page, mais nous allons vous mettre les liens accessibles quand même depuis ici.

Cette liste de jeux diffusés gratuitement contient des jeux diverses, dont parmi les notables le célèbre I wanna be the Guy. Parmi la liste des jeux anciennement commerciaux ressortit en freeware, vous pourrez trouver des classiques comme R-Type, ou les deux premier Elder Scroll.

[Liste de jeux open-source](https://en.wikipedia.org/w/index.php?title=List_of_open_source_video_games)  
[Liste de jeux commerciaux ressortit gratuitement](https://en.wikipedia.org/wiki/List_of_commercial_games_released_as_freeware)