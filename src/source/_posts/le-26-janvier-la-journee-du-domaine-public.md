---
title: 'Le 26 Janvier, la journée du domaine public !'
tags:
  - Domaine Public
url: 54.html
id: 54
categories:
  - Contenus culturels
  - Libreries
date: 2014-01-25 18:36:00
---

Le domaine public que une chose importante de la culture gratuite et libre, puisqu'il s'agit de toutes les oeuvres qui sont accessible à tous parce qu'appartenant désormais à tout le monde, faisant partie entière et sans limite de la culture. Il arrive en France 70 ans après la mort du créateur de l'oeuvre, sauf exception. Est donc organisé à toulouse le 26 Janvier une journée du domaine public, avec plusieurs évenements, la liste que nous vous présentons n'étant pas exhaustive :)

> Conférence de 14h à 18h, Médiathèque José Cabanis de Toulouse, entrée libre  
>   
> \* Présentation générale du domaine public, Véronique Boukali et Alexis Kauffmann, Romaine Lubrique ;  
> \* Rapport d’étude empirique : que se passe-t’il quand des œuvres entrent dans le domaine public ? Caroline Becker, Wikimédia France ;  
> \* Que fait Europeana pour le domaine public ? Jean-Frédéric Berthelot, Wikimédia France, pour Europeana ;  
> \* Fond Ancely et Fond Trutat, le domaine public à la Bibliothèque de Toulouse, Patrick Hernebring ;  
> \* Henri Martin et les autres, le domaine public au musée des Augustins, Christelle Molinié ;  
> \* Wikimedia Commons, Wikisource, Wiki Loves Monuments… et domaine public, Pierre-Selim Huard, Wikimedia France ;  
> \* Le domaine public vivant : présentation de l’auteur Pouhiou par lui-même et de la peintre Gwenn Seemel.

Mais ce n'est pas tout, il y aura également un edit-a-thon (c'est à dire un atelier de contribution à Wikipédia), qui se tiendra à la bibliothèque du musée de 9h à 12h et réunira une dizaine de contributeurs :)

Bref, si vous voulez en savoir plus, si cela vous intéresse, ou si vous êtes sur Toulouse demain, voici plus d'information sur le [site officiel de l'évenement](http://journeedudomainepublic.fr/) :o)