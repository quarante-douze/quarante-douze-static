---
title: 'Le domaine public payant, une fausse bonne idée'
tags:
  - Domaine Public
url: 50.html
id: 50
categories:
  - Libreries
  - Politique
date: 2014-04-30 17:03:00
---

Une idée qui commence à revenir ces derniers temps est celle d'un domaine public payant. Dans un contexte de lutte contre le piratage, certaines personnes se demandent s'il ne serait pas préférable de faire du domaine public un domaine payant, dans le sens ou l'on devrait s’acquitter d'une taxe pour se procurer une œuvre qui s'est élevé dans le domaine public, la différence entre le domaine public et le domaine privé serait que l'argent obtenu par le domaine public serait redonné aux jeunes auteurs, pour les aider à démarrer. L'idée n'est pas nouvelle, Victor Hugo la défendait déjà...

Comme quoi, tout le monde peut défendre des idées qui n'en sont pas des bonnes.

Bon, je ne vais pas taper comme un sourd sur Victor Hugo, d'autant plus que son idée doit être remis en perspective : A son époque, la production n'était pas comme maintenant, et il n'y avait pas une durée des droits d'auteur aussi longue, ni une armée de pit-bull terminator enragé au service des ayants-droits. Cet article va donc critiquer cette idée, et en donner une alternative plus nuancé et intelligente, basé sur le concept de partage des revenus.

La première critique que je vais adresser au concept de « Domaine Public Payant » est basé sur le fait que l'échange présent dans le domaine public prendrait un aspect assez étrange. En effet, l'idée du domaine public est que le domaine public est supposé être un espace « qui appartient à personne et à tous ». Une œuvre du domaine public n'est plus lié à un ayant droit qui peut réceptionner les dîmes liés aux œuvres. La seule chose que l'on peut défendre est un devoir de « mémoire » dans le sens ou l'on doit tout de même attribuer l'oeuvre que l'on redistribue/utilise à son créateur, ne serait-ce que par politesse et respect. Rendre le domaine public payant serait donc faire que cet échange qui n'est plus totalement un échange, puisque c'est juste se procurer un exemplaire de ce qui est la propriété de tous, un échange commercial, ce qui ferait que nous devrions payer pour ce qui est pourtant devenu une œuvre supposé être accessible sans médiation à tous. Nous pourrions dire que défendre le concept de « Domaine Public Payant » sous entend une défense implicite de l'échange commercial comme la forme première de l'échange. L'accès pour tous à la culture ne peut se passer par un verrouillage du patrimoine culturel.

De plus, la culture classique étant enseigné en classe, faire payer en plus ces textes mettrait un baton dans les roues de touts les cours demandant l'étude de ces documents historiques, et défavoriserait encore plus les élèves ayant des difficultés financières. En effet, le domaine public est une solution permettant d'alléger le poids financier de l'école tout en augmentant l'accès à la culture : Puiser dans le domaine public des ouvrages que les élèves pourront se procurer facilement. Nous pourrions nous demander si équiper les élèves de liseuse électronique et leur faire découvrir le site Gallica de la BNF et l'internet archive, et leur faire lire plus d'ouvrage présents dans le domaine public ne serait pas une idée plus intéressante que de les équiper d'ordinateur comme le font certains lycée voir collège…

Pour finir, nous pouvons nous dire qu'il y a autre chose à viser. En effet, nous pourrions nous dire que si on veut que le domaine public serve à aider les jeunes artistes, on peut s'intéresser à une autre source de revenu : plutôt que de faire payer le peuple avec ce qui serait une véritable « taxe sur la culture », nous pourrions nous pencher sur les réutilisation commerciale d’œuvre dans le domaine public : Les réimpression et ventes d'ouvrage du domaine public (qui n'ont donc plus de droits d'auteurs à payer…), et les adaptations, qui n'ont pas d'accord à faire avec les auteurs. Donc, nous pourrions nous demander s'il n'est pas plus intéressant de demander une contribution à ceux qui profitent de ces échanges qui sont déjà des échanges commerciaux, plutôt que de faire payer encore plus les citoyens en rendant payant tout un plan de la culture qui était sous régime non-commercial.