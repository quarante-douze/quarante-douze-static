---
title: 'Dash-to-Panel, une extension pour avoir un panneau à la Windows 10 sous GNOME'
tags:
  - Extensions GNOME
  - GNOME
  - Linux
url: 28.html
id: 28
categories:
  - Libreries
date: 2017-01-26 14:00:00
---

Si [GNOME](http://gnome.org) à le défaut d'être très peu configurable par défaut, il possède un système d'extension très puissant, qui permet de faire des tas de modifications au bureau. Avec sa version 12, le système d'exploitation Zorin à montré cela [en basant son bureau sur GNOME Shell](http://zoringroup.com/blog/2016/09/19/this-is-zorin-os-12/), et avec une série d'extension transformant le shell pour ressembler par défaut à un Windows 7~10, mais pouvant le faire ressembler à un tas d'autre bureaux différents (Mac, Unity, Windows Classic, etc).

Le projet étant libre, son code est accessible à tous, et modifiable par tous. C'est en vertu de cela que le zorin-panel, l'extension créant le panel a été forké afin de créer [dash-to-panel](https://github.com/jderose9/dash-to-panel) et de fournir la possibilité d'avoir une interface avec une barre de tâche sur l'écran, plus proche de celle d'un Windows moderne pour Gnome.

![capture de la barre de tâche](https://github.com/jderose9/dash-to-panel/raw/master/media/screenshot.png)

Les fonctionnalités affichées sont :

*   Déplace le Dash (qui affiche les applications actives et favorites) dans le panel principal (la barre du haut)
    
*   Control de la position du panneau (en haut ou en bas) et sa taille.
    
*   Les indicateurs incluent le comptage des fenêtres, qui peut être positionnées en haut ou en bas du panneau.
    
*   Preview des fenêtres ouvertes quand on passe la sourie sur l'icône de l'application
    
*   Configuration de la localisation de l'horloge
    
*   Retrait de l'icone "afficher les applications" du dash
    
*   On peut cacher le bouton "activités" du panel
    
*   Isolation des applications actives dans les espaces de travail
    
*   Assignation des comportement de clic (lancer une nouvelle fenêtre, passer d'une fenêtre ouverte à une autre, minimiser, etc)
    
*   Assignation de la police, de la taille des icones et de marges aux icones du dash, aux icones de status et aux éléments de la barre de tâche.
    

La barre de tâche ne comporte pas par défaut le menu à la Windows 7 de Zorin OS. Cependant, étant donnée que la barre de tâche se fondre sur la barre déjà présente dans Gnome 3, l'extension est compatible avec un tas d'autres extensions Gnome, tels que [Dynamic Panel Transparency](https://extensions.gnome.org/extension/1011/dynamic-panel-transparency/), [Hide Top Bar](https://extensions.gnome.org/extension/545/hide-top-bar/), [Gno-Menu](https://extensions.gnome.org/extension/608/gnomenu/), etc. Il est également compatible avec les thèmes Gnome-Shell.

Si vous voulez quelque chose qui ressemble encore plus à Windows 10, vous pouvez rajouter à cela le [Windows 10 Transformation Pack](http://b00merang.weebly.com/windows-10-transformation-pack.html) :)