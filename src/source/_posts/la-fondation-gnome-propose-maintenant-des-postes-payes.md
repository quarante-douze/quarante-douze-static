---
title: La Fondation GNOME propose maintenant des postes payés.
tags:
  - GNOME
url: 16.html
id: 16
categories:
  - Libreries
date: 2018-07-06 18:25:00
---

Depuis ce matin à commencé la [GUADEC](https://2018.guadec.org), la conférence anuelle européenne des utilisateurs et développeur du projet GNOME, le projet d'environnement de bureau libre. C'est souvent le moment où des annonces sont faits sur ce qui est à venir, et des bilans de ce qui ont été fait.

Ce premier jour, il y a eut des conférences sur les performances de GNOME Shell - le composant s'occupant d'afficher l'interface du bureau, sur la transition vers GNOME Shell d'Ubuntu, sur GTK4, sur comment faire des interfaces pour téléphone avec GTK… et pleins d'autres sujets que je vous laisse découvrir sur [l'emploi du temps](https://2018.guadec.org) !

Ici, j'aimerais parlé surtout d'une conférence, par Rosanna Yuen : « GNOME Foundation: Looking into the Future ». Je n'ai pas vu cette conférence, mais deux annonces particulière y ont retenu mon attention : les projets de la fondation avec la promesse de donation d'un millions de dollar, mais également des offres d'emploi chez GNOME.

Pour le premier, je rappelle qu'un don (le donnateur est gardé anonyme) avait été promis d'un million de dollar, afin de permettre l'évolution de la fondation GNOME. Ce que la Fondation projete de faire avec cet argent a été révélé dans [une slide de cette conférence](https://twitter.com/sramkrishna/status/1015181879341961216) :

*   Améliorer les opération de la fondation GNOME.
    
*   Diriger de meilleurs levée de fonds
    
*   Centraliser et améliorer les processus de contribution au projet GNOME
    
*   Lancer le projet pour faire qu'il soit autonome sur une plus grande échelle.
    

En effet, dans cette conférence a été remarqué le besoin d'évolution de la fondation face à [ses challenges](https://twitter.com/sramkrishna/status/1015180914383577088), qui avait eut un certain conservatisme dans la gestion de ses finances, des difficultés à garder les donateurs et des changements rapide dans le bureau. Traditionnellement, la fondation à surtout eut pour rôle de fournir une infrastructure et un cadre légal au projet, tandis que c'était surtout les sponsors qui finançaient directement le développement (ainsi que des projets de stages tels le Google Summer of Code).

Le fait d'utiliser cet argent pour aider à faire en sorte que la fondation ait une plus grande facilité à faire évoluer le projet - et donc que le projet ne soit pas uniquement dépendant de ses sponsors - est à mon opinion une très bonne chose. En effet, même si je suis très reconnaissant envers les sponsors du projet GNOME, je pense qu'il est aussi important que la fondation ait une part d'autonomie, et puisse influer sur l'évolution du projet qu'elle soutient.

Ce n'est pas la première fois que la fondation montre cette volonté cette année, puisqu'elle a aussi proposé [des stages dédiés à des questions de sécurités](https://wiki.gnome.org/Internships/2018/Projects).

Parmi les volontés qui m'intéresse le plus, il y a toute amélioration possible au processus de contribution à GNOME. Si les choses se sont déjà bien amélioré avec le pasage à [Gitlab](https://gitlab.gnome.org) et des projets tels que GNOME-Builder et l'utilisation de [flatpak](http://flatpak.org), il y a encore des possibilités d'améliorer cela.

Et pour aider à cet objectif, la fondation propose aussi des emplois, qui seront donc directement payé par la Fondation et non par des entreprises externes. Parmi ces emplois, il y a :

*   Un⋅e [coordinateur⋅rice du développement](https://www.gnome.org/foundation/careers/development-coordinator/), qui s'occupera de lever les fonds et de diriger les plan de développement de la Fondation.
    
*   Un⋅e [coordinateur⋅rice de programme](https://www.gnome.org/foundation/careers/program-coordinator/) qui planifiera et organisera les programmes et les activités (GUADEC, Libre Application Summit, GNOME.Asia, hackfest…) de GNOME
    
*   Un⋅e [DevOps / SysAdmin](https://www.gnome.org/foundation/careers/devops-sysadmin/) qui s'occupera de gérer l'administration (avec l'équipe administratrice systeme existente) des infrastructure internet du projet.
    
*   Un⋅e [développeur⋅euse cœur pour GTK](https://www.gnome.org/foundation/careers/gtk-core-developer/) qui s'occupera d'un certain nombre de tâche de développement et d'administration pour la boite à outil graphique GTK.
    

Ces postes sont prévu initialement pour 1 an, avec pour objectif d'en faire des positions permanente si les financements le permettent.

Les raisons pourquoi cela me rend content sont double :

Premièrement, c'est un moyen de permettre à plus de développeur⋅euses de pouvoir vivre du logiciel libre, et c'est une bonne chose. C'est tout bête, mais les developpeur⋅euse⋅s de logiciel libre et open-source ont aussi besoin de se nourir, vivre, etc. Et c'est donc pour moi important pour elleux d'avoir des offres diverses. C'est donc une bonne chose pour les devs du projet GNOME d'avoir plus de possibilités d'emplois. Ce projet, c'est donc plus de développeurs qui vont pouvoir vivre de leur passion.

Secondement, parce que j'espère que cela va offrir au projet GNOME de nouvelles possibilités d'évolutions. Si je trouve que le projet GNOME à eut des tas d'évolutions sympa ces dernières années (par exemple Gilab, Flatpak et Flathub, divers projets, et pleins d'autres trucs que j'oublie), je pense que ce genre d'initiative devrait permettre au projet GNOME de pouvoir plus facilement évoluer, lancer des initiatives, etc. Notamment si tout un travail est fait autour de la coordination, cela pourrait permettre plus de collaboration entre les différents modules du projet.

Bref, j'ai hâte et je suis très curieux de voir ce que vont permettre ces évolutions, et je souhaite tout le courage à tout⋅e⋅s celleux concerné⋅e⋅s.