---
title: Comment se dégoogliser pour ceux qui le souhaitent.
tags:
  - Outils numériques alternatifs
url: 52.html
id: 52
categories:
  - Internet et numérique
  - Libreries
date: 2014-04-12 19:21:00
---

Cet article est un article réagissant à [un article du journal "La voix du Nord"](http://www.lavoixdunord.fr/culture-loisirs/nouvelles-technologies-pourquoi-et-comment-se-liberer-de-ia0b0n2049507), qui indique "comment se libérer de Google". Le problème est que si vouloir se détacher de Google est une chose valable au vue de certains scandales comme Prism et autre, les solutions proposés par le journal sont pas vraiment mieux, puisqu'il s'agit de truc Apple, Yahoo, Microsoft... En bref, on peut trouver ça assez ridicule, surtout quand leur motivation est (je cite) «* Internet, téléphonie, recherche, photo, vidéo…, Google fait tout, Google sait tout. Le problème est que ce groupe puissant et richissime n’a qu’une chose à vendre : ses utilisateurs, vous ! Être épié en permanence est insupportable, avoir sa vie privée décortiquée et commercialisée l’est encore plus. Il est temps de briser les chaînes.* » Donc, ils veulent remplacer par des compagnies qui font sans doute la même chose. A ce point, c'est assez impressionnant, si on veut être logique, il faut l'être jusqu'au bout.

Navigateur web
--------------

Bon, en fait là c'est le seul ou ils ont bon. Ils conseillent Firefox, c'est du bon, même si en plus de Adblock je conseille : [HTTPS Everywhere](https://www.eff.org/https-everywhere), ainsi que [Disconnect](https://disconnect.me/) (pour vous protéger du traçage). Parmi d'autres choix que Firefox (si vous voulez notamment quelque chose de plus léger), il existe [Midori](http://midori-browser.org/).

**Edit :** J'ai écris tout un article sur [les add-on à installer sur Firefox pour plus de sécurité](http://beerware-magazine.net/content/comment-customiser-son-firefox-pour-proteger-sa-vie-privee-322), si cela vous intéresse.

Recherche
---------

Ils conseillent... Bing, Yahoo et [Écosia](http://www.ecosia.org/), ce dernier est le seul concept intéressant. J'ai quelques alternatives pour vous dans [cet article](http://souklemanteau.quarante-douze.net/content/logicielsquelques-moteurs-de-recherches-respecteux-de-votre-vie-privee-223).

Téléphones
----------

Bon, là, faut aussi se rendre compte d'un truc dans ce qu'ils écrivent : Le système très ouvert est une menace ?

Pour se dégoogliser d'Android, je conseille plutot : De passer à des versions alternatives libres d'Android (du coup ou Google ne peut pas ou moins vous faire de mal) : [CyanogenMod](https://quarante-douze.net/www.cyanogenmod.org) ou [Replicant](https://quarante-douze.net/www.replicant.us), qui utilisent notamment un market alternatif permettant d'avoir des applications open source. Autre alternative, [Firefox OS](https://www.mozilla.org/en-US/firefox/os/), créé par Mozilla.

Cependant, il est possible de réduire l'influence de Google sur votre Android, j'ai quelques conseils pour vous dans cet article.

Courrier
--------

Pour trouver des hébergeur mail plus éthique, nous vous conseillons plutot d'aller voir [cet article du Wiki de Korben](http://wiki.korben.info/email#fournisseurs_d%27e-mails_libres) qui vous donnera pleins de liens utile. Un deuxième très bon article a été écrit sur le site [Spirale Digitale](http://spiraledigitale.com/fournisseur-email-messagerie-web-webmail-ou-service-de-courriels-lequel-choisir/) sur le sujet.