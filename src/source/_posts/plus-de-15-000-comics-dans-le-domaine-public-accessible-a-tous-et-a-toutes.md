---
title: 'Plus de 15 000 comics dans le domaine public, accessible à tous et à toutes !'
tags:
  - Comic
  - Domaine Public
url: 53.html
id: 53
categories:
  - Contenus culturels
  - Libreries
date: 2014-03-11 20:22:00
---

Si vous appréciez les comics, le site qu'on va vous présenter devrais vous plaire ! Il s'agit du Digital Comics Museum, un musée de comics accessible gratuitement à tous, puisqu'ils sont dans le domaine publique ! De quoi revivre ou découvrir toute une ancienne période des comics, faisant partie de ce qu'on appelle généralement \[sic\] « l'Age d'Or des Comics », une période faste ou les comics book étaient extrèmement consommés. Les comics sont nombreux, et sont pour la pluparts de compagnies de comics que vous ne connaissiez surement pas ! En effet, il ne s'agit ici pas de comics Marvel ou DC, mais plutot d'autres compagnies moins célèbre, sans doute parce qu'elles ne sont aujourd'hui plus actives, et donc n'ont pas fait d'extension de copyright par des réimpression, du moins c'est ce que je pense.

Toujours est-il que vous pouvez souhaiter la bienvenue à tous ces héros un peu obscurs, et peut-être que vous allez trouver votre bonheur, et faire des découvertes intéressante dedans ! Le seul petit bémol est qu'il faut être inscrit sur le site pour profiter de cette immense bibliothèque de comics.

[Le Digital Comics Museum est accessible ici](http://digitalcomicmuseum.com/).