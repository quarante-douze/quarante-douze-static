---
title: Quand l'Internet Archive s'offre les jeux Atari
tags:
  - Atari
  - Internet Archive
url: 68.html
id: 68
categories:
  - Contenus culturels
date: 2014-01-04 14:00:00
---

Ah, l'atari 2600. Moins connu de la génération des "jeunot" (à l'exception de ceux qui ont regardé [la vidéo du joueur du grenier](http://www.youtube.com/watch?v=hx_uQNgdsm0) sur le sujet) que de celle qui précédait, cette console était célèbre non seulement pour avoir des jeux qui demandaient plus d'imagination que nos jeux actuels, mais aussi et surtout parce que c'est elle qui a connu le célèbre crash du jeu vidéo, considéré comme ayant été causé par le célèbre jeu ET, considéré comme le plus mauvais jeu de tout le temps. Mais nous ne sommes pas ici pour parler d'histoire vidéoludique, mais pour annoncer que [les jeux de cette fameuse console ont été rendu disponible sur l'internet archive](https://archive.org/details/consolelivingroom), avec ceux de l'atari 7800, la ColecoVision, la Magnavox Odyssey², l'Astrocade...  
  
Vous pourrez donc jouer à tous ces oldies depuis le site de l'Internet Archive, et découvrir les graphismes somptueux de cette époque ;-) Bon jeu !