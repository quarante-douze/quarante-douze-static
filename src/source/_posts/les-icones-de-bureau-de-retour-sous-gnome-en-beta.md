---
title: Les icones de bureau de retour sous GNOME en beta
tags:
  - GNOME
url: 11.html
id: 11
categories:
  - Libreries
date: 2018-08-22 17:52:00
---

Comme vous le savez peut-être déjà, les icônes de bureaux ont été retiré de la version 3.28 de Nautilus (le gestionnaire de fichier de GNOME), pour des raisons techniques. En effet, non seulement cette fonctionnalité n'était plus utilisée dans la version vanilla de GNOME depuis la version 3.0, mais en plus cette fonctionnalité posait des problèmes techniques au navigateur de fichier. Les problèmes de cette fonctionnalité était que c'était un code ancien, qui n'était plus véritablement maintenu, et qui demandait beaucoup d'exception spéciale dans le code de Nautilus. En effet, les icones sur le bureau ont des besoins spécifiques que n'ont pas les icones dans une fenêtre "classique" de navigateur de fichier. Parmi les principaux soucis, il y avait :

*   Le fait que cela demande à une instance de Nautilus d'être toujours active. (mitigée par la séparation en deux executable, nautilus et nautilus-desktop)
*   Le fait que cela empêchait un gros retravail du code de Nautilus, notemment pour utiliser des nouvelles vues plus performantes, un système de "tâche" qui rendrait Nautilus plus fiable ou bien le portage de Nautilus vers GTK4.
*   Cette solution n'était pas efficace sous Wayland, puisque c'était un logiciel qui affichait une partie du shell. Sous Wayland, les icones de bureaux nécessitait XWayland et n'étaient pas native.
*   Cette solution fonctionnait mal avec plusieurs moniteurs.

Ce retrait à fait que Canonical qui utilise cette fonctionnalité sur la version de GNOME modifié qu'ils utilisent depuis Ubuntu 17.10 a décidé de rester sous la version 3.26 de Nautilus, afin de garder cette fonctionnalités. Il est à noter que cela a été fait [avec la bénédiction du mainteneur de Nautilus](https://didrocks.fr/2018/01/23/welcome-to-the-ubuntu-bionic-age-nautilus-a-lts-and-desktop-icons/). Après ce retrait, il a été énoncée que les icones de bureaux seraient réimplémenter en tant qu'extension pour GNOME-Shell, retirant les problèmes cités plus haut. Et aujourd'hui, la première version de cette extension [sort en beta](https://csorianognome.wordpress.com/2018/08/22/desktop-icons-goes-beta/) ! Ce travail est notamment fait afin de permettre de conserver des icones de bureau dans [GNOME Classic](https://help.gnome.org/users/gnome-help/stable/gnome-classic.html.fr), une session spéciale pour GNOME Shell introduisant des customisations et des extensions afin de permettre une façon de travailler plus "traditionnelle" (notamment avec les classiques "deux barres" de GNOME 2). Elle est notamment utilisée par défaut dans [Red Hat Enterprise Linux](https://fr.wikipedia.org/wiki/Red_Hat_Enterprise_Linux) (et ses dérivés comme [CentOS](https://fr.wikipedia.org/wiki/CentOS)) ![Des icônes de bureau sous GNOME](https://quarante-douze.net/data/medias/screenshot-from-2018-08-22-11-23-49.png) Les fonctionnalités suivantes ont été réimplémenté :

*   Ouvrir des fichiers
*   Executer des fichiers .desktop
*   Support du Drag and Drop pour réordonner les fichiers (sans chevauchement comme avant les anciennes icones de bureaux sous Nautilus)
*   Support du multi-moniteurs, une autre avancées par rapport au code de Nautilus
*   Ouvrir avec un terminal
*   Copier/Coller
*   Intégration avec Nautilus pour les opérations
*   Annuler/Refaire
*   Les raccourcis
*   La sélection en rectangle
*   100% Compatible avec Wayland !

Il manque encore le popover pour renommer les fichiers et la possibilité d'utiliser le drag and drop pour déposer un fichier dans un dossier. Vous pouvez télécharger l'extension sur [le site officiel des extensions de GNOME](https://extensions.gnome.org/extension/1465/desktop-icons/), comme toute autre extension, et on peut être certain que dès que possible ce sera aussi disponible sous la forme de paquet dans d'autres distributions (notamment sous Ubuntu où cela permettra d'utiliser une version plus moderne de Nautilus). Si l'ont peut critiqué le fait que cette extension a été faites après le retrait de la fonctionnalité, je pense qu'il faut remettre cela en perspective. En effet, cela bloquait un travail urgent (le code d'affichage des fichiers dans Nautilus est très vieux, et retient pas mal en arrière le projet), et j'ai pas l'impression qu'à part Red Hat (le développeur de l'extension, et mainteneur de Nautilus par la même occasion, travaille pour Red Hat), il n'y a pas eut un grand travail sur l'extension qui a été proposée avec un appel à contribution. Je ne dis pas ça pour attaquer Canonical (j'aime beaucoup leur session Ubuntu), mais pour montrer une partie du soucis : c'est que le nombre de développeur n'est pas infini et que ce genre de boulot prend du temps. Si personnellement je ne l'utiliserais pas, c'est une bonne nouvelle pour les utilisateurs GNOME qui apprécie les icônes sur le bureau, et pour les projets utilisant GNOME Shell et nécessitant cette fonctionnalité. Source et Source des images : Le blog de [Carlos Soriano](https://csorianognome.wordpress.com/2018/08/22/desktop-icons-goes-beta/)