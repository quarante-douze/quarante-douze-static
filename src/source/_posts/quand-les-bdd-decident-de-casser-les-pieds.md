---
title: Quand les BDD décident de casser les pieds
tags:
  - PluXML
  - Wordpress
url: 38.html
id: 38
categories:
  - La vie du site
date: 2016-01-15 13:07:00
---

Dans la série "meh, j'aurais du m'y attendre" : MySQL m'a (encore) laché, et mes backup m'ont dit également flute. Et bien, j'ai décidé de faire la même chose, et de passer à PluXML, un logiciel de blog sans base de données. Au revoir, wordpress ! Il me faudra peut-être du temps à m'habituer à ce nouveau logiciel, mais en tout cas, cela fait un soucis de réglé.

Pour l'instant, le blog est en cours de reconstruction. C'est dommage pour les articles de l'ancien sites, qui sont du coup indisponible. (je pense pouvoir en retrouver quelques uns une fois que je serais de retour sur ma tour, ou j'avais conservé quelques articles au format opendocument). Je vous présente toutes mes excuses pour le désagrément, et cette fois normalement cela devrait être plus simple à backup ou à retrouver. Un peu de principe UNIX, everything is a file, ne devrait pas faire de mal ;-) Je vais continuer de le pimper un peu, jusqu'à ce qu'il soit près pour l'ouverture.

Let's rock and roll !