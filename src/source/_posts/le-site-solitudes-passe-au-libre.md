---
title: Le site « Solitude(s) » passe au libre !
tags:
  - Solitude(s)
url: 47.html
id: 47
categories:
  - Libreries
date: 2014-05-08 10:00:00
---

[Solitude(s)](http://solitudes.fr/index.php) est un site lancé par DanyCalligula, un Youtubeur notamment connue pour sa chronique Doxa, une chronique parlant de philosophie qu'il publie sur Youtube et qui traite de sujets divers et variés. Solitude(s), lui, est un site collaboratif et horizontal, qui a pour but de fournir un média alternatifs aux média traditionnel (certains se diront "encore un ?", et je leur répondrais "encore un !" d'une voix joyeuse : En effet, si les médias alternatifs se multiplient et deviennent de plus en plus nombreux, c'est une bonne chose, puisque cela permet à de plus en plus de voix de s'exprimer sur internet !), et pour mieux décrire son fonctionnement, je me permettrais de citer le manifeste du site, et surtout la liste des principes qui régissent Solitude(s)

>  
2.  \> Chacun peut, selon son envie et ses capacités, contribuer à l'amélioration de Solitude(s).  
      
    >
>  
4.  \> Nous souhaitons proposer un contenu de qualité, constructif, varié, et [loin des poncifs habituels](http://solitudes.fr/index.php/articles/5-pourquoi-je-ne-m-informe-plus-ovis-solo).  
      
    >
>  
6.  \> Les articles doivent être présentés de manière honnête, sans verser dans le sensationalisme ou la recherche du buzz.  
      
    >
>  
8.  \> Les contenus doivent être compréhensibles par tous. Toute terminologie savante se doit d'être expliquée afin d'assurer au mieux l'égalité d'accès aux créations neuves.  
      
    >
>  
10.  \> Les créations pourront amorcer un débat qui se prolongera à la fois dans les commentaires, en live et sur [les forums](http://solitudes.fr/index.php/forum/index) de Solitude(s)... et partout ailleurs.  
      
    >
>  
12.  \> Le contenu du site est proposé [sous licence libre](http://solitudes.fr/index.php/articles/47-pourquoi-solitude-s-passe-au-libre), afin d'en favoriser la lecture, l'étude, la modification et la diffusion.  
      
    >
>  
14.  \> Toute publicité, publi-information, article sponsorisé sont des pollutions informationnelles qui ne trouveront pas leur place sur Solitude(s)  
      
    >
>  
16.  \> [L'économie du don](http://solitudes.fr/index.php/participer-a-solitudes) permettra l'accessibilité et la liberté du discours, exempt de toute contrainte économique.  
      
    >
>  
>

Ce qui nous intéressera surtout sont quelques points de ce manifeste : Déjà, l'absence de publicité est un point assez intéressant et qui peut être défendu sur pleins de points (que ce soit pour la qualité de l'accès au contenu, mais également pour tout ce qui est manipulation et traçage effectué par les publicité sur internet), mais c'est surtout sur un autre point qui va nous intéresser : Le contenus de Solitudes est sous licence Libre, plus précisément la licence Creative Common Attribution-Partage à l'identique (d'un point de vue de la licence, cela veut dire que les contenus dérivés doivent être partagé sous la même licence.).

Ce choix a été expliqué par un article de Pouhiou sur Solitude(s), qui se nomme tout simplement : "[Pourquoi Solitude(s) passe au libre.](http://solitudes.fr/index.php/articles/47-pourquoi-solitude-s-passe-au-libre)". Une autre source que nous pouvons que trop vous conseiller, c'est tout simplement les dires de Dany Calligula dans [une interview que vous pourrez trouver sur Framasoft](https://quarante-douze.net/www.framablog.org/index.php/post/2014/05/06/solitudes-site-libre-collaboratif) :

>   
> — **T’es pas libriste à l’origine. Pourtant, dès que j’ai évoqué l’idée de placer Solitude(s) sous CC-BY-SA, t’as été enthousiaste… Comment tu t’es intéressé au libre et pourquoi tu as décidé de sauter le pas ?**  
>   
> — Pour moi, ce n’était pas vraiment « sauter le pas », mais plutôt trouver chaussure à mon pied. Quasiment tout ce que je sais, toutes mes lectures, mes découvertes cinématographiques et vidéoludiques ne proviennent pas de mon éducation, des écoles, ou de tout ce que j’ai acheté à la FNAC ou sur Amazon… Au contraire, si je m’en étais limité à ça, je ne serais pas celui que je suis aujourd’hui. Par contre, les initiatives populaires, les bibliothèques, les internets et surtout le téléchargement m’ont permis de me construire intellectuellement. Vraiment, pour moi le libre était quelque chose d’inné, j’ai toujours trouvé que les choses et la culture étaient libres, d’autant plus avec internet, où quels que soient tes verrous on va te hacker. C’est pourquoi je me permets de télécharger et d’utiliser des extraits d’œuvres (films, musiques, etc.) dans mes émissions.

Solitude(s) défend donc une vision de la culture comme libre et partageable, remixable, et avec ce choix licence mentionne donc que c'est le cas pour le contenu offert par le site, et nous remarquons surtout un passage assez intéressant de la page "Solitude(s) passe au libre", qui va jusque à dire que les licences de libres diffusions ne seront pas retenues dans l'article "[Pourquoi Solitude(s) passe au libre.](http://solitudes.fr/index.php/articles/47-pourquoi-solitude-s-passe-au-libre)". La démarche est donc de permettre la réutilisation du contenu et la création de contenus dérivé, ce qui permet aux idées de circuler et d'être travaillée, retravaillée, reréfléchiée...

Un autre élément extrèmement intéressant du site est son fonctionnement via un systeme de don, qui présente un modèle économique différent de celui de la pub, qui n'est pas sans nous rappellez le modèle économique du prix libre [défendu par le bloggeur Ploum](http://ploum.net/le-prix-libre-une-impossible-utopie/).