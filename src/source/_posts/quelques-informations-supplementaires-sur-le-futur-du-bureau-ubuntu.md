---
title: Quelques informations supplémentaires sur le futur du bureau Ubuntu
tags:
  - GNOME
  - Ubuntu
url: 21.html
id: 21
categories:
  - Libreries
date: 2017-04-09 10:11:00
---

Suite à son annonce choc sur le retour d'Ubuntu vers GNOME pour la version 18.04, le fondateur de Canonical a posté [un message sur Google+](https://plus.google.com/+MarkShuttleworthCanonical/posts/7LYubpaHUHH) où il a répondu (notamment en commentaire) à des questions sur le futur de bureau Ubuntu. Il faut savoir que ces informations restent incomplètes, et qu'il se peut également que les choses changent avec le temps. Yep, c'est une reprise et traduction d'un commentaire que j'ai fait sur OMGUbuntu, je me suis dit que ce serait aussi utile de le publier ici :)

Son but est de rapidement se déplacer vers un modèle ou les ressources de Canonicals seront utilisée pour supporter la communauté Ubuntu-GNOME, dans le but de se concentrer sur la stabilité, les mises à jours, l'integration et l'experience, dans le but de fournir un fantastique bureau GNOME. Ce ne sera pas sûrement pas une expérience Unity-like mais un bureau assez "Vanilla" (sans doute pas à 100%, mais en grande partie), puisqu'il parle de fournir GNOME "de la manière dont GNOME veut qu'il soit fourni. Personellement, je trouve ça dommage, mais bon... Aucun mot sur le theme Ambiance et tout, j'espère quand même qu'il va pas abandonner l'idée d'avoir un theme propre à Ubuntu :/

Il y aura encore une équipe dédié au bureau Ubuntu, qui va donc plus se concentrer sur le fait de fournir un bureau de haute qualité pour tous. Cependant, ce travail sera plus du côté downstream (un peu comme à l'époque pré-Unity, en fait), puisque leur véritable focus est sur l'expérience utilisateur, ce qui veut dire que les choses doivent être bien faites au niveau de la distro, puisque c'est ce que les utilisateurs touchent.

Il dit également que la communauté GNOME sera fière d'à quel point GNOME sera bien dans Ubuntu, et qu'il espère que GNOME sera plus ouvert aux idées d'Unity. Je pense que l'apport d'utilisateur va sans doute affecter ça, pour ma part, parce que ça va sans doute augmenter le nombre de developpers pour GNOME, ou pour les extensions. En tout cas, je pense que ça va largement influencer le nombre d'application tièrce pour GNOME, puisque respecter les HIG de GNOME, ce sera s'assurer que l'application s'intègre bien dans Ubuntu, la plus grosse distribution Linux.

Snap n'est pas abandonné, et restera un projet important. Il vise un support de plusieurs distributions et une intégration aux différents bureaux Linux. Perso, je pense qu'à terme, s'ils n'adoptent pas Flatpak ne serait-ce qu'en partie, l'équipe Ubuntu GNOME participera à rendre disponible les logiciels GNOME sous Snap.

La raison de Unity a été expliqué : Il a tenté de présenter son idée à KDE et GNOME, et les deux ont refusé. GNOME, c'était un petit côté "mais pourquoi il vient nous dire ce qu'on doit faire alors qu'il contribue pas ?!" (même si après ils ont aussi refusé des patches). KDE, il dit que ce serait plus lié au caractère du leader du projet, qui s'est sentit menacé. Il a également développé un point très importante dans un autre commentaire : La tendance qu'il y a avec certaines personnes dans la communauté du libre, de détester tout ce qui est mainstream. Et je l'écrit ici, parce que je pense qu'il tient un point très important, même si ce n'est pas que dans la commu du libre, mais par exemple aussi dans _beaucoup_ de communauté lié au jeux-vidéo et au numérique (et aussi à la culture) en règle général.

Plus d'information nous seront sans doute également données plus tard, puisqu'une [annonce sera faite](https://lists.ubuntu.com/archives/ubuntu-gnome/2017-April/004260.html) avant la fin de la semaine prochaine par l'équipe d'Ubuntu GNOME quant au futur de la distribution.