---
title: Bientot un accès à la culture libre depuis le Dash d'Ubuntu ?
tags:
  - Domaine Public
  - Ubuntu
url: 60.html
id: 60
categories:
  - Contenus culturels
  - Libreries
date: 2014-01-09 11:38:00
---

Le méta-moteur de recherche d'Ubuntu à souvent été décrié pour le fait qu'il pouvait donner des données (nos mots clef de recherche + notre IP) aux sources utilisés pour le moteur de recherche. Le partenariat avec Amazon a été également critiqué pour diverses raisons (le fait que ce soit une forme de publicité principalement, et également le fait que Amazon utilisant fortement des DRMs notamment dans ses ebooks (il utilise notamment un format autre que le format epub, qui lui est un format ouvert), mais un nouveau projet a été lancé, basé sur le site The Pirate Bay, qui permettrait un système de recherche de Torrent.

Au début refroidit par l'idée que son scope (outil de recherche) puisse être vu comme illégal puisqu'il renverrait aussi des contenus illégaux, David Callé a énoncé que son but était « d’apporter la culture libre dans l’expérience utilisateur en la proposant dans la barre de recherche de l’OS. » Il déclare notamment qu'il pousse que pour le plugin Jamendo « devienne une des sources de musique par défaut. » C'est pour cela que le plugin aurait par défaut un système de filtrage de licence, pour permettre une mise en avant de la culture libre, que ce soit des travaux dans le domaine publique, ou des travaux mis en ligne sous licence libre.

Évidemment, pour ceux qui détestent les filtres, ( ou pour ceux qui veulent bénéficier d'une recherche complète ;-) ), le filtrage des résultats serait désactivable. On peut dire que c'est plus en rapport avec "l'esprit du libre" que les liens amazons qui ont été décriés par le site fixubuntu :-)

[Source](http://www.framablog.org/index.php/post/2014/01/08/torrent-unity-ubuntu)