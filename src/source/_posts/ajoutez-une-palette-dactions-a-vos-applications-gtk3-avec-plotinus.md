---
title: Ajoutez une palette d'actions à vos applications GTK3 avec Plotinus !
tags:
  - GNOME
  - GTK
url: 26.html
id: 26
categories:
  - Libreries
date: 2017-01-28 13:00:00
---

Plotinus est un module construit pour GTK3 qui permet d'ajouter une palette d'action, à la manière du HUD de Unity, ou les palettes de commandes d'Atom et Sublime Text. Il va chercher dans les menus du logiciel et ses raccourcis claviers les différentes actions, ce qui le rend plus efficace que le HUD pour les applications GNOME3 qui n'ont pas de barre de menu classique. Il ne marche cependant pas avec les applications autre que GTK3.

![Plotinus en action](https://cloud.githubusercontent.com/assets/2702526/20246717/454a1a9a-a9e3-11e6-8b19-4db092348793.gif)

Il s'active avec la combinaison `Ctrl + Shift + P`, et vous pouvez taper le nom de l'action pour trouver l'action dans la liste. Le raccourcis n'est cependant pas modifiable sans recompiler le logiciel.

Pour l'installer sous une distribution basée sur Ubuntu.

`sudo add-apt-repository ppa:nilarimogard/webupd8`

`sudo apt update && sudo apt install libplotinus`

Pour lancer une application avec Plotinus, et avoir accès à toutes ses fonctions sur votre application GTK3, vous devez lancer l'application de la manière suivante :

`GTK3_MODULES=/usr/lib/x86_64-linux-gnu/libplotinus/libplotinus.so`

Pour l'utiliser de manière globale, vous devez rajouter aux fichier `/etc/environment` la commande suivante à la fin du fichier afin de charger le module pour toutes les applications.

`GTK3_MODULES=/usr/lib/x86_64-linux-gnu/libplotinus/libplotinus.so`

Voilà, maintenant, vous pourrez chercher rapidement les actions de toutes vos applications GTK3 :)

Source : [Webupd8](http://www.webupd8.org/2017/01/add-searchable-command-palette-to-any.html)