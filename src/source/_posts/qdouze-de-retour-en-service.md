---
title: Quarante-Douze de retour en service
url: 1.html
id: 1
categories:
  - Non classé
date: 2019-05-16 08:33:33
tags:
---

Dimanche dernier, suite à un incident serveur, Quarante-Douze est resté indisponible jusqu'à ce matin. J  

Dimanche dernier, suite à un incident technique sur chlore.net, le serveur hébergeant les sites Quarante-Douze ainsi que Kazhnuz.Space, les deux sites (ainsi que les autres services hébergé par chlore) sont resté indisponible.

J’écris donc ici sur Quarante-Douze un petit article pour explique ce qui s’est passé.

Tout est parti d’une erreur de mise à jour automatique serveur causée par un dépôt externe utilisé par Matrix. Cela a provoqué un problème qui a empêché de lancer le système d’exploitation qui est resté en erreur. Si j’ai pu récupérer une partie des données, la BDD s’est retrouvée corrompue par une erreur de relance. La récupération des données a été possible grâce au système de secours du serveur que j’utilise (qui a permis d’aller récupérer toutes les données sur le disque).

Si Kazhnuz.Space est un site sans BDD (ce qui rend plus facile la récupération), Quarante-Douze a souffert du crash de MySQL. Cependant cela n’a pas été un problème puisque les backups régulier du site mis en place depuis le dernier crash de BDD on permit de résoudre le souci. Pour l’instant, tous les contenus de Quarante-Douze et Kazhnuz.Space sont à 100% restaurée, à une exception près : les miniatures d’article de Quarante-Douze. Celles-ci seront récupérée et remise d’ici les prochains jours. Le serveur git sera également relancé dans ces eaux là.

J’en ai profité pour faire deux modifications au changement du serveur :

*   Tout d’abord, une mise à niveau de tout ce qui était possible de mettre à niveau. C’est un chlore tout propre et tout neuf qui nous héberge !
*   Ensuite, une réorganisation du fonctionnement du serveur au niveau des DNS. En effet, désormais je sépare distinctement les services (qui seront fourni par chlore et donc directement sur le DNS de chlore, par exemple code.kazhnuz.space va devenir git.chlore.net) et les sites hébergés qui seront sur leur propre domaine. Tout possible futur service utilisable par Quarante-Douze ou Kazhnuz.Space seront dessus, sauf si le service serait explicitement fait uniquement pour le site (par exemple un wiki pour un site, etc).

Voilà pour les nouveautés ! À bientôt pour de nouvelles aventures sur chlore, Kazhnuz.Space et Quarante-Douze !