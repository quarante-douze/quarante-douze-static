---
title: De nouveaux mockup pour GNOME sur téléphone.
tags:
  - GNOME
  - GNOME-Mobile
  - Librem5
url: 14.html
id: 14
categories:
  - Libreries
date: 2018-07-13 17:35:00
---

La compagnie Purism travaille depuis un moment déjà sur le Librem 5, un projet de téléphone portable libre tournant sous GNU/Linux. Le Librem 5 à une particularité, celle de vouloir proposer comme interface principale GNOME, avec comme alternative possible KDE Plasma Mobile ou la version d'Ubuntu Phone maintenu désormais par la communauté UBPorts. Cette interface n'utilisera pas GNOME Shell, mais Phosh, une interface spécialisé pour le mobile développé à l'aide des technologies GTK et [wlroots](https://quarante-douze.net/)

À la GUADEC, Tobias Bernard, designer pour GNOME et travaillant chez Purism, nous a proposé une conférence sur le travail qui est fait sur le design des applications et de l'interface principale. IL a rendu les slides de sa conférence accessible sur un [dépot github](https://github.com/bertob/guadec2018-slides). **Attention, je ne sais pas à quel point ce travail est terminé, c'est peut-être encore du WIP.** C'est cependant des concepts très intéressant à observer.

Tout d'abord, pour les applications, nous pouvions déjà trouver une image de présentation des applications sur le wiki GNOME (sur la [page](https://wiki.gnome.org/Design/Whiteboards/AppMenuMigration) parlant du projet de déplacer les menus d'applications, accessible sur la barre du haut de l'écran) :

![Image des applications sur le Librem 5](https://gitlab.gnome.org/Community/Design/os-mockups/raw/master/app-menu/example-apps.png)

En plus du travail fait par [Adrien Plazza sur les applications en deux parties](http://bytesgnomeschozo.blogspot.com/2018/03/one-widget-to-adapt-them-all-and-to.html), nous pouvons remarquer un travail fait pour les applications ayant trop d'élément sur la HeaderBar (la barre de titre fusionnée des applications GNOME). Le but est alors de déplacer le contenu en trop de cette HeaderBar vers une barre situé en bas de l'écran, à la manière de certaines applications Android. Ce travail a déjà été effectué sur Epiphany (GNOME Web), comme vous pouvez le voir dans cette [requette d'ajout de code](https://gitlab.gnome.org/GNOME/epiphany/merge_requests/6) sur le gitlab officiel de GNOME. Je vois un second intérêt à ce choix en plus de celui de faire tenir l'application dans un plus petit espace, il y a celui de rendre plus accessible certaines commandes importantes de l'application au pouce.

En plus de cela, Tobias Bernard à également montré des mock-ups intéressant de l'interface mobile du Librem5, présente sur le [diaporama](https://github.com/bertob/guadec2018-slides) présenté plus haut.

La première présente une "interface minimale viable"

![Interface minimale viable](https://quarante-douze.net/data/medias/capture-decran-de-2018-07-11-17-55-04.png)

Le second n'indique pas trop ce que c'est, mais je peux supposer que c'est l'objectif "plus tard" du Librem5

![Interface ](https://quarante-douze.net/data/medias/capture-decran-de-2018-07-11-17-54-49.png)

On note une très grande proximité avec GNOME, avec une barre de haut très proche de celle de GNOME, avec juste en moins le menu d'application - qui sera aussi retiré de GNOME - et le menu "Activité". Ce dernier est remplacé par une barre en bas, avec une flêche vers le haut. Celle ci active un menu proche du menu activité, mais plus adapté au mobile : La taille des image des fenêtre d'applications actives est maximisée, offrant plus de visiblité, et un bout de l'écran de liste d'application est déjà visible, offrant plus la possibilité à l'utilisateur de voir comment y accéder.

On note également que cette présence de la liste des applications qui se déplace pour prendre toute la place ensuite fait partie du projet d'utiliser plus [un modèle spatial et des animations sémantiques](https://github.com/bertob/guadec2017-slides) pour le projet GNOME.

Même s'il y a encore du travail pour implémenter tout cela, c'est intéressant de voir l'avancée en terme de design du projet :)