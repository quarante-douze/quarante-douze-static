---
title: 'EasyRPG, un clone libre de RPG Maker 2000/2003'
tags:
  - EasyRPG
  - RPGMaker
url: 25.html
id: 25
categories:
  - Libreries
date: 2017-01-30 10:13:00
---

RPG Maker 2000 et 2003 sont très connu des créateurs amateurs, parce qu'ils ont été les logiciels de créations de RPG les plus populaires, avant l'arrivée de RPG Maker XP et de son langage de scripting basé sur le Ruby, le RGSS. Malgré ses limitations, RPG Maker 2003 était très apprécié parce qu'il était simple à utiliser et permettait de faire des jeux vraiment sympa. Parmi mes préférés, il y a le très sympathique [Chocobo Panic Space](https://rpgmaker.net/games/3679/), un clone de Pacman vraiment très réussi, et qui pousse bien plus loin le concept avec pas mal de petites idées sympa.

[EasyRPG](https://easyrpg.org/) est une réimplémentation libre de RPG Maker 2000 et 2003. Son player est disponible sous Linux, Android, Mac et Windows, mais également sous des plateformes telles que la Nintendo Wii, la Pandora, la 3DS... et sur navigateur via HTML5/JS !

Le projet n'a pas encore de version stable, que ce soit dans la création du logiciel de création (deux logiciels officiel sont en cours de création, l'un écrit en vala et utilisant GTK, et un autre écrit en C++ et utilisant Qt5), dans la création du player ou des RTP. Cependant, le projet est encore en vie et avance bien. La dernière version est la version 0.5 sortie en [septembre dernier](https://blog.easyrpg.org/2016/09/easyrpg-player-0-5-potion/). La compatibilité s'améliore, et il ne faut pas hésiter à tester des jeux pour pouvoir aider en montrant là ou son les soucis d'implémentation :)

On peut tester ici [la compatibilité de EasyRPG](https://easyrpg.org/play/).

Je trouve que c'est un projet vraiment sympa, notamment pour assurer la préservation et la compatibilité d'une partie des créations amateures qui ont été faites depuis des années. Et je comprend qu'ils aient préféré se pencher sur les version 2000 et 2003 : Elles sont bien plus populaire que RPG Maker 95, et n'ont pas le besoin de réimplémenter entièrement le RGSS que demanderait un portage des version plus récente d'RPG Maker.

Perso, ce que je trouverais génial ce serait une fois qu'il a atteint une version stable, un portage de EasyRPG Player en tant que noyeau libretro (comme cela a été fait pour Doom, la version opensource du moteur de Cave Story, etc). Cela permettrait de jouer grâce à libretro à tous les jeux RPG Maker 2000 et 2003 sur des consoles comme [lakka](http://lakka.tv).

Ce serait vraiment cool :)