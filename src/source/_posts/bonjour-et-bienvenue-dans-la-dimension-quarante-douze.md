---
title: Bonjour et bienvenue dans la dimension quarante-douze
url: 79.html
id: 79
categories:
  - La vie du site
date: 2013-09-28 09:19:00
tags:
---

Bonjour et bienvenue sur mon blog, la Dimension Quarante-Douze. Ce blog est un blog personnel, ou je présenterais différentes choses qui me plaise, et ou je donnerais mon avis sur différents sujets. Bref, un blog comme les autres. J'y présenterais aussi mes propres réalisations et créations pour ceux que ça intéressent :) Parmi mes buts, il y a éviter de se contenter des opinions, tenter de construire plutôt que frapper. Sortir de la peur partout, et montrer ce qui peut se faire dans le monde. Montrer les aspects positifs de notre monde, ou même si le pire se passe parfois, on trouve aussi du meilleurs. Cet espace est également ouvert si d'autres personnes veulent me rejoindre et partagent cette envie. Ils pourrons alors profiter de cet espace web et des possibilités qu'il offre, que ce soit pour créer, donner des avis construit. Je vous donc une bonne journée et de passer un bon moment sur la Dimension Quarante-Douze :)