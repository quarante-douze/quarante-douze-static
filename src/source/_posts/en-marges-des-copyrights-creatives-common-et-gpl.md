---
title: 'En marges des copyrights, creatives common et GPL...'
tags:
  - Licences Libres
url: 67.html
id: 67
categories:
  - Libreries
date: 2014-01-05 14:00:00
---

Que ce soit les licences dites "propriétaires" (copyright, etc), ou celles dites libres (Creative Common, Gnu Public Licence), nous ne connaissons souvent qu'une petite partie des licences qui sont fournies pour nos oeuvres. En effet, techniquement, nous pouvons tous nous estimer capable de choisir sous quelles licences sous laquelles nous mettons... Et parfois, on choisit des licences rigolotes, funky ou amusantes. Voici donc un "top-5-pas-du-tout-dans-l'ordre" de licences amusantes ou juste pas très communes. Enjoy !

1 - La licence Beerware
-----------------------

Vous connaissiez les freeware, les shareware, les logiciels libres, les lgociels payants... Voici ici la licence beerware, dont le concept est très simple, puisque qu'il s'agit d'une manière humoristique de dire que la licence est très permissive. L'utilisateur est alors encouragé - ce sans la moindre obligation - à payer une bière en retour du logiciel au créateur, ce s'il l'a trouvé utile. Le terme est assez vieux, puisqu'il vient de John Bristor, qui l'aurait inventé le 25 avril 1987 (merci wikipédia), et aurait été mis en licence par Poul-Henning Kamp, dans le but de faire quelque chose de plus simple que le mastodonte juridique qu'étais la GPL. En effet, même si la GPL était d'une grande précision, sa complexité pouvait avoir quelque chose de rebutant pour ceux qui voulaient simplement dire : "voilà, vous pouvez partagez ça comme ça, comme vous voulez". La licence Beerware à donc le mérite d'être claire et précise.

`/* * ---------------------------------------------------------------------------- * "LICENCE BEERWARE" (Révision 42):  
* < phk@FreeBSD.ORG > a créé ce fichier. Tant que vous conservez cet avertissement, * vous pouvez faire ce que vous voulez de ce truc. Si on se rencontre un jour et * que vous pensez que ce truc vaut le coup, vous pouvez me payer une bière en * retour. Poul-Henning Kamp  
* ---------------------------------------------------------------------------- */`

( Vous noterez la magnifique référence, certe assez classique, avec le numéro de révision ;-) )

2 - [La _Do What The Fuck You Want_ Public Licence](http://www.wtfpl.net/)
--------------------------------------------------------------------------

Donc, voici la _do What the Fuck you want_ Public Licence (WTFPL, ou licence ["Rien a Branler" en notre belle langue poétique](http://sam.zoy.org/lprab/)), une autre licence créé dans le but de... Que, attendez. Comment ça, des gros-mots ? Vous êtes membres d'association de défense des enfants et vous estimez que les pauvres petits sales marmots enfants pourraient être choqué ? J'en ai rien à faire, d'ailleurs je vais citer [la faq de cette licence](http://www.wtfpl.net/faq/) :

> You know what? Fuck your stance on profanity.

Merci beaucoup, concepteur de la faq. Bref, cette appellation est surtout présente dans l'esprit "provocateur" d'internet, l'utilisation de l'expression "fuck" dans le titre de la licence étant surtout présente pour exagérer la portée de celle ci, pour la rendre plus direct et moins "juste gentillette". Ou alors c'est qu'il en avait marre que l'on lui demande sa permission quand il proposait son travail à tous comme dans la BD ci-dessous, et que tout son ressentit s'est crystalisé dans l'utilisation de ce mot...

[![Permission](http://copyheart.org/)](http://copyheart.org/)

La licence est présenté dans ces termes :

> `LICENCE PUBLIQUE RIEN À BRANLER  
> Version 1, Mars 2009  
>   
> Copyright (C) 2009 Sam Hocevar  
> 14 rue de Plaisance, 75014 Paris, France  
>   
> La copie et la distribution de copies exactes de cette licence sont  
> autorisées, et toute modification est permise à condition de changer  
> le nom de la licence.  
>   
> CONDITIONS DE COPIE, DISTRIBUTON ET MODIFICATION  
> DE LA LICENCE PUBLIQUE RIEN À BRANLER  
>   
> 0. Faites ce que vous voulez, j’en ai RIEN À BRANLER.  
> `

C'est clair, net et efficace, et sans interprétation différente possible.

### 3 - [Le copyheart](http://copyheart.org/)

Le copyheart se différencie des deux licences précédentes, dans cela qu'elle est plus destinée aux créations artistiques que aux créations logicielles. En premier lieu, son idée que le le fait de copier et partager de l'art - c'est à dire par exemple de mettre une oeuvre artistiques sur son blog ou autre - est une preuve d'amour, et que la valeur n'est pas prise par les fans, qu'elle est ajouté par eux. À cela, je mettrais un petit bémol, c'est là que viens ce qui est pour moi, auteur de cet article, le point le plus important dans ce genre de cas : Toujours mettre d'ou vient quelque chose. Pourquoi ? Non seulement pour une question de respect pour le créateur de l'oeuvre utilisé, mais aussi pour la personne qui aimerait ça (par exemple, la BD de Mimi et Eulice plus haut est clicable ;-) ) Toujours est-il que son symbole est simple, puisque s'est un coeur, et que ses termes aussi tout aussi simples :

> ♡ Copying is an act of love. Please copy and share.

### 4 - La licence [Complete Bullshit](http://jcfrog.com/blog/licence-complete-bullshit/)

![](http://jcfrog.com/blog/data/images/2013/06/completebullshit.jpg)

Pour la dernière de ces licences, nous allons vous en présenter une un peu plus humoristique, qui est la création de JcFrog, un bloggueur également parodiste de musique, qui a créé cette licence qui sert à la distribution d'oeuvre à caractère parodique ou humoristique, et possède une caractéristique : L'oeuvre en question est immédiatement reconnue « d'inutilité publique ». En effet, voici les termes de cette licence :

> Tout article ou image produite sous licence Complete Bullshit est reconnu d’inutilité publique. Tout y est ouvertement faux et scandaleusement mensonger, en général dans l’unique espoir d’aider à la LOLitude ambiante.  
>   
> Toute action en justice serait donc ridicule puisque ça ne me ferait pas plaisir, d’autant que cela n’est pas très gentil.

### 5 - La licence la plus fermée du monde

Celle-ci, il fallait également qu'on en parle. Quoique, même pas besoin, cette licence - l'unique véritablement juste à pour toutes la planète - parle d'elle même tellement elle est forte, tellement elle est puissante. (La [traduction provient du framablog](http://www.framablog.org/index.php/post/2013/12/05/licence-la-plus-fermee-du-monde))

> La licence La-plus-fermée-de-tous-les-temps version 1  
>   
> Ce site internet et toute sa propriété intellectuelle est la seule et unique propriété de Winestock Webdesign LLC. Toute personne (ci-après nommée le Couillon) utilisant cette propriété sans la permission expresse de Winestock Webdesign (ci-après nommée l’Entreprise) doit, de ce fait, renoncer au profit de l’Entreprise à toutes ses propriétés et tous ses biens ainsi que la propriété et les biens de ses collègues, de ceux qu’il aime, à ses animaux domestiques et de tous ceux qu’il a un jour rencontrés.  
>   
> Lire cette licence entraîne l’accord automatique de ces termes ainsi que l’accord à toute modification ou révision que l’Entreprise pourrait effectuer dans le futur à sa propre discrétion sur ce site, un site aléatoire, ou dans un autre univers. Aucun juge ou médiateur ne pourra rendre un avis sur la validité légale de cette licence ; en fait, tous les juges et médiateurs sont liés aux termes de cette licence même s’ils ne l’ont jamais lue ou ne savent pas qu’elle existe.  
>   
> Objecter, argumenter contre ou citer la moindre ligne de cette licence est interdit. Si le Couillon fait la moindre de ces choses, n’importe lequel ou tous les employés de l’Entreprise, leurs délégués, copains, potes de beuverie ou n’importe qui parmi leurs connaissances ou leurs pires ennemis auront le droit de prostituer l’épouse du Couillon, de faire l’amour avec passion à la plus belle des filles du Couillon, et forcer le Couillon à déclarer aux représentants de l’ordre choisis par l’Entreprise : « je suis un pédophile pratiquant qui trafique les mineurs en fugue » sans plus de développement. Le choix de la juridiction est laissé à la discrétion de l’Entreprise (mais ladite juridiction sera au minimum à 1000 km du lieu de résidence du Couillon).  
>   
> Rire de tout ou partie de cette licence, même en privé ou de façon inconsciente, est interdit.

\[\]: http://mimiandeunice.com/2011/08/30/permission-2/