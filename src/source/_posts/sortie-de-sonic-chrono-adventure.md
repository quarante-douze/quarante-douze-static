---
title: Sortie de Sonic Chrono Adventure
tags:
  - Fangame
  - Jeux vidéo
url: 72.html
id: 72
categories:
  - Fandoms
date: 2013-12-22 22:38:00
---

LakeFeperd n'a pas mis beaucoup de temps avant de devenir une référence incontournable du monde du fangame Sonic. En effet, après avoir sortie Sonic Before the Sequel (qui eut le droit à une bande son entièrement custom par la suite), Sonic Before the Sequel : Aftermath (une expérimentation de "Metronic", ou mélange du style de level designe à la Metroid avec du Sonic) et Sonic After the Sequel, il nous délivre aujourd'hui son dernier bijou : [Sonic Chrono Adventure](https://sites.google.com/site/sonicchronoadventure/).

Doté d'une histoire plus présente que dans ses précédants jeux, ce qui n'est pas sans nous rappeller la saga des Sonic Adventure, ce jeu se distingue par la reprise d'élément de Sonic BtS : Aftermath, notamment le système de pouvoir, ainsi que par une structure scénaristique ne fonctionnant pas avec un système de "Zone 1 Act 1", mais par un système mélangeant une aventure assez linéaire, avec une forme d'openworld. Une autre différence avec les Sonic de style Adventure-like est que Sonic ne parle pas dans ce jeu, ce qui n'est pas pour nous déplaire, vu que le personnage pouvait parfois être sacrément casse-pied. Des voyages dans le temps sont également de la partie, pour votre plus grand plaisir. Contrairement à Before et After the Sequel, ce jeu ne dispose pas d'une bande son entièrement composée pour l'occasion, et reprend beaucoup de l'OST des jeux précédant de Lake.