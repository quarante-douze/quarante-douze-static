---
title: 'Gallica, la bibliothèque numérique de la BNF'
tags:
  - BNF
  - Domaine Public
  - Gallica
url: 74.html
id: 74
categories:
  - Contenus culturels
  - Libreries
date: 2013-12-22 16:39:00
---

La Bibliothèque Nationale de France ne possède pas que des oeuvres physique, puisqu'elle dispose également d'un grand catalogue numérique, qui est disponible sur le site [Gallica](https://quarante-douze.net/gallica.bnf.fr).

Disposant d'un moteur de recherche intégrer compatible OpenSearch (ce qui est utilisé notamment par Firefox pour intégrer les moteurs de recherche, a coté de votre barre d'adresse) et d'un catalogue fourni de plus de 2 500 000 oeuvres, Gallica est une bonne alternative à l'Internet Archive pour les textes en langue française.

Bref, c'est un site que je conseille facilement pour tous ceux qui ont envie d'avoir accès à quelque à des tas d'oeuvres du domaine public en Français :)