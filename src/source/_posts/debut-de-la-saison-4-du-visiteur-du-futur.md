---
title: Début de la saison 4 du visiteur du futur
tags:
  - Webserie
url: 55.html
id: 55
categories:
  - Contenus culturels
date: 2014-01-19 17:58:00
---

Le visiteur du futur est un des emblême de la culture geek francophones. Cette websérie réalisé par François Descraques, notamment connu pour son personnage principale (le fameux "Visiteur du Futur") a été remarqué par une monté en qualité graphique et technique durant les trois dernières saisons. Cette saison 4 de dix épisodes (comme la saisons précédante) se nomme "Neo-Versaille" (la précédante se nommait "les missionnaires") et se déroulera dans le futur apocalyptique du la série, plus précisément dans la ville de Néo-Versaille, dans une ambiance qualifié par Descraques de "Steampunk à chien", mélangeant les influences du rétrofuturisme de l'époque pré-contemporaine avec l'ambiance "clochard du futur" de la série.

Le premier épisode sort donc ce dimanche 19 Janvier 2014 à 18 heures, et se nomme "La Balade de Raph et Stella". Vous pourrez le voir sur Youtube et Dailymotion, et pour plus d'information, [n'hésitez pas à vous rendre sur cette note du blog de François Descraques](http://www.frenchnerd.com/article-dimanche-a-18h-122121193.html).

Si vous ne connaissez pas cette web-série (ou si vous voulez pas voir en même temps que tout le monde), plutot que de vous accuser de vivre dans une grotte, [nous allons vous rediriger vers cette récap des saisons précédantes](http://www.youtube.com/watch?feature=player_embedded&v=Ok7DRQaPy20).

Bon visionnages ! :)