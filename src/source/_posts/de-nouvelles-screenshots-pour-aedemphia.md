---
title: De nouvelles screenshots pour Aëdemphia
tags:
  - Jeux indé
  - Jeux vidéo
url: 69.html
id: 69
categories:
  - Contenus culturels
date: 2014-01-02 14:00:00
---

Aedemphia est un projet de RPG inspiré de l'époque 16 bits fait avec RPG Maker, qui offre une esthétique graphique forte et des graphismes assez impressionnant. Mélengeant le RPG occidental et asiatique et très intéressant, ce projet nous a gratifié il a peu de nouvelles screenshots, assez sympathiques :

![Une screenshot d'Aedemphia](http://www.aedemphia-rpg.net/news/106/flene_inners_kulmie1.png)

![Une autre screenshot](http://www.aedemphia-rpg.net/news/106/flene_inners_manalianne.png)

Vous pourrez voir plus de screens [sur le site du projet](http://www.aedemphia-rpg.net/news-2013-12-31-flenemere-et-des-lieux-annexes.html) ;-) Fans de RPG, n'hésitez pas, et foncez ;-)