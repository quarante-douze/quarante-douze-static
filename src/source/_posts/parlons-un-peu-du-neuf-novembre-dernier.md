---
title: Parlons un peu du neuf novembre dernier...
tags:
  - Attentat
url: 31.html
id: 31
categories:
  - Politique
date: 2016-11-10 15:20:00
---

Au début, j'avais prévu de ne pas faire de long post sur l’élection de Donald Trump. Après tout, je me disais, pourquoi en faire un ? Partout on s'étonne (ou pas) du résultat, et bien des gens sont déjà à dire "c'est terrible" ? Du coup, qu'est-ce que je pourrais dire de plus ? Cependant, je pense avoir quelque choses à dire.

Comme pas mal de monde, je m'inquiète avant tout pour les personnes qui vivent là bas, qui font parties des minorités (LGBTQ+, personnes de couleurs, personnes handicapées, etc), qui vont à la fois subir la bigoterie du personnage, mais également le réveil de tous ceux qui vont se sentir revitalisé par ce qui s'est passé. Trump participe à libérer le phénomène de recherche de boucs émissaires. Évidemment, c'est une tendance qui existait déjà (nos politiciens passent leur temps à le faire), mais il participe à l'amplifier.

Ce genre de conséquences, on va les vivre jusque ici.

Cependant, cette première pensée n'est pas ma seule pensée. Evidemment, je ne peux pas m'empêcher comme bien des gens de me demander comment on en est arrivé là. La réponse est simple : Trump est l'effet d'un disfonctionnement très ancien des médias et de tout le système politique.

Mon but n'est pas ici d'excuser le "vote Trump". C'est loin d'être mon sujet. Je pointe surtout l'hypocrisie des médias traditionnels dans cette situation. Et surtout je veux un peu me sortir toutes ces pensées de la tête. Pouvoir souffler un peu.

L'élection de Trump comme "volonté du peuple" ?
-----------------------------------------------

Généralement, quand on met un point d'interrogation sur un titre, c'est pour montrer qu'on réfute une idée. Et là ne fait pas exception. Je pense que toutes les sorties sur le fait que la présence au pouvoir de Trump serait la "volonté du peuple", c'est faux.

Tout d'abord, je vais commencer par rappeller une chose : Le système éléctoral amériquain du président n'est pas un suffrage universel directe, mais fonctionne avec un système de grands élécteurs. On vote pour un certains nombres de grands élécteurs qui vont voter ensuite pour le président. Et la manière dont c'est fait fait qu'un président peut gagner en ayant un pourcentage plus faible que son adversaire. C'est ce qui est arrivé en 2000 pour l'éléction de George W Bush face à Al Gore, et c'est ce qui est [arrivé hier pour Trump face à Clinton](http://www.npr.org/sections/thetwo-way/2016/11/09/501393501/shades-of-2000-clinton-surpasses-trump-in-popular-vote-tally). Même si c'est à un poil de fesse près, cela montre surtout à quel point non, il n'a pas eut une "victoire écrasante".

Ce n'est pas la première fois que ça arrive, et ce ne sera sans doute pas la dernière fois. C'est en quelque sorte un débat proche que celui de la proportionnelle en France pour les éléctions législatives.

Donc, même si le système a donné Trump vainqueur, **il n'y a pas eut une "vague de Trumpisme", de raz-de-marée populaire qui lui a donné la victoire**. Ce mensonge est à la fois partagé par ses partisans qui veulent utiliser cette image qui les arranges, et à la fois certains de ses adversaires pour qui c'est bien plus confortable de chercher à accuser les réseaux sociaux ou le "peuple stupide qui vote en masse comme des moutons" plutot que de chercher à se remettre en question sur quelque que chose qu'ils ont contribués à provoquer.

C'est sans doute bien plus confortable.

D'autant plus que [les sondages sur les résultats de Trump](http://www.nytimes.com/interactive/2016/11/08/us/politics/election-exit-polls.html) montre bien qu'il a fait de bien plus forts résultats dans certaines portions de la populations : Les personnes plus agées, les blancs, les hommes, les plus riches... Les tendances montrent bien que ce n'est ni la "population dans son ensemble", ni le "petit peuple", opprimé. Comme celui du "raz-de-marée", ce genre de récits ont des buts différents, suivant qui le dit :

Chez les partisants du 45e président, il s'agit de faire passer l'éléction du président comme un effet de _la colère du peuple_. En effet, cela leur permet de se créer une légitimité "populaire" en imaginant ce mythe du peuple, las des injustices qu'il subit, qui en masse irait voter pour leur grand héros... Ce qui ne tient pas, surtout en raison du très grande nombre d'abstentionniste : Non seulement Trump a eut **moins** de voix que Clinton, mais il n'a pas mobiliser ceux qui ne croyaient pas dans le système. La plupart de ceux qui ne croient vraiment plus en le système ne sont simplement pas aller voter.

Ou alors c'était uniquement les mecs sur reddit et 4chan qui ont chouiné parce qu'ils avaient pas pensé à s'inscrire aux élections, une obligation qui d'habitude plaît bien aux républicain parce que ça pose plus soucis aux personnes d'origine étrangères ou aux minorités ethniques.

Chez ses adversaires (et c'est quelque chose qui se retrouve dans tous les pays dans la lutte contre l’extrême droite), il y a l'idée de fustiger le "bas peuple stupide qui ne sait pas ce qui est pour son bien". C'est d'ailleurs un peu ce qu'on retrouve dans le terme "populiste" : Trump aurait gagné parce qu'il flatterait les "bas instincts" du "bas peuple", celui qui ne serait pas capable de penser par lui même (veulent-ils derrière un retour du suffrage censitaire ? Pour certains je serais pas surpris).

Cependant, il ne faut pas oublier dans les personnes de classe sociales peu élevé, il y a justement fortement le genre de personnes sur qui Trump tape. D'ailleurs, l'idée que ce serait lié à l'éducation : Même si le nombre de blanc au niveau d'étude peu élevé ayant voté Trump est plus fort que le nombre de blanc au niveau d'étude élevé ayant voté pour lui, il semblerait que ce nombre soit fort lui aussi (et dépasse celui de Clinton).

Bref, dire de ce vote "le soucis c'est le peuple idiot et ignorant" est sans doute la pire des réactions possible.

Même si oui, l'extrème droite attire plus qu'avant les classes populaires, et les vise, tout rejeter sur elles, alors qu'elles sont déjà dans des situations peu enviables, et en plus contiennent pas mal de gens qui seraient victimes de leurs politiques (des personnes d'origines étrangères, des LGBT rejetés de leur familles, des personnes en situations de handicap, mais même de base les personnes en situations précaires vu le nombre de fois ou on les entends s'énerver sur les "assistés").

Dans tous les cas, la _colère d'un peuple_ peut revétir bien des formes, et n'en a pas en soi. Si l'extrème droite n'hésite pas à réutiliser le raz-le-bol lié au système pour le rediriger vers des idées pleines de haines de son prochain, se contenter d'expliquer ce genre de chose par "c'est le peuple", que ce soit dans une vision négative ou positive est tout simplement faux, et dangereux. Cela ne veut pas dire qu'il n'y a pas une dimension de rejet du système dans ce vote, mais juste que le récit du petit opprimé qui vote pour l'extrème-droite est un mythe.

D'autant plus qu'il y a une certaine ironie à qualifier l'extrême droite comme "anti-système".

Les score de l'extrème droite comme en partie le produit du "système"...
------------------------------------------------------------------------

Sous cette première phrase conçue de manière un peu "choc" envers tous les fameux "anti-système" de l'extrème droite, il y a quelque chose que je pense très important sur toute cette vague : Elle est produite et alimentée par le système, et ce à deux niveaux. Le premier est celui d'avoir amplifié leur visibilité, et le second est par la diffusion d'une vision du monde, surnommée de manière intéressante celle du "[syndrome du grand méchant monde](https://www.youtube.com/watch?v=8WiiqssAME4)" par la chaine Horizon-Gull.

En effet, les médias recherchent avant tout l'audimat. En utilisant le clan le Pen et l'extrème droite (cela comporte tous les Zemmour et autre personnes du genre qui aiment se pleindre sur tous les journaux et toutes les chaînes qu'ils sont opprimés et qu'il y a une cabale visant à les censurer. C'est plus ce que c'était la censure, alala) pour renforcer leur audimat, ils ont participé à véritablement mettre en avant ce genre de personnages parce que cela renforce l'audimat (comme tout ce qui est extrème) plutot que de mettre en valeur tout ce qui existe comme autre types de réflexions non-mainstream. En les utilisants a fond, à la fois comme épouvantail, et presque pour se donner des frissons, les médias ont participés à les rendre visible partout, bien plus que pas mal d'autres idées qui auraient gagnées a être partagées.

Evidemment, ensuite ils ont flippés. Mais le mal était fait.

Mais ce n'est pas tout, puisqu'ils ont aussi participé à cela de par cette manière du grand méchant monde, expression que je vais bien aimer reprendre. Cela fait des années que les médias abusent des faits divers et vont leur accorder une place bien plus grande dans leur couverture de "l'information". De même, un attentat et un fait divers sera couvert d'une manière presque digne d'un fait policier, pour faire monter l'adrénaline, mais ce qui va aussi augmenter la peur. L'infotainement, l'information est devenu un _grand spectacle_, et parmi les choses spéctaculaire il y a malheureusement tout ce que est glauque et horrible dans la vie : les meurtres et toutes autres atrocités que l'homme peut commettre sur son prochain.

C'est aussi quelque chose qui arrange bien les politiciens plus classiques, dans le sens ou avoir ce genre d'épouvantail permet de dire "nan mais non c'est bon, ça va". C'est le fait d'avoir utilisé pendant des années la "lutte contre le FN" pour ne jamais avoir à remettre en cause toutes les fois ou les partis "conventionels" diffusaient pourtant le même genre d'idées. Ce double jeu est sans doute une des choses qui ont le plus contribués à cette montée : Quand on dit la même chose, tout en diabolisant l'autre, c'est le meilleurs moyen de les faire progresser. Pourquoi le FN ferait peur quand on a des ministres qui balance des trucs racistes, sexistes, etc ? Le système médiatique et politique à participer pendant des années à la décomplexion de la parole raciste, sexiste, etc. On en a eut beaucoup d'exemples ces dernières années.

Donc, les voir jouer les surpris est tout bonnement insupportable.

Les politiques ont joué à ce jeu dangereux en toute conscience de cause, pensant qu'ils pourraient toujours avoir ça bien entre leurs mains, et parce qu'ils s'en foutait totalement des conséquences qu'il y aurait pour les victimes.

Et maintenant c'est nous tous qui nous brulons.

... et également de la perte de crédibilité de la politique traditionnels
-------------------------------------------------------------------------

Même si comme dit plus haut, l'extrème droite n'a pas le monopole de la haine de l'autre, ils ont participé a une transformation, puisque cette haine de l'autre est devenu une réponse à la question du "pourquoi" des problèmes de la société. Les problèmes que vivent la France sont mis sur le dos des étrangers, des LGBTs qui "sappent les fondements de la société", des "social justices warrior"... Et même chez les PUA, on retrouve l'idée "si vous êtes célibataires c'est de la faute des méchantes féministe".

L'idée est de donner une réponse simple aux problèmes. Une réponse facile. Des ennemis à combattre.

La politique traditionnelle à perdu son image de pouvoir donner des réponses aux problèmes. Plus personne n'y croit, parce qu'il y a eut trop de magouille, trop de trucs pas net. Sarkozy a 36 000 casseroles sur le dos, Hollande a trahi toutes les personnes de gauche avec son programme libéral économiquement. Les petits partis se lance dans des guerres internes qui font perdre confiance en eux. Le système participatif même a montré ses limites (carriérisation de la politique, impunité des élus), de même pour une grande partie les médias traditionnels, notamment la télévision.

Face à cela, les personnes ont deux réactions : Soit ils se désintéressent de la politique politicienne tout de suite, soit ils cherchent des alternatives.

Et un grand soucis, c'est qu'on a aucune alternative démocratique fondée sur le respect de l'autre. En partie parce qu'ils les ont toutes tuées. Mittérand n'est pas innocent dans l'amplification de la perte de vitesse du PCF, et tous les mouvements demandant des alternatives sont soit vus comme bisounours et moqués, au nom d'un "réalisme" douteux, soit vu comme violent et dangereux, notamment chaque manif à chaque fois qu'il y a des casseurs. Les partis traditionnels ont participé a détruire toutes les possibilités de s'exprimer pour les alternatives, pour dire qu'il n'en existait pas.

Et aussi cassé toutes les tentatives d'explications. Notre premier ministre "socialiste" n'a-t-il pas dit lui-même que "expliquer, c'est excuser", à l'encontre des sociologues ?

D'où une très forte montée de l'abstentionnisme, sous le signe d'un "a quoi bon" (ou d'une recherche de la démocratie par d'autres moyens)...

... et aussi une aubaine pour les extrèmes, qui peuvent se présenter comme la "seule alternative au système" (alors qu'ils y sont très liés, en profitent pas mal, et en fin de compte ont pleins d'idées commune au système). Ils peuvent jouer la victimisation, l'idée qu'ils seraient des "rebelles" face au grand méchant système qui est contre eux... d'autant plus que cedit système est en plus avant ce qui a participé à décomplexer leur parole, à mettre certains de leurs auteurs et polémistes phares en plein milieu de la scène.

Cela donne l'impression qu'on arrive dans un monde à deux pôles, celui du "système" et celui des réac', un monde bipolaire qui nous donne le choix entre la peste et le choléra. Même si les systèmes de valeurs "apparents" (libéralisme économique + modernisme sur certains points vs conservatisme) sont différents, ceux profonds sont les mêmes (société verticale, essentialisation des personnes, chacun pour soi et remplacement de la construction de valeurs et de libre arbitre par de la manipulation de l'image).

Qu'est-ce qu'on peut faire ?
----------------------------

Voici je pense la question la plus importante : qu'est-ce qu'on peut faire ? Je parle bien de nous, qui voulons voir autre chose que ce nouveau bipartisme à peine masqué.

Je n'ai pas vraiment d'excellente solution. S'organiser pour essayer de ne pas leur laisser l'occasion de faire du mal envers les personnes qu'ils veulent viser, qu'on en fasse partie ou non. Continuer à militer pour nos cause, faire de la pédagogie quand on le peut, essayer de faire comprendre ? Protéger et se protéger de la haine. Construire et maintenir les réseaux de solidarité.

Ne pas céder au chacun pour soi.

Donner de la visibilité aux idéologies qu'on respecte, repartager les paroles des victimes de discrimination.

Nous avons des alternatives au monde sécuritariste qui veut tout controler, à l'ultralibéralisme... dans le domaine de la consommation, dans le logiciel libre, les AMAP, les [CHATONS](http://chatons.org/). C'est pas tout, mais c'est déjà une chose. Les outils comme PGP et la protection de la vie privée sont d'autant plus importante qu'on fait partie d'une minorité.

Refuser l'idée que le monde serait juste un "grand méchant monde", partager les informations positives, les bonnes nouvelles dans le monde. Une avancée ? Une action positive pour le monde ? N'est pas aussi important de partager ça ? Refuser le cynisme des médias. Il ne faut pas que ce soit la peur qui gouverne toutes nos actions.

Est-ce que ça peut marcher ? Je ne sais pas.