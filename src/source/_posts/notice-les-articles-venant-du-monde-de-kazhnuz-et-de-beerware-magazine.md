---
title: >-
  Notice : Les articles venant du « Monde de Kazhnuz » et de « Beerware Magazine
  »
url: 44.html
id: 44
categories:
  - La vie du site
date: 2014-06-21 23:20:00
tags:
---

Vous notez que certains articles ont été importé des deux blogs suivant : [Le Monde de Kazhnuz](http://kazhnuz.quarante-douze.net/) et [Beerware-Magazine](http://beerware-magazine.net/), et que je n'ai cependant pas mis de notice pour prévenir de cela, sauf de temps en temps, alors que dans l'un des cas, les articles sont sous licence creative-common BY, et dans l'autre BY-SA.

La raison à cela est simple : les sites et articles dont je viens de parler sont de moi, pour le premier cela se voit (il s'agit de mon ancien blog personnel), mais le second est un ancien projet que je n'ai jamais réussi à faire décoller. Du coup, je n'ai pas vraiment trouvé important de préciser cela, parce que je me suis dit que dans les deux cas, je suis créateur de ces articles, et je peux me dire que je n'ai pas besoin de dire que c'est moi qui les ait fait, ahah :) Si vous vous demandez pourquoi je les ai importé ici : C'est tout simplement parce que c'est plus simple de réunir tout ces sujets similaire, sur un seul site. Le projet de réunions est encore en cours à l'heure ou ce message est publié, un message sera fait plus tard pour expliquer plus précisément le pourquoi du comment.

Avec des câlins arc-en-ciel,  
Kazhnuz