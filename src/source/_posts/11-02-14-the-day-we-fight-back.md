---
title: '11/02/14 : The Day We Fight Back'
tags:
  - Libertés numériques
url: 57.html
id: 57
categories:
  - Internet et numérique
date: 2014-01-14 00:14:00
---

Merci au Framasoft pour avoir diffusé la nouvelle et avoir traduit, pleins de datalove à vous. Une grande journée d'action est prévue pour le 11 février, en hommage à Aaron Swarz (un des meneurs de la lutte contre Acta). Parmi des organisations qui participe à cette journée d'action, on trouve Mozilla, Reddit, et d'autres organisations importantes des internets. Qu'elle marche ou ne marche pas, cette journée sera un moyen de dire qu'internet ce sont les utilisateurs, et que nous sommes près à nous engager pour ce média qui nous permet de tous d'avoir l'accès à un outil d'expression, que l'on soit petit ou grand.

Bref, nous relayons ce mouvement, à notre toute petite échelle, nous partageons, parce que nous sommes également pour un internet libre et ouvert, sans que l'on ait un gros œil au dessus de nous. [Vous pouvez avoir accès ici au site du mouvement](https://thedaywefightback.org/)

(Traduction venant du [Framablog](http://www.framablog.org/index.php/post/2014/01/13/the-day-we-fight-back), par audionuma, baba, Asta, toufalk, KoS, Omegax.)

> À l’occasion de l’anniversaire de la disparition tragique d’Aaron Swartz, des grands groupes d’internet et des plate-formes en ligne annoncent le jour de l’activisme contre la surveillance de la NSA  
>   
> Mobilisation nommée « The Day We Fight Back » en l’honneur de Swartz et pour célébrer l’anniversaire de la disparition de SOPA  
>   
> Une large coalition de groupes activistes, compagnies, et plate-formes en ligne tiendront une journée mondiale de l’activisme en opposition au régime d’espionnage de masse de la NSA, le 11 février. Nommé « Le Jour De La Contre-Attaque », la journée de l’activisme a été annoncée la veille de l’anniversaire de la disparition tragique de l’activiste et technologiste Aaron Swartz. La manifestation est en son honneur et en célébration de la victoire contre SOPA deux ans plus tôt, qu’il a contribué à arrêter.  
>   
> Parmi les participants, on compte Access, Demand Progress, l’Electronic Frontier Foundation, Fight for the Future, Free Press, BoingBoing, Reddit, Mozilla, ThoughtWorks, et plus encore à venir. Ils rejoindront potentiellement des millions d’internautes pour persuader les législateurs de mettre fin à la surveillance de masse ? non seulement des Américains, mais aussi des citoyens du monde entier.  
>   
> Le 11 janvier 2013, Aaron Swartz s’est suicidé. Aaron était doté d’un esprit brillant et intuitif, qu’il mettait au service de la technologie, de l’écriture, de la recherche, de l’art, et plus encore. Ses derniers jours furent consacrés à l’activisme politique, en soutien aux libertés civiles, à la démocratie et à la justice économique.  
>   
> Aaron a déclenché, puis aidé à diriger le mouvement qui viendrait finalement à bout de SOPA en janvier 2012. Cette loi aurait détruit l’Internet tel que nous le connaissons, en bloquant l’accès aux sites qui proposent du contenu généré par les utilisateurs ? ceux-là même qui rendent Internet si dynamique.  
>   
> David Segal, directeur exécutif de Demand Progress, qu’il a co-fondé avec Swartz, a dit : « Aujourd’hui, la plus grande menace contre un Internet libre, et plus largement contre une société libre, est le système d’espionnage de masse de la NSA. Si Aaron était encore en vie, il serait sur le front, pour combattre des pratiques qui minent notre capacité à entreprendre tous ensemble, comme des êtres humains réellement libres. »  
>   
> Selon Roy Singham, président de la compagnie de technologies globales ThoughtWorks, où Aaron a travaillé jusqu’à sa mort : « Aaron nous a montré qu’être technologiste au 21e siècle signifiait prendre des mesures pour empêcher la technologie d’être retournée contre l’intérêt public. Le moment est venu pour la tribu mondiale des technologues de se lever ensemble et de faire échouer la surveillance de masse. »  
>   
> Selon Josh Levy, de Free Press : « Depuis les premières révélations l’été dernier, des centaines de milliers d’internautes se sont réunis en ligne et hors ligne pour protester contre le programme de surveillance de la NSA, contraire à la Constitution. Ces programmes attaquent notre droit fondamental à nous connecter et à communiquer de façon privée, et frappe au cœur des fondations de la démocratie elle-même. Seul un large mouvement d’activistes, d’organisations et d’entreprises, peut convaincre Washington de restaurer ces droits. »  
>   
> Brett Solomon, directeur exécutif chez Access, ajoute : « Aaron pensait en terme de systèmes. Il savait qu’un Internet libre et ouvert est un prérequis vital pour préserver la liberté et l’ouverture des sociétés. Son esprit vit encore à travers notre conviction que là où des menaces pèsent sur cette liberté, nous nous lèverons pour les combattre. Le 11 février, nous nous battrons contre la surveillance de masse. »  
>   
> Le jour J, le collectif et les activistes qu’ils représentent téléphoneront et enverront des mails aux députés. Les propriétaires de sites web mettront en place des bannières pour encourager leurs visiteurs à combattre la surveillance et les employés d’entreprises technologiques demanderont que leur organisation fasse de même. Il sera demandé aux usagers d’Internet de créer des mèmes et de changer leurs avatars sur les média sociaux pour refléter leur demande.  
>   
> Les sites web et les usagers d’Internet qui veulent prendre part peuvent visiter TheDayWeFightBack.org pour s’inscrire et ainsi recevoir par mail les dernières nouvelles et indiquer que leur site participe à l’évènement. Des nouvelles seront régulièrement postées sur le site entre maintenant et le 11 février, date de la journée d’action.  
>   
> QUI : Access, Demand Progress, Electronic Frontier Foundation, Fight for the Future, Free Press, The Other 98%, BoingBoing, Mozilla, Reddit, ThoughtWorks… et beaucoup d’autres à venir  
>   
> QUOI : Journée d’action en opposition à la surveillance de masse, en l’honneur de Aaron Swartz et pour fêter l’anniversaire de l’abandon de SOPA  
>   
> QUAND : 11 février 2014  
>   
> CE QUE PEUVENT FAIRE LES UTILISATEURS D’INTERNET :  
>   
> \- Visiter TheDayWeFightBack.org  
> \- S’inscrire pour indiquer que vous participerez et recevoir les dernières nouvelles.  
> \- S’inscrire pour installer des widgets à mettre sur vos sites web pour encourager vos visiteurs à combattre la surveillance. (Ils seront finalisés dans les jours à venir.)  
> \- Utiliser les outils des média sociaux disponibles sur le site pour annoncer votre participation.  
> \- Developper des mèmes, des outils, des sites web et faire tout ce que vous pouvez pour participer et encourager les autres à faire de même.