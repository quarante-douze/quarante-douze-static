# Quarante-Douze

Bonjour, et bienvenue sur Dimension Quarante-Douze, ou Quarante-Douze pour les intimes. Ce blog / magazine est géré par Kazhnuz, un féru d’informatique et de culture, et cette page à pour but de présenter un peu le site et quelques points particulier.

Le site est propulsé grace à hexo.

## De quoi on parle, et comment en parle-t-on.

Ce site n’est pas un blog personnel, mais un blog orienté qui parle de plusieurs sujets qui me tiennent à cœur. Ces sujets vient des différents blogs auquel celui-ci succède (le but était de n’avoir qu’un seul blog pour parler de tous ces sujets), à savoir Souk le Manteau, Beerware-Magazine et Project Alexandria. Ces themes sont les suivants :

- Les communs, le domaine public, et la création sous licence libre.

- Les créations alternatives sur internet (notamment tout ce qui est fangame, jeux indépendant, fancomics)

- Le logiciel libre et l’internet libre.

- La défense des droits collectifs, et le partage.

## La licence Creative Commons

Tout le contenu de ce site est diffusé sous licence Creative Commons BY-SA (Attribution – Partage à l’identique. Cette licence est un copyleft, c’est à dire que vous pouvez copiez mes articles et les reproduire (où les traduire, modifier, etc), à deux conditions :

- Vous devez me citer comme auteur de l’article original (et ps : évidemment cette licence ne donne pas le droit d’attribuer des propos modifiés 😛)

- Vous devez partager l’article sous la même licence. Cela veut dire que les modification faites à l’article devront aussi être sous licence creative common BY-SA

- Évidemment, citer un de mes articles ne force absolument pas votre article à être sous Creative Commons, ni y répondre 🙂
