const { src, dest, parallel } = require('gulp');
const include = require('gulp-include');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

function css() {
  return src('scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('public'))
}

function data() {
  return src(['src/data/**/*'])
    .pipe(dest('public'));
}

exports.css  = css;
exports.data  = data;
exports.default = parallel(css, data);
